import requests
import time
from json import loads


requests.packages.urllib3.disable_warnings()


class Admin:

    user_url = 'https://ac-user.e-cash.pro/_handler/wapi/'
    admin_url = 'https://acuseradm.e-cash.pro/_handler/api/'

    def __init__(self):
        file = open(r'/Users/viktoryahoda/Desktop/ACauto/admin/token', 'r')
        self.session_token = file.read().rstrip()
        self.headers = {'x-token': self.session_token}
        file.close()
        self.currency = self._curr_id()
        self.payway = self._payway_id()
        self.lang_id = self._lang_id()

    def _curr_id(self):
        response = requests.post(url=self.admin_url, json={'model': 'currency', 'method': 'select', 'selector': {}},
                                 headers=self.headers, verify=False)
        numb = loads(response.text)[0]['fields']
        return {curr[numb.index('name')]: curr[numb.index('id')] for curr in loads(response.text)[0]['data']}

    def _payway_id(self):
        response = requests.post(url=self.admin_url, json={'model': 'payway', 'method': 'select', 'selector': {}},
                                 headers=self.headers, verify=False)
        numb = loads(response.text)[0]['fields']
        return {pw[numb.index('name')]: pw[numb.index('id')] for pw in loads(response.text)[0]['data']}

    def _lang_id(self):
        response = requests.post(url=self.admin_url, json={'model': 'lang', 'method': 'select', 'selector': {}},
                                 headers=self.headers, verify=False)
        numb = loads(response.text)[0]['fields']
        return {ln[numb.index('name')]: ln[numb.index('id')] for ln in loads(response.text)[0]['data']}

    def currencies(self):
        response = requests.post(url=self.admin_url, json={'model': 'currency', 'method': 'select', 'selector': {}},
                                 headers=self.headers, verify=False)
        numb = loads(response.text)[0]['fields']
        return {curr[numb.index('name')]: {'balance': float(curr[numb.index('balance')] / 1000000000),
                                           'is_active': curr[numb.index('is_active')], 'is_crypto': curr[numb.index('is_crypto')]} for curr in loads(response.text)[0]['data']}

    def pwcurrency(self, is_active):
        response = requests.post(url=self.admin_url,
                                 json={'model': 'pwcurrency', 'method': 'select',
                                       'selector': {'is_active': ['eq', is_active]}},
                                 headers=self.headers, verify=False)
        return loads(response.text)

    def uplink_sync(self):
        response = requests.post(url=self.admin_url,
                                 json={'model': 'uplink', 'method': 'sync'},
                                 headers=self.headers, verify=False)
        print(response.text)
        time.sleep(1)

    def uplink_sw(self):
        r = requests.post(url=self.admin_url,
                          json={'model': 'uplink', 'method': 'sw'},
                          headers=self.headers, verify=False)
        print(r.text)

    def set_st(self, name, data):
        r = requests.post(url=self.admin_url, json={'model': 'st', 'method': 'update', 'data': data, 'selector': {'name': ['eq', name]}},
                          headers=self.headers, verify=False)
        print(r.text)

    def get_user(self, email):
        time.sleep(0.5)
        response = requests.post(url=self.admin_url,
                                 json={'model': 'user', 'method': 'select',
                                       'selector': {'email': ['like', '%' + email + '%']}},
                                 headers=self.headers, verify=False)
        try:
            fields = loads(response.text)[0]['fields']
            data = loads(response.text)[0]['data'][0]
            return dict(zip(fields, data))
        except IndexError:
            return {'exception': "User wasn't found"}

    def set_wallet_amount(self, balance, currency, email):
        requests.post(url=self.admin_url,
                      json={'model': 'wallet', 'method': 'update', 'data': {'balance': balance},
                            'selector': {'currency_id': ['parent_in', {'name': ['=', currency]}],
                                         'owner_id': ['parent_in', {'email': ['=', email]}]}},
                      headers=self.headers, verify=False)

    def drop_wallet_data(self, email):
        requests.post(url=self.admin_url,
                      json={'model': 'wallet', 'method': 'update', 'data': {'balance': 0},
                            'selector': {'owner_id': ['parent_in', {'email': ['=', email]}]}},
                      headers=self.headers, verify=False)

    def set_rate_exchange(self, rate, fee, in_currency, out_currency):
        requests.post(url=self.admin_url,
                      json={'model': 'exchange', 'method': 'update', 'data': {'fee': fee, 'rate': rate},
                            'selector': {'in_currency_id': ['eq', self.currency[in_currency]],
                                         'out_currency_id': ['eq', self.currency[out_currency]]}},
                      headers=self.headers, verify=False)

    def get_rate(self, in_curr, out_curr):
        r = requests.post(url=self.admin_url,
                          json={'model': 'exchange', 'method': 'select',
                                'selector': {'in_currency_id': ['in', [in_curr]],
                                             'out_currency_id': ['in', [out_curr]]}},
                          headers=self.headers, verify=False)
        numb = loads(r.text)[0]['fields']
        return loads(r.text)[0]['data'][0][numb.index('rate')] / 1000000000

    def set_tech_limits_exchange(self, tech_min, tech_max, in_currency, out_currency):
        requests.post(url=self.admin_url,
                      json={'model': 'exchange', 'method': 'update',
                            'data': {'tech_min': tech_min, 'tech_max': tech_max},
                            'selector': {'in_currency_id': ['parent_in', {'name': ['=', in_currency]}],
                                         'out_currency_id': ['parent_in', {'name': ['=', out_currency]}]}},
                      headers=self.headers, verify=False)

    def set_personal_exchange_fee(self, is_active, in_currency, out_currency, email, fee=0):
        requests.post(url=self.admin_url,
                      json={'model': 'exchfee', 'method': 'update',
                            'data': {'is_active': is_active, 'fee': fee},
                            'selector': {'in_currency_id': ['parent_in', {'name': ['=', in_currency]}],
                                         'out_currency_id': ['parent_in', {'name': ['=', out_currency]}],
                                         'owner_id': ['parent_in', {'email': ['=', email]}]}},
                      headers=self.headers, verify=False)

    def set_user_data(self, user_email, is_customfee, user, bonus_points=0, ref_points=0):
        requests.post(url=self.admin_url,
                      json={'model': 'user', 'method': 'update',
                            'data': {'bonus_points': bonus_points, 'is_customfee': is_customfee,
                                     'ref_points': ref_points},
                            'selector': {'email': ['=', user_email]}},
                      headers=self.headers, verify=False)
        user.authorization_by_email(email=user_email)

    def create_bonus_level(self, name, points, amount, surplus, is_active):
        requests.post(url=self.admin_url,
                      json={'model': 'bonus', 'method': 'insert',
                            'data': {'name': name, 'points': points, 'amount': amount, 'surplus': surplus,
                                     'is_active': is_active}, 'selector': {}},
                      headers=self.headers, verify=False)

    def set_bonus_level(self, name=None, points=0, amount=0, surplus=0, is_active=False):
        requests.post(url=self.admin_url,
                      json={'model': 'bonus', 'method': 'update',
                            'data': {'points': points, 'amount': amount, 'surplus': surplus, 'is_active': is_active},
                            'selector': {'name': ['=', name]}},
                      headers=self.headers, verify=False)

    def delete_bonuses(self):
        requests.post(url=self.admin_url,
                      json={'model': 'bonus', 'method': 'delete', 'selector': {}},
                      headers=self.headers, verify=False)

    def set_fee(self, mult=0, add=0, _min=0, _max=0, around='ceil', tp=0, currency_id=None, payway_id=None,
                is_active=True, owner_id=None):
        requests.post(url=self.admin_url,
                      json={'model': 'fee',
                            'method': 'update',
                            'data': {'fee': {'max': _max, 'add': add, 'm': around, 'mult': mult, 'min': _min},
                                     'is_active': is_active},
                            'selector': {'currency_id': ['=', currency_id],
                                         'payway_id': ['=', payway_id],
                                         'tp': ['=', tp],
                                         'owner_id': ['=', owner_id]}},
                      headers=self.headers, verify=False)

    def set_payways(self, name, is_inactive=True):
        response = requests.post(url=self.admin_url,
                      json={'model': 'payway', 'method': 'update',
                            'data': {'is_inactive': is_inactive}, 'selector': {'name': ['eq', name]}},
                      headers=self.headers, verify=False)
        print(response.text)

    def disable_personal_fee(self, owner_id):
        requests.post(url=self.admin_url,
                      json={'model': 'fee', 'method': 'update', 'data': {'is_active': False},
                            'selector': {'owner_id': ['=', owner_id]}},
                      headers=self.headers, verify=False)

    def set_order_status(self, lid, status):
        requests.post(url=self.admin_url,
                      json={'model': 'order', 'method': 'update', 'data': {'status': status},
                            'selector': {'lid': ['in', [lid]]}},
                      headers=self.headers, verify=False)
        time.sleep(1)

    def get_order(self, lid=None, email=None):
        if lid:
            response = requests.post(url=self.admin_url,
                                     json={'model': 'order', 'method': 'select', 'selector': {'lid': ['in', [lid]]}},
                                     headers=self.headers, verify=False)
            try:
                fields = loads(response.text)[0]['fields']
                data = loads(response.text)[0]['data'][0]
                return dict(zip(fields, data))
            except IndexError:
                return {'exception': "Order wasn't found"}
        else:
            response = requests.post(url=self.admin_url,
                                     json={'model': 'order', 'method': 'select',
                                           'selector': {'owner_id': ['parent_in', {'email': ['=', email]}]}},
                                     headers=self.headers, verify=False)
            return loads(response.text)

    def get_ibill(self, email=None):
        response = requests.post(url=self.admin_url,
                                 json={'model': 'ibill', 'method': 'select',
                                       'selector': {'owner_id': ['parent_in', {'email': ['=', email]}]}},
                                 headers=self.headers, verify=False)
        return loads(response.text)

    def set_currency_data(self, is_crypto, admin_min, admin_max):
        requests.post(url=self.admin_url,
                      json={'model': 'currency', 'method': 'update',
                            'data': {'admin_min': admin_min, 'admin_max': admin_max},
                            'selector': {'is_crypto': ['=', is_crypto]}},
                      headers=self.headers, verify=False)

    def set_pwcurrency_data(self, is_active, tech_min, tech_max, currency):
        requests.post(url=self.admin_url,
                      json={'model': 'pwcurrency', 'method': 'update',
                            'data': {'tech_min': tech_min, 'tech_max': tech_max, 'is_active': is_active},
                            'selector': {'currency_id': ['eq', self.currency[currency]]}},
                      headers=self.headers, verify=False)

    def create_partner(self, name, points, total, surplus, gr_surplus, is_active=True):
        requests.post(url=self.admin_url,
                      json={'model': 'partner', 'method': 'insert',
                            'data': {'name': name, 'surplus': surplus, 'grand_surplus': gr_surplus,
                                     'is_active': is_active, 'points': points, 'total': total}},
                      headers=self.headers, verify=False)

    def set_partner(self, name, points, total, surplus, gr_surplus, is_active=True):
        requests.post(url=self.admin_url,
                      json={'model': 'partner', 'method': 'update',
                            'data': {'surplus': surplus, 'grand_surplus': gr_surplus, 'is_active': is_active,
                                     'points': points, 'total': total},
                            'selector': {'name': ['=', name]}},
                      headers=self.headers, verify=False)

    def delete_promo_levels(self):
        requests.post(url=self.admin_url,
                      json={'model': 'partner', 'method': 'delete', 'selector': {}},
                      headers=self.headers, verify=False)

    def get_profit(self, lid):
        response = requests.post(url=self.admin_url,
                                 json={'model': 'order', 'method': 'select', 'selector': {'lid': ['in', [lid]]}},
                                 headers=self.headers, verify=False)
        numb = loads(response.text)[0]['fields']
        return loads(response.text)[0]['data'][0][numb.index('profit')]

    def get_onetime_code(self, email):
        time.sleep(0.5)
        response = requests.post(url=self.admin_url,
                                 json={'model': 'twostepauth', 'method': 'select',
                                       'selector': {'owner_id': ['parent_in', {'email': ['=', email]}]}},
                                 headers=self.headers, verify=False)
        numb = loads(response.text)[0]['fields']
        return loads(response.text)[0]['data'][0][numb.index('code')]

    def get_session(self, email):
        response = requests.post(url=self.admin_url,
                                 json={'model': 'session', 'method': 'select',
                                       'selector': {'owner_id': ['parent_in', {'email': ['=', email]}]}},
                                 headers=self.headers, verify=False)
        numb = loads(response.text)[0]['fields']
        try:
            return loads(response.text)[0]['data'][0][numb.index('token')]
        except IndexError:
            return {'exception': "Session wasn't found"}

    def delete_promocodes(self, names):
        for name in names[1:]:
            requests.post(url=self.admin_url, json={'model': 'promoreward', 'method': 'delete',
                                                    'selector': {'promocode_id': ['parent_in', {'name': ['=', name]}]}},
                          headers=self.headers, verify=False)
            requests.post(url=self.admin_url, json={'model': 'promocode', 'method': 'delete',
                                                    'selector': {'name': ['=', name]}},
                          headers=self.headers, verify=False)

    def get_promocode(self, name):
        response = requests.post(url=self.admin_url,
                                 json={'model': 'promocode', 'method': 'select',
                                       'selector': {'name': ['like', '%' + name + '%']}},
                                 headers=self.headers, verify=False)
        try:
            fields = loads(response.text)[0]['fields']
            data = loads(response.text)[0]['data'][0]
            return dict(zip(fields, data))
        except IndexError:
            return {'exception': "Promocode wasn't found"}

    def delete_user(self, email):
        requests.post(url=self.admin_url, json={'model': 'user', 'method': 'delete',
                                                'selector': {'email': ['eq', email]}},
                      headers=self.headers, verify=False)

    def delete_sessions(self, email):
        requests.post(url=self.admin_url,
                      json={'model': 'session', 'method': 'delete',
                            'selector': {'owner_id': ['parent_in', {'email': ["like", '%' + email + '%']}]}},
                      headers=self.headers, verify=False)

    def delete_auth2type_token(self, email):
        requests.post(url=self.admin_url,
                      json={'model': 'twostepauth', 'method': 'delete',
                            'selector': {'owner_id': ['parent_in', {'email': ["like", '%' + email + '%']}]}},
                      headers=self.headers, verify=False)

    def get_cryptoadress(self, email):
        response = requests.post(url=self.admin_url,
                                 json={'model': 'address', 'method': 'select',
                                       'selector': {'owner_id': ['parent_in', {'email': ['=', email]}]}},
                                 headers=self.headers, verify=False)
        return loads(response.text)[0]['data']


if __name__ == '__main__':
    admin = Admin()
    # admin.set_tech_limits_exchange(tech_min=10000000000, tech_max=100000000000, in_currency='RUB', out_currency='UAH')
    # admin.set_rate_exchange(rate=25000000000, fee=0, in_currency='USD', out_currency='UAH')
    # print(admin.currency, admin.payway, admin.lang_id, admin.currencies())
    admin.delete_user(email='viktor.yahoda@gmail.com')
    # admin.set_domain(name='dev_login', data={'value': {'value': True}})
    # print(admin.get_rate(in_curr=admin.currency['UAH'], out_curr=admin.currency['USD']))
    # admin.uplink_sw()
    # admin.uplink_sync()
    # print(admin.get_onetime_code('anycashuser100@mailinator.com'))
    # admin.uplink_sync()
    # admin.set_wallet_amount(balance=100000000000, currency='UAH', email='anycashuser1@mailinator.com')
    # admin.set_rate_exchange(in_currency='USD', out_currency='UAH', fee=0, rate=28199900000)
    # admin.set_personal_exchange_fee(in_currency='RUB', out_currency='UAH',
                                    # is_active=False, email='anycashuser1@mailinator.com')
    # print(admin.get_order(lid=4458)['payee'])
    # admin.set_payways(name='visamc', is_inactive=False)
    # admin.set_order_status(lid=6061, status=100)
    # admin.set_order_status(lid=5008, status=100)
    # admin.set_tech_limits_exchange(tech_min=1000000000, tech_max=3000000000000, in_currency='UAH', out_currency='USD')
    # print(admin.get_session(email='anycashuser1@mailinator.com'))


