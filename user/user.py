import requests
import random
import time
from json import loads, dumps
from string import ascii_uppercase


requests.packages.urllib3.disable_warnings()


class User:

    wapi_url = 'https://ac-user.e-cash.pro/_handler/wapi/'
    users_data = {}
    cheque = ''
    bill = ''
    protected_cheque = ''
    part_protected_cheque = ''
    resp_registration = {}
    headers = {'x-token': ''}
    confirm_key = ''
    resp_confirm = {}

    def __init__(self, user=None):
        if user:
            file = open(r'/Users/viktoryahoda/Desktop/ACauto/user/users', 'r')
            user_data = loads(file.read())
            self.email = user_data[user]['email']
            self.btc_wallet = user_data[user]['btc_wallet']
            self.eth_wallet = user_data[user]['eth_wallet']
            self.id = user_data[user]['id']
            file.close()
        self.promocodes = [None]

    def ex_id(self):
        return random.choice(list(ascii_uppercase)) + str(random.randint(0, 100000))

    @staticmethod
    def registration(email, pwd, promo=None):
        print(email, pwd)
        response = requests.post(url=User.wapi_url,
                                 json={'method': 'register', 'model': 'account',
                                       'data': {'email': email, 'pwd': pwd, 'promo': promo}},
                                 verify=False)
        print(response.text)
        if 'key' in loads(response.text):
            User.confirm_key = loads(response.text)['key']
            User.resp_registration = loads(response.text)
            return loads(response.text)['key']
        else:
            User.resp_registration = loads(response.text)

    @staticmethod
    def cancel_2auth(key):
        requests.post(url=User.wapi_url,
                      json={'method': 'auth2cancel', 'model': 'account', 'data': {'key': key}},
                      verify=False)

    @staticmethod
    def confirm_registration(key, code, user=None):
        if not user:
            response = requests.post(url=User.wapi_url,
                                     json={'method': 'auth2confirm', 'model': 'account',
                                           'data': {'code': code, 'key': key}}, verify=False)
            print(response.text)
            if 'session' in loads(response.text):
                User.resp_confirm = loads(response.text)
                User.headers = {'x-token': loads(response.text)['session']['token']}
            else:
                User.resp_confirm = loads(response.text)
        else:
            response = requests.post(url=User.wapi_url,
                                     json={'method': 'auth2confirm', 'model': 'account',
                                           'data': {'code': code, 'key': key}},
                                     verify=False)
            print(response.text)
            if 'key' in loads(response.text):
                user.confirm_key = loads(response.text)['key']
                return loads(response.text)['key']
            else:
                if 'session' in loads(response.text):
                    user.headers = {'x-token': loads(response.text)['session']['token']}
                    user.resp_confirm = loads(response.text)
                else:
                    user.resp_confirm = loads(response.text)

    def second_confirm(self, code, key):
        response = requests.post(url=User.wapi_url,
                                 json={'method': 'auth2confirm', 'model': 'account',
                                       'data': {'code': code, 'key': key}},
                                 verify=False)
        print(response.text)
        if 'session' in loads(response.text):
            self.headers = {'x-token': loads(response.text)['session']['token']}
            self.resp_second_confirm = loads(response.text)
        else:
            self.resp_second_confirm = loads(response.text)

    def authorization_by_email(self, email, pwd='123456'):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'login', 'model': 'account', 'data': {'email': email, 'pwd': pwd}},
                                 verify=False)
        print(response.text)
        if 'session' in loads(response.text):
            self.resp_authorization = loads(response.text)
            self.headers = {'x-token': loads(response.text)['session']['token']}
        elif 'key' in loads(response.text):
            self.resp_authorization = loads(response.text)
            self.confirm_key = loads(response.text)['key']
        else:
            self.resp_authorization = loads(response.text)

    def set_2type(self, tp=None):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'set_auth2type', 'model': 'account', 'data': {'tp': tp}},
                                 headers=self.headers, verify=False)
        print(response.text)
        time.sleep(0.5)
        if 'key' in loads(response.text):
            self.confirm_key = loads(response.text)['key']
        elif 'session' in loads(response.text):
            self.headers = {'x-token': loads(response.text)['session']['token']}
        else:
            self.resp_2type = loads(response.text)

    def logout(self):
        requests.post(url=self.wapi_url, json={'model': 'account', 'method': 'logout'}, headers=self.headers, verify=False)

    def renew_session(self):
        response = requests.post(url=self.wapi_url, json={'model': 'account', 'method': 'renew'}, headers=self.headers,
                                 verify=False)
        print(response.text)
        if 'token' in loads(response.text):
            self.headers = {'x-token': loads(response.text)['token']}
            self.resp_renew = loads(response.text)
        else:
            self.resp_renew = loads(response.text)

    def forgot(self, email):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'forgot', 'model': 'account', 'data': {'email': email}},
                                 verify=False)
        print(response.text)
        if 'key' in loads(response.text):
            self.forgot_key = loads(response.text)['key']
        else:
            self.resp_forgot = loads(response.text)

    def save_session(self, user):
        self.renew_session()
        User.users_data[user]['token'] = self.headers['token']
        user_string = dumps(User.users_data)
        file = open(r'user/users', 'w')
        file.write(user_string)
        file.close()

    def get_balances(self, curr):
        time.sleep(0.5)
        response = requests.post(url=self.wapi_url,
                                 json={'model': 'account', 'method': 'balances', 'data': {'curr': curr}},
                                 headers=self.headers, verify=False)
        print(response.text)
        self.resp_balances = loads(response.text)

    def get_bonuses(self):
        response = requests.post(url=self.wapi_url,
                                 json={'model': 'account', 'method': 'bonus'},
                                 headers=self.headers, verify=False)
        print(response.text)
        self.response_bonuses = loads(response.text)

    def update_email(self, email):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'update_email', 'model': 'account',
                                       'data': {'email': email}},
                                 headers=self.headers, verify=False)
        print(response.text)
        if 'key' in loads(response.text):
            self.confirm_key = loads(response.text)['key']
            return loads(response.text)['key']
        else:
            self.resp_update_email = loads(response.text)

    def update_pwd(self, pwd):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'update_pwd', 'model': 'account',
                                       'data': {'pwd': pwd}},
                                 headers=self.headers, verify=False)
        print(response.text)
        if 'key' in loads(response.text):
            self.confirm_key = loads(response.text)['key']
            return loads(response.text)['key']
        elif 'session' in loads(response.text):
            self.resp_update_pwd = loads(response.text)
            self.headers = {'x-token': loads(response.text)['session']['token']}
        else:
            self.resp_update_pwd = loads(response.text)

    def update_lang(self, lang):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'update_lang', 'model': 'account',
                                       'data': {'lang': lang}},
                                 headers=self.headers, verify=False)
        if 'session' in loads(response.text):
            self.headers = {'x-token': loads(response.text)['session']['token']}
        else:
            self.resp_update_lang = loads(response.text)

    def update_tz(self, tz):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'update_tz', 'model': 'account',
                                       'data': {'tz': tz}},
                                 headers=self.headers, verify=False)
        if 'session' in loads(response.text):
            self.headers = {'x-token': loads(response.text)['session']['token']}
        else:
            self.resp_update_tz = loads(response.text)

    def update_safemode(self, safemode):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'update_safemode', 'model': 'account',
                                       'data': {'safemode': safemode}},
                                 headers=self.headers, verify=False)
        if 'session' in loads(response.text):
            self.headers = {'x-token': loads(response.text)['session']['token']}
        else:
            self.resp_update_safemode = loads(response.text)

    def payway_list(self):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'list', 'model': 'payway'},
                                 headers=self.headers, verify=False)
        self.resp_payway_list = loads(response.text)

    def exchange_list(self, in_curr, out_curr):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'list', 'model': 'exchange',
                                       'data': {'in_curr': in_curr, 'out_curr': out_curr}},
                                 headers=self.headers, verify=False)
        self.resp_exchange_list = loads(response.text)

    def order_history(self, tp=None, group=None, in_curr=None, out_curr=None, payway=None, final=None, st=None,
                      begin=None, end=None, first=None, count=None, ord_by=None, ord_dir=None, address_id=None):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'order_history', 'model': 'account',
                                       'data': {'tp': tp, 'group': group,  'in_curr': in_curr, 'out_curr': out_curr,
                                                'payway': payway, 'final': final, 'st': st,  'begin': begin, 'end': end,
                                                'first': first, 'count': count, 'ord_by': ord_by, 'ord_dir': ord_dir,
                                                'address_id': address_id}},
                                 headers=self.headers, verify=False)
        self.resp_order_history = loads(response.text)

    def ibill_list(self, curr=None, group=None, tp=None, oid=None, order_id=None, begin=None, end=None, first=None,
                   count=None):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'list', 'model': 'ibill',
                                       'data': {'curr': curr, 'group': group, 'tp': tp, 'oid': oid,
                                                'order_lid': order_id, 'begin': begin, 'end': end, 'first': first,
                                                'count': count}},
                                 headers=self.headers, verify=False)
        self.resp_ibill_list = loads(response.text)

    def userdata_create(self, payway, account, curr, coment):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'create', 'model': 'userdata',
                                       'data': {'payway': payway, 'account': account, 'curr': curr, 'comment': coment}},
                                 headers=self.headers, verify=False)
        self.resp_userdata_create = loads(response.text)

    def userdata_get(self, oid):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'get', 'model': 'userdata', 'data': {'oid': oid}},
                                 headers=self.headers, verify=False)
        self.resp_userdata_get = loads(response.text)

    def userdata_list(self, payway, account, curr, first, count):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'list', 'model': 'userdata',
                                       'data': {'payway': payway, 'account': account, 'curr': curr,
                                                'first': first, 'count': count}},
                                 headers=self.headers, verify=False)
        self.resp_userdata_list = loads(response.text)

    def userdata_validate(self, payway, account):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'validate', 'model': 'userdata',
                                       'data': {'payway': payway, 'account': account}},
                                 headers=self.headers, verify=False)
        self.resp_userdata_validate = response.text

    def userdata_delete(self, oid):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'delete', 'model': 'userdata',
                                       'data': {'oid': oid}},
                                 headers=self.headers, verify=False)
        self.resp_userdata_validate = loads(response.text)

    def adress_currencies(self):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'currencies', 'model': 'address'},
                                 headers=self.headers, verify=False)
        self.resp_adress_currencies = loads(response.text)

    def adress_list(self, first, count, in_curr, out_curr):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'list', 'model': 'address',
                                       'data': {'first': first, 'count': count, 'in_curr': in_curr, 'out_curr': out_curr}},
                                 headers=self.headers, verify=False)
        self.resp_adress_list = loads(response.text)

    def adress_get(self, oid):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'get', 'model': 'address', 'data': {'oid': oid}},
                                 headers=self.headers, verify=False)
        self.resp_adress_get = loads(response.text)

    def adress_edit(self, oid, comment):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'edit', 'model': 'address', 'data': {'oid': oid, 'comment': comment}},
                                 headers=self.headers, verify=False)
        self.resp_adress_edit = loads(response.text)

    def adress_out_currencies(self, in_curr):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'out_currencies', 'model': 'address', 'data': {'in_curr': in_curr}},
                                 headers=self.headers, verify=False)
        self.resp_adress_out_currencies = loads(response.text)

    def adress_params(self, in_curr, out_curr):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'params', 'model': 'address', 'data': {'in_curr': in_curr, 'out_curr': out_curr}},
                                 headers=self.headers, verify=False)
        self.resp_adress_params = loads(response.text)

    def adress_create(self, in_curr, out_curr):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'create', 'model': 'address', 'data': {'in_curr': in_curr, 'out_curr': out_curr}},
                                 headers=self.headers, verify=False)
        self.resp_adress_create = loads(response.text)

    def convert_calc(self, in_amount=None, out_amount=None, in_curr=None, out_curr=None):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'calc', 'model': 'convert',
                                       'data': {'in_amount': in_amount, 'out_amount': out_amount, 'in_curr': in_curr,
                                                'out_curr': out_curr}},
                                 headers=self.headers, verify=False)
        self.resp_convert_calc = loads(response.text)

    def convert(self, in_amount=None, out_amount=None, in_curr=None, out_curr=None):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'create', 'model': 'convert',
                                       'data': {'externalid': self.ex_id(), 'in_amount': in_amount,
                                                'out_amount': out_amount, 'in_curr': in_curr, 'out_curr': out_curr}},
                                 headers=self.headers, verify=False)
        if 'lid' in loads(response.text):
            self.convert_lid = loads(response.text)['lid']
            self.resp_convert = loads(response.text)
        else:
            self.resp_convert = loads(response.text)


    def convert_get(self, lid):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'get', 'model': 'convert', 'data': {'lid': lid}},
                                 headers=self.headers, verify=False)
        self.resp_convert_get = loads(response.text)

    def convert_list(self, in_curr, out_curr, first, count):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'list', 'model': 'convert',
                                       'data': {'in_curr': in_curr, 'out_curr': out_curr, 'first': first, 'count': count}},
                                 headers=self.headers, verify=False)
        self.resp_convert_list = loads(response.text)

    def convert_params(self, in_curr, out_curr):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'params', 'model': 'convert', 'data': {'in_curr': in_curr, 'out_curr': out_curr}},
                                 headers=self.headers, verify=False)
        self.resp_convert_params = loads(response.text)

    def transfer_calc(self, amount=None, in_curr=None, out_curr=None):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'calc', 'model': 'transfer',
                                       'data': {'amount': amount, 'in_curr': in_curr, 'out_curr': out_curr}},
                                 headers=self.headers, verify=False)
        print(response.text)
        self.resp_transfer_calc = loads(response.text)

    def transfer(self, amount=None, in_curr=None, out_curr=None, email_user=None):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'create', 'model': 'transfer',
                                       'data': {'externalid': self.ex_id(), 'amount': amount, 'in_curr': in_curr,
                                                'out_curr': out_curr, 'tgt': email_user}},
                                 headers=self.headers, verify=False)
        print(response.text)
        if 'key' in loads(response.text):
            self.transfer_2type = loads(response.text)['key']
        if 'lid' in loads(response.text):
            self.transfer_lid = loads(response.text)['lid']
        self.resp_transfer = loads(response.text)

    def transfer_last_used(self, count):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'last_used', 'model': 'transfer',
                                       'data': {'count': count}},
                                 headers=self.headers, verify=False)
        self.resp_last_used = loads(response.text)

    def transfer_get(self, lid):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'get', 'model': 'transfer', 'data': {'lid': lid}},
                                 headers=self.headers, verify=False)
        self.resp_transfer_get = loads(response.text)

    def transfer_list(self, in_curr, out_curr, first, count):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'list', 'model': 'transfer',
                                       'data': {'in_curr': in_curr, 'out_curr': out_curr, 'first': first, 'count': count}},
                                 headers=self.headers, verify=False)
        self.resp_transfer_list = loads(response.text)

    def transfer_params(self, in_curr, out_curr):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'params', 'model': 'transfer',
                                       'data': {'in_curr': in_curr, 'out_curr': out_curr}},
                                 headers=self.headers, verify=False)
        self.resp_transfer_params = loads(response.text)

    def transferprotect_create(self, amount, out_curr, tgt):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'create', 'model': 'transferprotect',
                                       'data': {'externalid': self.ex_id(), 'amount': amount, 'out_curr': out_curr,
                                                'tgt': tgt}},
                                 headers=self.headers, verify=False)
        if 'lid' in loads(response.text):
            self.transferprotect_lid = loads(response.text)['lid']
            self.resp_transferprotect_create = loads(response.text)
        if 'key' in loads(response.text):
            self.resp_transferprotect_create = loads(response.text)
            self.transferprotect_2type = loads(response.text)['key']
        else:
            self.resp_transferprotect_create = loads(response.text)

    def transferprotect_calc(self, amount, out_curr):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'calc', 'model': 'transferprotect',
                                       'data': {'amount': amount, 'out_curr': out_curr}},
                                 headers=self.headers, verify=False)
        self.resp_transferprotect_calc = loads(response.text)

    def transferprotect_params(self, out_curr):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'params', 'model': 'transferprotect',
                                       'data': {'out_curr': out_curr}},
                                 headers=self.headers, verify=False)
        self.resp_transferprotect_params = loads(response.text)

    def transferprotect_confirm(self, lid):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'confirm', 'model': 'transferprotect', 'data': {'lid': lid}},
                                 headers=self.headers, verify=False)
        if 'key' in loads(response.text):
            self.transferprotect_2type = loads(response.text)['key']
        else:
            self.resp_transferprotect_confirm = loads(response.text)

    def transferprotect_refuse(self, lid):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'refuse', 'model': 'transferprotect', 'data': {'lid': lid}},
                                 headers=self.headers, verify=False)
        self.resp_transferprotect_refuse = loads(response.text)

    def transferprotect_get(self, lid):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'get', 'model': 'transferprotect', 'data': {'lid': lid}},
                                 headers=self.headers, verify=False)
        self.resp_transferprotect_get = loads(response.text)

    def transferprotect_list(self, out_curr, first, count):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'list', 'model': 'transferprotect',
                                       'data': {'out_curr': out_curr, 'first': first, 'count': count}},
                                 headers=self.headers, verify=False)
        print(response.text)
        self.resp_transferprotect_list = loads(response.text)

    def cheque_create(self, amount, out_curr, tgt, trust_expiry):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'create', 'model': 'cheque',
                                       'data': {'externalid': self.ex_id(), 'amount': amount, 'out_curr': out_curr,
                                                'tgt': tgt, 'trust_expiry': trust_expiry}},
                                 headers=self.headers, verify=False)
        print(response.text)
        if 'cheque' in loads(response.text):
            self.resp_cheque_create = loads(response.text)
            self.cheque = loads(response.text)['cheque']
            self.part_cheque = self.cheque.split('/')[0]
        elif 'key' in loads(response.text):
            self.cheque_2type = loads(response.text)['key']
            self.resp_cheque_create = loads(response.text)
        else:
            self.resp_cheque_create = loads(response.text)

    def cheque_calc(self, amount, out_curr):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'calc', 'model': 'cheque',
                                       'data': {'amount': amount, 'out_curr': out_curr}},
                                 headers=self.headers, verify=False)
        self.resp_cheque_calc = loads(response.text)

    def cheque_params(self, out_curr):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'params', 'model': 'cheque',
                                       'data': {'out_curr': out_curr}},
                                 headers=self.headers, verify=False)
        self.resp_cheque_params = loads(response.text)

    def cheque_cash(self, token):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'cash', 'model': 'cheque', 'data': {'token': token}},
                                 headers=self.headers, verify=False)
        print(response.text)
        self.resp_cheque_cash = loads(response.text)

    def cheque_get(self, part_token):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'get_cheque', 'model': 'cheque', 'data': {'token': part_token}},
                                 headers=self.headers, verify=False)
        self.resp_cheque_get = loads(response.text)

    def cheque_list(self, out_curr, first, count):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'list', 'model': 'cheque',
                                       'data': {'out_curr': out_curr, 'first': first, 'count': count}},
                                 headers=self.headers, verify=False)
        self.resp_cheque_list = loads(response.text)

    def cashed_list(self, out_curr, begin=None, end=None, first=None, count=None, ord_by=None, ord_dir=None):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'cashed_list', 'model': 'cheque',
                                       'data': {'out_curr': out_curr, 'first': first, 'count': count, 'begin': begin,
                                                'end': end, 'ord_by': ord_by, 'ord_dir': ord_dir}},
                                 headers=self.headers, verify=False)
        self.resp_cashed_list = loads(response.text)

    def payin_calc(self, payway, amount, in_curr, out_curr):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'calc', 'model': 'payin',
                                       'data': {'payway': payway, 'amount': amount,
                                                'in_curr': in_curr, 'out_curr': out_curr}},
                                 headers=self.headers, verify=False)
        self.response_payin_calc = loads(response.text)

    def payin(self, payway, amount, in_curr, out_curr, payee=None, payer=None, email=None):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'create', 'model': 'payin',
                                       'data': {'externalid': self.ex_id(), 'payway': payway, 'amount': amount,
                                                'in_curr': in_curr, 'out_curr': out_curr,
                                                'userdata': {'payer': payer, 'payee': payee, 'contact': email}}},
                                 headers=self.headers, verify=False)
        print(response.text)
        if 'lid' in loads(response.text):
            self.payin_lid = loads(response.text)['lid']
            self.response_payin = loads(response.text)
        else:
            self.response_payin = loads(response.text)

    def payin_list(self, in_curr, out_curr):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'list', 'model': 'payin',
                                       'data': {'in_curr': in_curr, 'out_curr': out_curr,
                                                         'first': None, 'count': None}},
                                 headers=self.headers, verify=False)
        self.response_payin_list = loads(response.text)

    def payin_params(self, payway, in_curr, out_curr):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'params', 'model': 'payin',
                                       'data': {'payway': payway, 'in_curr': in_curr, 'out_curr': out_curr}},
                                 headers=self.headers, verify=False)
        self.response_payin_params = loads(response.text)

    def payin_get(self, lid):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'get', 'model': 'payin', 'data': {'lid': lid}},
                                 headers=self.headers, verify=False)
        self.response_payin_get = loads(response.text)

    def payout_calc(self, payway, amount, in_curr, out_curr):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'calc', 'model': 'payout',
                                       'data': {'payway': payway, 'amount': amount,
                                                'in_curr': in_curr, 'out_curr': out_curr}},
                                 headers=self.headers, verify=False)
        self.resp_payout_calc = loads(response.text)

    def payout(self, payway, payout_wallet, amount, in_curr, out_curr):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'create', 'model': 'payout',
                                       'data': {'externalid': self.ex_id(), 'userdata': {'payee': payout_wallet},
                                                'payway': payway, 'amount': amount, 'in_curr': in_curr,
                                                'out_curr': out_curr}},
                                 headers=self.headers, verify=False)
        if 'lid' in loads(response.text):
            self.payout_lid = loads(response.text)['lid']
            self.resp_payout = loads(response.text)
        elif 'key' in loads(response.text):
            self.payout_2type = loads(response.text)['key']
            self.resp_payout = loads(response.text)
        else:
            self.resp_payout = loads(response.text)

    def payout_params(self, payway, in_curr, out_curr):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'params', 'model': 'payout',
                                       'data': {'payway': payway, 'in_curr': in_curr, 'out_curr': out_curr}},
                                 headers=self.headers, verify=False)
        self.resp_payout_params = loads(response.text)

    def payout_list(self, in_curr, out_curr, first, count):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'list', 'model': 'payout',
                                       'data': {'in_curr': in_curr, 'out_curr': out_curr, 'first': first, 'count': count}},
                                 headers=self.headers, verify=False)
        self.resp_payout_list = loads(response.text)

    def payout_get(self, lid):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'get', 'model': 'payout', 'data': {'lid': lid}},
                                 headers=self.headers, verify=False)
        self.resp_payout_get = loads(response.text)

    def in_curr_list(self, out_curr, payway):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'in_curr_list', 'model': 'payout',
                                       'data': {'out_curr': out_curr, 'payway': payway}},
                                 headers=self.headers, verify=False)
        self.resp_in_curr_list = loads(response.text)

    def create_promo(self, title=None):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'create', 'model': 'promocode', 'data': {'title': title}},
                                 headers=self.headers, verify=False)
        print(response.text)
        if 'name' in loads(response.text):
            self.promocodes.append(loads(response.text)['name'])
            self.resp_create_promo = loads(response.text)
        else:
            self.resp_create_promo = loads(response.text)

    def edit_promo(self, name, title, i):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'edit', 'model': 'promocode', 'data': {'title': title, 'name': name}},
                                 headers=self.headers, verify=False)
        if 'title' in loads(response.text):
            self.resp_edit_promo = loads(response.text)
            self.promocodes.pop(i)
            self.promocodes.insert(i, loads(response.text)['name'])

    def get_promo(self, name):
        print(name)
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'get', 'model': 'promocode', 'data': {'name': name}},
                                 headers=self.headers, verify=False)
        if 'name' in loads(response.text):
            self.resp_get_promo = loads(response.text)
        else:
            self.resp_get_promo = loads(response.text)

    def partner_list(self, first, count):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'list', 'model': 'partner', 'data': {'first': first, 'count': count}},
                                 headers=self.headers, verify=False)
        self.resp_partner_list = loads(response.text)

    def list_promo(self, first, count, is_active):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'list', 'model': 'promocode',
                                       'data': {'first': first, 'count': count, 'is_active': is_active}},
                                 headers=self.headers, verify=False)
        self.resp_list_promo = loads(response.text)

    def set_promo(self, name):
        response = requests.post(url=self.wapi_url,
                      json={'method': 'set_promo', 'model': 'account', 'data': {'promo': name}},
                      headers=self.headers, verify=False)
        self.resp_set_promo = loads(response.text)

    def partner_stats(self):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'stats', 'model': 'partner'},
                                 headers=self.headers, verify=False)
        self.resp_partner_stats = loads(response.text)

    def partner_currentlevel(self):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'currentlevel', 'model': 'partner'},
                                 headers=self.headers, verify=False)
        self.resp_partner_currentlevel = loads(response.text)

    def delete_promocode(self, name):
        requests.post(url=self.wapi_url,
                      json={'method': 'delete', 'model': 'promocode', 'data': {'name': name}},
                      headers=self.headers, verify=False)

    def bill_calc(self, amount, currency):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'calc', 'model': 'bill', 'data': {'amount': amount,
                                                                                   'curr': currency}},
                                 headers=self.headers, verify=False)
        self.response_bill_calc = loads(response.text)

    def bill_create(self, amount, curr, tgt=None):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'create', 'model': 'bill',
                                       'data': {'externalid': self.ex_id(), 'amount': amount, 'curr': curr,
                                                'tgt': tgt}},
                                 headers=self.headers, verify=False)
        self.response_bill_create = loads(response.text)
        if 'token' in self.response_bill_create:
            User.bill = self.response_bill_create['token']
            self.bill_lid = self.response_bill_create['lid']

    def bill_pay(self, in_curr, bill_token):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'pay', 'model': 'bill',
                                       'data': {'in_curr': in_curr, 'token': bill_token}},
                                 headers=self.headers, verify=False)
        self.response_bill_pay = loads(response.text)
        if 'lid' in self.response_bill_pay:
            self.bill_lid = self.response_bill_pay['lid']

    def bill_get(self, lid):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'get', 'model': 'bill', 'data': {'lid': lid}},
                                 headers=self.headers, verify=False)
        self.response_bill_get = loads(response.text)

    def bill_list(self, currency):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'list', 'model': 'bill',
                                       'data': {'curr': currency, 'first': None, 'count': None}},
                                 headers=self.headers, verify=False)
        self.response_bill_list = loads(response.text)

    def bill_params(self, currency):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'params', 'model': 'bill',
                                       'data': {'curr': currency}},
                                 headers=self.headers, verify=False)
        self.response_bill_params = loads(response.text)

    def bill_revoke(self, lid):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'revoke', 'model': 'bill', 'data': {'lid': lid}},
                                 headers=self.headers, verify=False)
        self.response_bill_revoke = loads(response.text)

    def chequeprotect_calc(self, amount, out_curr):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'calc', 'model': 'chequeprotect',
                                       'data': {'amount': amount, 'out_curr': out_curr}},
                                 headers=self.headers, verify=False)
        self.resp_chequeprotect_calc = loads(response.text)

    def chequeprotect_params(self, out_curr):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'params', 'model': 'chequeprotect', 'data': {'out_curr': out_curr}},
                                 headers=self.headers, verify=False)
        self.resp_chequeprotect_params = loads(response.text)

    def chequeprotect_create(self, amount, out_curr, tgt):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'create', 'model': 'chequeprotect',
                                       'data': {'externalid': self.ex_id(), 'amount': amount, 'out_curr': out_curr,
                                                'tgt': tgt}},
                                 headers=self.headers, verify=False)
        if 'cheque' in loads(response.text):
            User.protected_cheque = loads(response.text)['cheque']
            User.part_protected_cheque = loads(response.text)['cheque'].split(sep='/')[0]
            self.chequeprotect_lid = loads(response.text)['lid']
            self.resp_chequeprotect_create = loads(response.text)
        else:
            self.resp_chequeprotect_create = loads(response.text)

    def chequeprotect_confirm(self, token):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'confirm', 'model': 'chequeprotect', 'data': {'token': token}},
                                 headers=self.headers, verify=False)
        self.resp_chequeprotect_confirm = loads(response.text)

    def chequeprotect_refuse(self, token):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'refuse', 'model': 'chequeprotect', 'data': {'token': token}},
                                 headers=self.headers, verify=False)
        self.resp_chequeprotect_refuse = loads(response.text)

    def chequeprotect_cash(self, token):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'cash', 'model': 'chequeprotect', 'data': {'token': token}},
                                 headers=self.headers, verify=False)
        self.resp_chequeprotect_cash = loads(response.text)

    def chequeprotect_get_cheque(self, token):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'get_cheque', 'model': 'chequeprotect', 'data': {'token': token}},
                                 headers=self.headers, verify=False)
        response = loads(response.text)
        if 'ctime' in response:
            response.pop('ctime')
        if 'mtime' in response:
            response.pop('mtime')
        self.resp_chequeprotect_get_cheque = response

    def chequeprotect_list(self, out_curr, first=None, count=None):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'list', 'model': 'chequeprotect',
                                       'data': {'out_curr': out_curr, 'first': first, 'count': count}},
                                 headers=self.headers, verify=False)
        self.resp_chequeprotect_list = loads(response.text)

    def limit_convert(self, in_amount, out_amount, in_curr, out_curr, rate=None, rate_isdir=None, expiry=None):
        r = requests.post(url=self.wapi_url,
                          json={'method': 'create', 'model': 'limit_convert',
                                'data': {'externalid': self.ex_id(), 'in_amount': in_amount, 'out_amount': out_amount,
                                         'in_curr': in_curr, 'out_curr': out_curr, 'rate': rate,
                                         'rate_isdir': rate_isdir, 'expiry': expiry}},
                          headers=self.headers, verify=False)
        print('CREATE: ', r.text)
        if 'lid' in loads(r.text):
            self.limit_order_lid = loads(r.text)['lid']
            self.resp_limit_convert = loads(r.text)
        else:
            self.resp_limit_convert = loads(r.text)

    def revoke_convert(self, lid):
        r = requests.post(url=self.wapi_url,
                          json={'method': 'revoke', 'model': 'limit_convert', 'data': {'lid': lid}},
                          headers=self.headers, verify=False)
        print('REVOKE: ', r.text)
        if 'lid' in loads(r.text):
            self.resp_revoke_convert = loads(r.text)
        else:
            self.resp_revoke_convert = loads(r.text)

    def verify_cheque(self, cheque):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'verify', 'model': 'extcheque', 'data': {'cheque': cheque}},
                                 headers=self.headers, verify=False)
        self.resp_verify_cheque = loads(response.text)

    def redeem_cheque(self, cheque, secret=None):
        response = requests.post(url=self.wapi_url,
                                 json={'method': 'redeem', 'model': 'extcheque',
                                       'data': {'cheque': cheque, 'secret': secret}},
                                 headers=self.headers, verify=False)
        self.resp_redeem_cheque = loads(response.text)


if __name__ == '__main__':
    us1 = User()
    us1.authorization_by_email(email='anycashuser1@mailinator.com', pwd='123456')
    us1.transfer_list()
    # print(user.chequeprotect_get_cheque(token='ACUAH-2954'))
