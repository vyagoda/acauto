import requests
import pprint
from json import loads, dumps
import math


requests.packages.urllib3.disable_warnings()


class Uplink:

    url = 'https://acadm.e-cash.pro/_handler/api/'

    def __init__(self):
        file = open(r'/Users/viktoryahoda/Desktop/ACauto/user/uplink_token', 'r')
        self.params = {'token': file.read().rstrip()}
        file.close()
        self.currency = self._curr_id()
        self.name = self._curr_name()

    def _curr_id(self):
        response = requests.post(url=self.url, json={'model': 'currency', 'method': 'select', 'selector': {}},
                                 params=self.params, verify=False)
        numb = loads(response.text)[0]['fields']
        return {curr[numb.index('name')]: curr[numb.index('id')] for curr in loads(response.text)[0]['data']}

    def _curr_name(self):
        response = requests.post(url=self.url, json={'model': 'currency', 'method': 'select', 'selector': {}},
                                 params=self.params, verify=False)
        numb = loads(response.text)[0]['fields']
        return {curr[numb.index('id')]: curr[numb.index('name')] for curr in loads(response.text)[0]['data']}

    def rate_exchange(self, in_curr, out_curr):
        response = requests.post(url=self.url,
                                 json={'model': 'exchange', 'method': 'select',
                                       'selector': {'in_currency_id': ['in', [in_curr]],
                                                    'out_currency_id': ['in', [out_curr]]}},
                                 params=self.params, verify=False)
        return loads(response.text)[0]['data'][0][0] / 1000000000

    def rates(self):
        response = requests.post(url=self.url,
                                 json={'model': 'exchange', 'method': 'select',
                                       'selector': {'is_active': ['=', True]}},
                                 params=self.params, verify=False)
        print(response.text)
        return loads(response.text)


if __name__ == '__main__':
    uplink = Uplink()
    print(uplink.currency, uplink.name, math.ceil(uplink.rate_exchange(in_curr=uplink.currency['UAH'], out_curr=uplink.currency['USD']) * 100 ) / 100)
    # print(math.ceil(uplink.rate_exchange(in_curr=uplink.currency['UAH'], out_curr=uplink.currency['USD'])*100)/100)
    # pprint.pprint({(uplink.name[rt[18]], uplink.name[rt[19]]): {'rate': (rt[0]/1000000000), 'tech_min': (rt[6]/1000000000),
                                                        # 'tech_max': (rt[1]/1000000000)} for rt in uplink.rates()[0]['data']})
