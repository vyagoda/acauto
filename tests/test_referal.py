import pytest
import time
from user.user import User


class TestAddingReferralsPoints:

    def test_0(self, _partners):
        """ Warm """
        global admin, ref1, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9
        admin, ref1, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9 = _partners


    def test_adding_1(self):
        """ Payin referal first level 1 USD, payin another referal first level 15.30 UAH by another promo,
            payin referal second level 45 RUB, payin referal second level after registration by promocode. """
        admin.set_rate_exchange(rate=28199900000, fee=0, in_currency='USD', out_currency='UAH')
        admin.set_rate_exchange(rate=66950000000, fee=0, in_currency='USD', out_currency='RUB')
        admin.set_user_data(user_email=ref1.email, is_customfee=False, user=ref1, ref_points=0)
        admin.set_user_data(user_email=ref2.email, is_customfee=False, user=ref2, ref_points=0)
        admin.set_fee(payway_id=admin.payway['payeer'], currency_id=admin.currency['USD'], tp=0)
        admin.set_fee(payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'], tp=0)
        admin.set_fee(payway_id=admin.payway['payeer'], currency_id=admin.currency['RUB'], tp=0)
        ref2.payin(payway='payeer', amount=1, in_curr='USD', out_curr='USD')
        time.sleep(3)
        admin.set_order_status(lid=ref2.payin_lid, status=0)
        admin.set_order_status(lid=ref2.payin_lid, status=100)
        ref4.payin(payway='visamc', amount=15.3, in_curr='UAH', out_curr='UAH')
        time.sleep(3)
        admin.set_order_status(lid=ref4.payin_lid, status=0)
        admin.set_order_status(lid=ref4.payin_lid, status=100)
        ref5.payin(payway='payeer', amount=45, in_curr='RUB', out_curr='RUB')
        time.sleep(3)
        admin.set_order_status(lid=ref5.payin_lid, status=0)
        admin.set_order_status(lid=ref5.payin_lid, status=100)
        ref8.payin(payway='payeer', amount=2, in_curr='USD', out_curr='USD')
        time.sleep(3)
        admin.set_order_status(lid=ref8.payin_lid, status=0)
        admin.set_order_status(lid=ref8.payin_lid, status=100)
        assert admin.get_user(email=ref1.email)['ref_points'] / 1000000000 == 1.76
        assert admin.get_user(email=ref2.email)['ref_points'] / 1000000000 == 2.13


    def test_adding_2(self):
        """ Payin by referal second level 50 UAH, with common exchange fee 10% and fee for operation 1 UAH. """
        admin.set_rate_exchange(rate=28199900000, fee=100000000, in_currency='USD', out_currency='UAH')
        admin.set_user_data(user_email=ref1.email, is_customfee=False, user=ref1, ref_points=0)
        admin.set_user_data(user_email=ref2.email, is_customfee=False, user=ref2, ref_points=0)
        admin.set_fee(payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'], tp=0, add=1000000000)
        ref5.payin(payway='visamc', amount=50, in_curr='UAH', out_curr='UAH')
        time.sleep(3)
        admin.set_order_status(lid=ref5.payin_lid, status=0)
        admin.set_order_status(lid=ref5.payin_lid, status=100)
        assert admin.get_user(email=ref1.email)['ref_points'] / 1000000000 == 0.35
        assert admin.get_user(email=ref2.email)['ref_points'] / 1000000000 == 1.41


    def test_adding_3(self):
        """ Payin with exchange by referal second level 50 UAH, with common exchange fee 20% and personal
            exchange fee 10% with common fee for operation 2 UAH and 1%. """
        admin.set_rate_exchange(rate=28199900000, fee=200000000, in_currency='USD', out_currency='UAH')
        admin.set_user_data(user_email=ref5.email, is_customfee=True, user=ref5, ref_points=0)
        admin.set_personal_exchange_fee(is_active=True, in_currency='USD', out_currency='UAH', email=ref5.email,
                                        fee=100000000)
        admin.set_fee(payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'], tp=0, add=2000000000,
                      mult=10000000)
        admin.set_user_data(user_email=ref1.email, is_customfee=False, user=ref1, ref_points=0)
        admin.set_user_data(user_email=ref2.email, is_customfee=False, user=ref2, ref_points=0)
        ref5.payin(payway='visamc', amount=50, in_curr='UAH', out_curr='USD')
        time.sleep(3)
        admin.set_order_status(lid=ref5.payin_lid, status=0)
        admin.set_order_status(lid=ref5.payin_lid, status=100)
        assert admin.get_user(email=ref1.email)['ref_points'] / 1000000000 == 0.35
        assert admin.get_user(email=ref2.email)['ref_points'] / 1000000000 == 1.41
        admin.set_user_data(user_email=ref5.email, is_customfee=False, user=ref5, ref_points=0)


    def test_adding_4(self):
        """ Payin by referal third level 85 RUB. Referal first level don't get refs points. """
        admin.set_rate_exchange(rate=66950000000, fee=0, in_currency='USD', out_currency='RUB')
        admin.set_user_data(user_email=ref1.email, is_customfee=False, user=ref1, ref_points=1500000000)
        admin.set_fee(payway_id=admin.payway['payeer'], currency_id=admin.currency['RUB'], tp=0)
        ref7.payin(payway='payeer', amount=85, in_curr='RUB', out_curr='RUB')
        time.sleep(3)
        admin.set_order_status(lid=ref7.payin_lid, status=0)
        admin.set_order_status(lid=ref7.payin_lid, status=100)
        assert admin.get_user(email=ref1.email)['ref_points'] / 1000000000 == 1.5

    def test_adding_5(self):
        """ Adding referal points by deleted promocode. """
        admin.set_user_data(user_email=ref1.email, is_customfee=False, user=ref1, ref_points=0)
        admin.set_user_data(user_email=ref2.email, is_customfee=False, user=ref2, ref_points=0)
        admin.set_fee(payway_id=admin.payway['payeer'], currency_id=admin.currency['USD'], tp=0)
        ref9.payin(payway='payeer', amount=2, in_curr='USD', out_curr='USD')
        time.sleep(3)
        admin.set_order_status(lid=ref9.payin_lid, status=0)
        admin.set_order_status(lid=ref9.payin_lid, status=100)
        assert admin.get_user(email=ref1.email)['ref_points'] / 1000000000 == 0.40
        assert admin.get_user(email=ref2.email)['ref_points'] / 1000000000 == 1.60



class TestAddingReferralsAmount:

    def test_0(self, _partners):
        """ Warm """
        global admin, ref1, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9
        admin, ref1, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9 = _partners


    def test_gift_1(self):
        """ Exchange currency by referal second level UAH to USD: buying 2 USD with 10% common exchange fee. """
        admin.drop_wallet_data(email=ref1.email)
        admin.set_user_data(user_email=ref1.email, bonus_points=0, is_customfee=False, user=ref1,
                            ref_points=50000000000)
        admin.drop_wallet_data(email=ref2.email)
        admin.set_wallet_amount(balance=50010000000, currency='USD', email=ref2.email)
        ref1.partner_currentlevel()
        assert ref1.resp_partner_currentlevel == {'grand_surplus': '0.1', 'name': 'First', 'points': '50',
                                                  'surplus': '0.2', 'total': '50'}
        admin.set_rate_exchange(rate=28199900000, fee=100000000, in_currency='UAH', out_currency='USD')
        admin.set_wallet_amount(balance=80000000000, currency='UAH', email=ref5.email)
        admin.set_wallet_amount(balance=80000000000, currency='UAH', email=ref8.email)
        ref5.convert(in_amount=None, out_amount=2, in_curr='UAH', out_curr='USD')
        ref8.convert(in_amount=None, out_amount=2, in_curr='UAH', out_curr='USD')
        time.sleep(0.5)
        profit_ref5 = admin.get_order(lid=ref5.convert_lid)['profit']
        profit_ref8 = admin.get_order(lid=ref8.convert_lid)['profit']
        ref1.get_balances('UAH')
        assert float(ref1.resp_balances['UAH']) == (int((profit_ref5 * 0.1 * 100) / 1000000000) / 100) + (int((profit_ref8 * 0.1 * 100) / 1000000000) / 100)
        ref2.get_balances('UAH')
        assert float(ref2.resp_balances['UAH']) == (int((profit_ref5 * 0.2 * 100) / 1000000000) / 100) + (int((profit_ref8 * 0.2 * 100) / 1000000000) / 100)


    def test_gift_2(self):
        """ Exchange currency by referral second level UAH to USD: selling 3 USD with 10% common exchange fee.
            Referrer and referral first level haven't enough refs (0.01 pts) and 0.01 USD to get refs money. """
        admin.drop_wallet_data(email=ref1.email)
        admin.set_user_data(user_email=ref1.email, bonus_points=0, is_customfee=False, user=ref1,
                            ref_points=49990000000)
        admin.drop_wallet_data(email=ref2.email)
        admin.set_user_data(user_email=ref2.email, bonus_points=0, is_customfee=False, user=ref2,
                            ref_points=0)
        admin.set_wallet_amount(balance=49990000000, currency='USD', email=ref2.email)
        admin.set_wallet_amount(balance=3000000000, currency='USD', email=ref5.email)
        admin.set_rate_exchange(rate=28199900000, fee=100000000, in_currency='USD', out_currency='UAH')
        ref5.convert(in_amount=3, out_amount=None, in_curr='USD', out_curr='UAH')
        time.sleep(0.5)
        ref1.get_balances('USD')
        assert ref1.resp_balances['USD'] == '0'
        ref2.get_balances('USD')
        assert ref2.resp_balances['USD'] == '49.99'
        admin.set_wallet_amount(balance=0, currency='USD', email=ref2.email)


    def test_gift_3(self):
        """ Transfer with exchange currency by referral second level UAH to USD: transfer 3 USD from UAH with
            common exchange fee 15% and personal exchange fee 10% . Referer has enough points to third referal level
            and enough amount for first referal level, referal 1-st level has points for first referal level and
            amount for third referal level. """
        admin.drop_wallet_data(email=ref1.email)
        admin.drop_wallet_data(email=ref2.email)
        admin.set_user_data(user_email=ref1.email, bonus_points=0, is_customfee=False, user=ref1,
                            ref_points=200000000000)
        admin.set_wallet_amount(balance=50000000000, currency='USD', email=ref1.email)
        admin.set_user_data(user_email=ref2.email, bonus_points=0, is_customfee=False, user=ref2,
                            ref_points=50000000000)
        admin.set_wallet_amount(balance=200000000000, currency='USD', email=ref2.email)
        admin.set_rate_exchange(rate=28199900000, fee=150000000, in_currency='UAH', out_currency='USD')
        admin.set_user_data(user_email=ref5.email, bonus_points=0, is_customfee=True, user=ref5, ref_points=0)
        admin.set_personal_exchange_fee(is_active=True, in_currency='UAH', out_currency='USD', fee=100000000, email=ref5.email)
        ref1.partner_stats()
        assert ref1.resp_partner_stats['current_lvl'] == {'grand_surplus': '0.5', 'name': 'Third', 'points': '200',
                                                          'surplus': '0.6', 'total': '200'}
        ref2.partner_stats()
        assert ref2.resp_partner_stats['current_lvl'] == {'grand_surplus': '0.5', 'name': 'Third', 'points': '200',
                                                          'surplus': '0.6', 'total': '200'}
        admin.set_wallet_amount(balance=200000000000, currency='UAH', email=ref5.email)
        ref5.transfer(amount=3, in_curr='UAH', out_curr='USD', email_user=ref8.email)
        time.sleep(0.5)
        profit = admin.get_order(lid=ref5.transfer_lid)['profit']
        ref1.get_balances('UAH')
        assert float(ref1.resp_balances['UAH']) == int((profit * 0.5 * 100) / 1000000000) / 100
        ref2.get_balances('UAH')
        assert float(ref2.resp_balances['UAH']) == int((profit * 0.6 * 100) / 1000000000) / 100
        admin.set_user_data(user_email=ref2.email, bonus_points=0, is_customfee=False, user=ref2, ref_points=0)
        admin.set_user_data(user_email=ref5.email, bonus_points=0, is_customfee=False, user=ref2, ref_points=0)


    def test_gift_4(self):
        """ Adding gift by deleted promocode. """
        admin.drop_wallet_data(email=ref1.email)
        admin.drop_wallet_data(email=ref2.email)
        admin.set_user_data(user_email=ref1.email, bonus_points=0, is_customfee=False, user=ref1,
                            ref_points=50000000000)
        admin.set_user_data(user_email=ref2.email, bonus_points=0, is_customfee=False, user=ref2,
                            ref_points=50000000000)
        admin.set_rate_exchange(rate=28199900000, fee=150000000, in_currency='UAH', out_currency='USD')
        admin.set_wallet_amount(balance=100000000000, currency='UAH', email=ref9.email)
        ref9.convert(in_amount=None, out_amount=2, in_curr='UAH', out_curr='USD')
        time.sleep(0.5)
        profit = admin.get_order(lid=ref9.convert_lid)['profit']
        ref1.get_balances('UAH')
        assert float(ref1.resp_balances['UAH']) == int((profit * 0.1 * 100) / 1000000000) / 100
        ref2.get_balances('UAH')
        assert float(ref2.resp_balances['UAH']) == int((profit * 0.2 * 100) / 1000000000) / 100

    @pytest.mark.skip(reason='?')
    def test_gift_5(self):
        """ Creating cheque by referal. """
        admin.set_wallet_amount(balance=0, currency='UAH', email=ref1.email)
        admin.set_wallet_amount(balance=10000000000, currency='UAH', email=ref5.email)
        admin.set_fee(currency_id=admin.currency['UAH'], tp=35)
        ref5.cheque_create(amount=5, out_curr='UAH', tgt=ref1.email, trust_expiry=None)
        ref1.cheque_cash(token=ref5.cheque)
        ref1.get_balances('UAH')
        assert ref1.resp_balances['UAH'] == '5'


@pytest.mark.usefixtures('_delete_referals')
class TestReferalStructure:

    def test_warm(self, _create_referals):
        """ Warm. """
        global admin, us1, us2, us3, us4, us5, us6, us7
        admin, us1, us2, us3, us4, us5, us6, us7 = _create_referals

    def test_referal_1(self):
        """ Create three different promocodes with different titles, and subscribe three referals first level. """
        us1.create_promo(title='Promo code 1')
        assert us1.resp_create_promo['title'] == 'Promo code 1'
        us1.create_promo(title='Промо код 2')
        assert us1.resp_create_promo['title'] == 'Промо код 2'
        us1.create_promo()
        assert us1.resp_create_promo['title'] is None
        assert 'name' in us1.resp_create_promo
        us2.set_promo(name=us1.promocodes[1])
        assert us2.resp_set_promo['promocode_id'] == admin.get_promocode(name=us1.promocodes[1])['id']
        assert us2.resp_set_promo['promocode_name'] == us1.promocodes[1]
        assert us2.resp_set_promo['email'] == 'us2@mailinator.com'
        assert us2.resp_set_promo['referer_id'] == admin.get_user(email='us1@mailinator.com')['id']
        us3.set_promo(name=us1.promocodes[1])
        us4.set_promo(name=us1.promocodes[2])
        us1.get_promo(name=us1.promocodes[1])
        assert us1.resp_get_promo['refs'] == 2
        assert us1.resp_get_promo['rewards'] == {'BCHABC': '0', 'BTC': '0', 'ETH': '0', 'LTC': '0', 'RUB': '0',
                                                 'UAH': '0', 'USD': '0', 'USDT': '0'}
        assert us1.resp_get_promo['title'] == 'Promo code 1'
        us1.list_promo(first=None, count=None, is_active=True)
        assert us1.resp_list_promo['data'][1]['title'] == 'Промо код 2'
        assert us1.resp_list_promo['data'][2]['title'] == 'Promo code 1'
        assert admin.get_user(email='us2@mailinator.com')['promocode_id'] == admin.get_promocode(name=us1.promocodes[1])['id']

    def test_referal_2(self):
        """ Subscribe two referrals second level and one referal third level. """
        us2.create_promo(title='Promo 3')
        us5.set_promo(name=us2.promocodes[1])
        us2.get_promo(name=us2.promocodes[1])
        assert us2.resp_get_promo['is_active'] is True
        us5.create_promo(title='Promo for ref third level')
        us6.set_promo(name=us5.promocodes[1])
        us1.get_promo(name=us1.promocodes[1])
        assert us1.resp_get_promo['refs'] == 2
        us5.edit_promo(title='Промокод для пользователя третьего уровня... ', name=us5.promocodes[1], i=1)
        us5.get_promo(name=us5.promocodes[1])
        assert us5.resp_get_promo['title'] == 'Промокод для пользователя третьего уровня... '
        assert us5.resp_get_promo['refs'] == 1
        us5.list_promo(first=0, count=5, is_active=True)
        assert us5.resp_list_promo['data'][0]['title'] == 'Промокод для пользователя третьего уровня... '
        assert len(us5.resp_list_promo['data']) == 1

    def test_referal_3(self):
        """ Delete promocode. """
        us5.delete_promocode(name=us5.promocodes[1])
        us5.get_promo(name=us5.promocodes[1])
        assert us5.resp_get_promo['is_active'] is False
        assert us5.resp_get_promo['refs'] == 1
        assert admin.get_promocode(name=us5.promocodes[1])['is_active'] is False
        us7.set_promo(name=us5.promocodes[1])
        us5.get_promo(name=us5.promocodes[1])
        assert us5.resp_get_promo['refs'] == 1
        us5.list_promo(first=0, count=5, is_active=False)
        assert us5.resp_list_promo['data'][0]['is_active'] is False

    def test_referal_4(self, _delete_referal):
        """ Subscribe on referrer by promocode in registration. """
        User.confirm_registration(key=User.registration('us8@mailinator.com', 'Ac*123', us1.promocodes[1]),
                                  code=admin.get_onetime_code('us8@mailinator.com'))
        assert User.resp_confirm['user']['promocode_id'] == admin.get_promocode(us1.promocodes[1])['id']
        assert User.resp_confirm['user']['referer_id'] is not None
        assert admin.get_user('us8@mailinator.com')['promocode_id'] == admin.get_promocode(name=us1.promocodes[1])['id']
        assert admin.get_user('us8@mailinator.com')['referer_id'] is not None
        us1.get_promo(name=us1.promocodes[1])
        assert us1.resp_get_promo['refs'] == 3
        assert admin.get_user('us1@mailinator.com')['refs'] == 4

    def test_referal_5(self, _delete_referal):
        """ Subscribe on referrer by promocode in registration on deleted promocode. """
        User.confirm_registration(key=User.registration('us8@mailinator.com', 'Ac*123', us5.promocodes[1]),
                                  code=admin.get_onetime_code('us8@mailinator.com'))
        assert admin.get_user('us8@mailinator.com')['promocode_id'] is None
        assert admin.get_user('us8@mailinator.com')['referer_id'] is None

    def test_referal_6(self, _delete_referal):
        """ Registration with wrong promocode. """
        User.confirm_registration(key=User.registration('us8@mailinator.com', 'Ac*123', 'SomePromo'),
                                  code=admin.get_onetime_code('us8@mailinator.com'))
        assert User.resp_confirm['user']['promocode_id'] is None
        assert User.resp_confirm['user']['referer_id'] is None
        assert admin.get_user('us8@mailinator.com')['promocode_id'] is None
        assert admin.get_user('us8@mailinator.com')['referer_id'] is None

    def test_referal_7(self):
        """ Banned on subscribe referals, that was register more early than referer. """
        us6.create_promo()
        us1.set_promo(name=us6.promocodes[1])
        assert admin.get_user(email='us1@mailinator.com')['referer_id'] is None

    def test_referal_8(self):
        """ Banned on subscribe referal themself. """
        us1.set_promo(name=us1.promocodes[1])
        assert admin.get_user(email='us1@mailinator.com')['referer_id'] is None

    def test_referal_9(self):
        """ Partner list level. """
        us1.partner_list(first=None, count=None)
        assert us1.resp_partner_list['data'][0] == {'grand_surplus': '0.1', 'name': 'First', 'points': '50',
                                                    'surplus': '0.2', 'total': '50'}
        assert us1.resp_partner_list['data'][1] == {'grand_surplus': '0.3', 'name': 'Second', 'points': None,
                                                    'surplus': '0.4', 'total': '100'}
        assert us1.resp_partner_list['data'][2] == {'grand_surplus': '0.5', 'name': 'Third', 'points': '200',
                                                    'surplus': '0.6', 'total': '200'}
        assert us1.resp_partner_list['total'] == 3
        us1.partner_list(first=None, count=1)
        assert us1.resp_partner_list['data'][0] == {'grand_surplus': '0.1', 'name': 'First', 'points': '50',
                                                    'surplus': '0.2', 'total': '50'}
        assert len(us1.resp_partner_list['data']) == 1
