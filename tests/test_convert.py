import pytest
import requests
import json
import time


class TestExchange:

    def test_0(self, create_session):
        """ Warm """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session


    def test_exchange_1(self):
        """ Exchange UAH to USD: buying 0.01 USD. """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=0, in_currency='UAH', out_currency='USD')
        user1.convert_params(in_curr='UAH', out_curr='USD')
        assert user1.resp_convert_params['in_curr_balance'] == '1'
        assert user1.resp_convert_params['in_min'] == '0.01'
        assert user1.resp_convert_params['in_max'] == '3000'
        assert user1.resp_convert_params['out_max'] == '106.38'
        assert user1.resp_convert_params['out_min'] == '0.01'
        assert user1.resp_convert_params['is_convert'] is True


    def test_exchange_2(self):
        """ Exchange UAH to USD: buying 2.15 USD. Checking wallet amount after convert. """
        admin.set_wallet_amount(balance=60910000000, currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=0, in_currency='UAH', out_currency='USD')
        user1.convert_calc(out_amount=2.16, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert_calc['equiv_amount'] == '2.16'
        assert user1.resp_convert_calc['in_amount'] == '60.92'
        assert user1.resp_convert_calc['out_amount'] == '2.16'
        assert user1.resp_convert_calc['rate'] == ['28.1999', '1']
        user1.convert_calc(out_amount=2.15, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert_calc['equiv_amount'] == '2.15'
        assert user1.resp_convert_calc['in_amount'] == '60.63'
        assert user1.resp_convert_calc['out_amount'] == '2.15'
        assert user1.resp_convert_calc['rate'] == ['28.1999', '1']
        user1.convert(in_amount=None, out_amount=2.15, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert['in_amount'] == '60.63'
        assert user1.resp_convert['out_amount'] == '2.15'
        '''
        time.sleep(3)
        user1.get_balances(curr='UAH')
        assert user1.resp_balances['UAH'] == '0.28'
        user1.get_balances(curr='USD')
        assert user1.resp_balances['USD'] == '2.15'
        '''


    def test_exchange_3(self):
        """ Exchange USD to UAH: buying 0.01 UAH. """
        admin.set_wallet_amount(balance=1000000000, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=0, in_currency='USD', out_currency='UAH')
        user1.convert_params(in_curr='USD', out_curr='UAH')
        assert user1.resp_convert_params['in_curr_balance'] == '1'
        assert user1.resp_convert_params['in_min'] == '0.01'
        assert user1.resp_convert_params['in_max'] == '3000'
        assert user1.resp_convert_params['out_max'] == '84599.7'
        assert user1.resp_convert_params['out_min'] == '0.29'
        user1.convert_calc(out_amount=0.01, in_curr='USD', out_curr='UAH')
        assert user1.resp_convert_calc['in_amount'] == '0.01'
        assert user1.resp_convert_calc['out_amount'] == '0.01'


    def test_exchange_4(self):
        """ Exchange USD to UAH: buying 33.15 UAH. """
        admin.set_wallet_amount(balance=1190000000, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=0, in_currency='USD', out_currency='UAH')
        user1.convert_calc(out_amount=33.60, in_curr='USD', out_curr='UAH')
        assert user1.resp_convert_calc['in_amount'] == '1.2'
        assert user1.resp_convert_calc['out_amount'] == '33.6'
        user1.convert(in_amount=None, out_amount=33.15, in_curr='USD', out_curr='UAH')
        assert user1.resp_convert['in_amount'] == '1.18'
        assert user1.resp_convert['out_amount'] == '33.15'
        time.sleep(3)
        assert admin.get_order(user1.convert_lid)['status'] == 100


    def test_exchange_5(self):
        """ Exchange USD to UAH: selling 0.01 USD. """
        admin.set_wallet_amount(balance=10000000000, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=0, in_currency='USD', out_currency='UAH')
        user1.convert_calc(in_amount=0.01, in_curr='USD', out_curr='UAH')
        assert user1.resp_convert_calc['in_amount'] == '0.01'
        assert user1.resp_convert_calc['out_amount'] == '0.28'


    def test_exchange_6(self):
        """ Exchange USD to UAH: selling 1.55 USD. Checking wallet amount after convert. """
        admin.set_wallet_amount(balance=10000000000, currency='USD', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='UAH', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=0, in_currency='USD', out_currency='UAH')
        user1.convert_calc(in_amount=10.01, in_curr='USD', out_curr='UAH')
        assert user1.resp_convert_calc['in_amount'] == '10.01'
        assert user1.resp_convert_calc['out_amount'] == '282.28'
        user1.convert(in_amount=1.55, out_amount=None, in_curr='USD', out_curr='UAH')
        assert user1.resp_convert['in_amount'] == '1.55'
        assert user1.resp_convert['out_amount'] == '43.7'
        '''
        time.sleep(3)
        user1.get_balances(curr='USD')
        assert user1.resp_balances['USD'] == '8.45'
        user1.get_balances(curr='UAH')
        assert user1.resp_balances['UAH'] == '43.7'
        '''

    def test_exchange_7(self):
        """ Exchange UAH to USD: selling 0.28 UAH. """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=0, in_currency='UAH', out_currency='USD')
        user1.convert_calc(in_amount=1.01, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert_calc['in_amount'] == '1.01'
        user1.convert_calc(in_amount=0.28, out_amount=None, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert_calc['in_amount'] == '0.28'
        assert user1.resp_convert_calc['out_amount'] == '0'

    @pytest.mark.skip(reason='Disable exchange')
    def test_exchange_8(self):
        """ Exchange USD to BTC: buying 0.005 BTC. """
        admin.set_wallet_amount(balance=20000000000, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=3580654100000, fee=0, in_currency='USD', out_currency='BTC')
        user1.convert_params(in_curr='USD', out_curr='BTC')
        assert user1.resp_convert_params['in_min'] == '0.01'
        assert user1.resp_convert_params['in_max'] == '3000'
        assert user1.resp_convert_params['out_min'] == '0.00001'
        assert user1.resp_convert_params['out_max'] == '0.83783574'
        user1.convert(in_amount=None, out_amount=0.005, in_curr='USD', out_curr='BTC')
        assert user1.resp_convert['in_amount'] == '17.91'
        assert user1.resp_convert['out_amount'] == '0.005'
        assert user1.resp_convert['in_curr'] == 'USD'
        assert user1.resp_convert['out_curr'] == 'BTC'

    @pytest.mark.skip(reason='Disable exchange')
    def test_exchange_9(self):
        """ Exchange BTC to USD: buying 0.01 USD. """
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        admin.set_rate_exchange(rate=3580654100000, fee=0, in_currency='BTC', out_currency='USD')
        user1.convert_calc(in_amount=None, out_amount=0.01, in_curr='BTC', out_curr='USD')
        assert user1.resp_convert_calc['in_amount'] == '0.0000028'
        assert user1.resp_convert_calc['out_amount'] == '0.01'

    @pytest.mark.skip('Desable exchange')
    def test_exchange_10(self):
        """ Exchange BTC to USD: selling 0.005 BTC. """
        admin.set_wallet_amount(balance=5000000, currency='BTC', email=user1.email)
        admin.set_rate_exchange(rate=3580654100000, fee=0, in_currency='BTC', out_currency='USD')
        user1.convert(in_amount=0.005, out_amount=None, in_curr='BTC', out_curr='USD')
        assert user1.resp_convert['in_amount'] == '0.005'
        assert user1.resp_convert['out_amount'] == '17.9'


    def test_exchange_11(self):
        """ Exchange UAH to USD: buying 1 USD with 0.01% common fee. """
        admin.set_wallet_amount(balance=30000000000, currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=100000, in_currency='UAH', out_currency='USD')
        user1.convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert['in_amount'] == '28.21'
        assert user1.resp_convert['out_amount'] == '1'
        assert user1.resp_convert['rate'] == ['28.20272', '1']
        '''
        time.sleep(2)
        user1.get_balances(curr='UAH')
        assert user1.resp_balances['UAH'] == '1.79'
        user1.get_balances(curr='USD')
        assert user1.resp_balances['USD'] == '1'
        '''


    def test_exchange_12(self):
        """ Exchange UAH to USD: buying 0.01 USD with 50% common fee. """
        admin.set_wallet_amount(balance=420000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=500000000, in_currency='UAH', out_currency='USD')
        user1.convert_calc(in_amount=None, out_amount=0.01, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert_calc['in_amount'] == '0.43'
        assert user1.resp_convert_calc['out_amount'] == '0.01'


    def test_exchange_13(self):
        """ Exchange USD to UAH: selling 1 USD with 50% common fee. """
        admin.set_wallet_amount(balance=1000000000, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=500000000, in_currency='USD', out_currency='UAH')
        user1.convert_calc(in_amount=1.01, out_amount=None, in_curr='USD', out_curr='UAH')
        assert user1.resp_convert_calc['in_amount'] == '1.01'
        user1.convert(in_amount=1, out_amount=None, in_curr='USD', out_curr='UAH')
        assert user1.resp_convert['in_amount'] == '1'
        assert user1.resp_convert['out_amount'] == '14.09'

    @pytest.mark.skip(reason='Disabled exchange')
    def test_exchange_14(self):
        """ Exchange USD to BTC: selling 95.5 USD with 5% common fee. """
        admin.set_wallet_amount(balance=100000000000, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=3580654100000, fee=50000000, in_currency='USD', out_currency='BTC')
        user1.convert_params(in_curr='USD', out_curr='BTC')
        assert user1.resp_convert_params['in_min'] == '0.01'
        assert user1.resp_convert_params['in_max'] == '3000'
        assert user1.resp_convert_params['rate'] == ['3759.68681', '1']
        user1.convert(in_amount=95.5, out_amount=None, in_curr='USD', out_curr='BTC')
        assert user1.resp_convert['in_amount'] == '95.5'
        assert user1.resp_convert['out_amount'] == '0.02540105'
        assert user1.resp_convert['in_curr'] == 'USD'
        assert user1.resp_convert['out_curr'] == 'BTC'


    def test_exchange_15(self, _personal_user_data):
        """ Exchange UAH to USD: buying 2.33 USD with 0.01% personal exchange fee without common exchange fee. """
        admin.set_wallet_amount(balance=100000000000, currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=1000000000, currency='USD', email=user1.email)
        admin.set_personal_exchange_fee(is_active=True, in_currency='UAH', out_currency='USD', email=user1.email,
                                        fee=100000)
        user1.convert(in_amount=None, out_amount=2.33, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert['in_amount'] == '65.72'
        assert user1.resp_convert['out_amount'] == '2.33'
        '''
        time.sleep(2)
        user1.get_balances(curr='UAH')
        assert user1.resp_balances['UAH'] == '34.28'
        user1.get_balances(curr='USD')
        assert user1.resp_balances['USD'] == '3.33'
        '''

    @pytest.mark.skip(reason='Disabled exchange')
    def test_exchange_16(self, _personal_user_data):
        """ Exchange USD to BTC: selling 95.5 USD with 5% personal exchange fee for first user without common fee.
            Exchange USD to BTC: selling 95.5 USD with disabled personal exchange fee and without common fee for second user"""
        admin.set_wallet_amount(balance=100000000000, currency='USD', email=user1.email)
        admin.set_wallet_amount(balance=100000000000, currency='USD', email=user2.email)
        admin.set_rate_exchange(rate=3580654100000, fee=0, in_currency='USD', out_currency='BTC')
        admin.set_personal_exchange_fee(is_active=True, in_currency='USD', out_currency='BTC', email=user1.email,
                                        fee=50000000)
        user1.convert(in_amount=95.5, out_amount=None, in_curr='USD', out_curr='BTC')
        user2.convert(in_amount=95.5, out_amount=None, in_curr='USD', out_curr='BTC')
        assert user1.resp_convert['in_amount'] == '95.5'
        assert user1.resp_convert['out_amount'] == '0.02540105'
        assert user1.resp_convert['in_curr'] == 'USD'
        assert user1.resp_convert['out_curr'] == 'BTC'
        assert user1.resp_convert['rate'] == ['3759.686805', '1']
        assert user2.resp_convert['in_amount'] == '95.5'
        assert user2.resp_convert['out_amount'] == '0.0266711'
        assert user2.resp_convert['in_curr'] == 'USD'
        assert user2.resp_convert['out_curr'] == 'BTC'
        assert user2.resp_convert['rate'] == ['3580.6541', '1']
        admin.set_personal_exchange_fee(is_active=False, in_currency='USD', out_currency='BTC', email=user1.email)


    def test_exchange_17(self, _personal_user_data):
        """ Exchange UAH to USD: buying 5.15 USD with 3% individual fee for first user without common fee.
            Exchange UAH to USD: buying 5.15 USD with 1% individual fee for second user without common fee """
        admin.set_wallet_amount(balance=300000000000, currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=300000000000, currency='UAH', email=user2.email)
        admin.set_rate_exchange(rate=28199900000, fee=0, in_currency='UAH', out_currency='USD')
        admin.set_user_data(user_email=user2.email, is_customfee=True, user=user2)
        admin.set_personal_exchange_fee(is_active=True, in_currency='UAH', out_currency='USD',
                                        email=user1.email, fee=30000000)
        admin.set_personal_exchange_fee(is_active=True, in_currency='UAH', out_currency='USD',
                                        email=user2.email, fee=10000000)
        user1.convert_calc(in_amount=None, out_amount=5.15, in_curr='UAH', out_curr='USD')
        user2.convert_calc(in_amount=None, out_amount=5.15, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert_calc['in_amount'] == '149.59'
        assert user1.resp_convert_calc['out_amount'] == '5.15'
        assert user2.resp_convert_calc['in_amount'] == '146.69'
        assert user2.resp_convert_calc['out_amount'] == '5.15'
        admin.set_user_data(user_email=user2.email, is_customfee=False, user=user2)


    def test_exchange_18(self, _personal_user_data):
        """ Exchange UAH to USD: buying 1 USD with 3% common fee and 1% personal exchange fee. """
        admin.set_wallet_amount(balance=28760000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=30000000, in_currency='UAH', out_currency='USD')
        admin.set_personal_exchange_fee(is_active=True, in_currency='UAH', out_currency='USD',
                                        email=user1.email, fee=10000000)
        user1.convert_calc(in_amount=None, out_amount=1.01, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert_calc['in_amount'] == '28.77'
        assert user1.resp_convert_calc['out_amount'] == '1.01'
        assert user1.resp_convert_calc['rate'] == ['28.4819', '1']
        user1.convert_params(in_curr='UAH', out_curr='USD')
        assert user1.resp_convert_params['in_min'] == '0.01'
        assert user1.resp_convert_params['in_max'] == '3000'
        assert user1.resp_convert_params['out_max'] == '105.33'
        assert user1.resp_convert_params['out_min'] == '0.01'
        assert user1.resp_convert_params['rate'] == ['28.4819', '1']
        user1.convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert['in_amount'] == '28.49'
        assert user1.resp_convert['out_amount'] == '1'
        user1.convert_list(in_curr='UAH', out_curr='USD', first=None, count=None)
        assert user1.resp_convert_list['data'][0]['in_amount'] == '28.49'
        assert user1.resp_convert_list['data'][0]['out_amount'] == '1'
        assert user1.resp_convert_list['data'][0]['rate'] == ['28.4819', '1']

    @pytest.mark.skip('Disable exchange')
    def test_exchange_19(self, _personal_user_data):
        """ Exchange BTC to USD: selling 0.055 BTC with 3% personal exchange fee for first user with 5% common exchange fee.
            Exchange BTC to USD: selling 0.055 BTC with 1% personal exchange fee for second user with 5% common exchange fee. """
        admin.set_wallet_amount(balance=1000100000, currency='BTC', email=user1.email)
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user2.email)
        admin.set_rate_exchange(rate=3580654100000, fee=50000000, in_currency='BTC', out_currency='USD')
        admin.set_user_data(user_email=user2.email, is_customfee=True, user=user2)
        admin.set_personal_exchange_fee(is_active=True, in_currency='BTC', out_currency='USD',
                                        email=user1.email, fee=30000000)
        admin.set_personal_exchange_fee(is_active=True, in_currency='BTC', out_currency='USD',
                                        email=user2.email, fee=10000000)
        user1.convert_calc(in_amount='1.0002', out_amount=None, in_curr='BTC', out_curr='USD')
        assert user1.resp_convert_calc['in_amount'] == '1.0002'
        assert user1.resp_convert_calc['out_amount'] == '3473.92'
        user1.convert_calc(in_amount=0.055, out_amount=None, in_curr='BTC', out_curr='USD')
        user2.convert_calc(in_amount=0.055, out_amount=None, in_curr='BTC', out_curr='USD')
        assert user1.resp_convert_calc['in_amount'] == '0.055'
        assert user1.resp_convert_calc['out_amount'] == '191.02'
        assert user1.resp_convert_calc['rate'] == ['1', '3473.23447']
        assert user2.resp_convert_calc['in_amount'] == '0.055'
        assert user2.resp_convert_calc['out_amount'] == '194.96'
        assert user2.resp_convert_calc['rate'] == ['1', '3544.84755']
        admin.set_user_data(user_email=user2.email, is_customfee=False, user=user2)


    def test_exchange_20(self):
        """ Exchange UAH to USD: buying 1 USD with collected personal bonus 1% without any fee. """
        admin.set_wallet_amount(balance=50000000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=0, in_currency='UAH', out_currency='USD')
        admin.set_user_data(user_email=user1.email, bonus_points=5000000000, is_customfee=False, user=user1)
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=True)
        user1.convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert['in_amount'] == '27.92'
        assert user1.resp_convert['out_amount'] == '1'
        assert user1.resp_convert['rate'] == ['27.91791', '1']
        assert user1.resp_convert['bonus_surplus'] == '0.01'
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=False)

    @pytest.mark.skip(reason='Disabled exchange')
    def test_exchange_21(self):
        """ Exchange USD to BTC: selling 30 USD with collected personal bonus 0.5% without any fee. """
        admin.set_wallet_amount(balance=30000000000, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=3580654100000, fee=0, in_currency='USD', out_currency='BTC')
        admin.set_user_data(user_email=user1.email, bonus_points=5000000000, is_customfee=False, user=user1)
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=5000000, is_active=True)
        user1.convert(in_amount=30, out_amount=None, in_curr='USD', out_curr='BTC')
        assert user1.resp_convert['in_amount'] == '30'
        assert user1.resp_convert['out_amount'] == '0.00842045'
        assert user1.resp_convert['bonus_surplus'] == '0.005'
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=5000000, is_active=False)


    def test_exchange_22(self):
        """ Exchange UAH to USD: buying 1 USD with collected personal bonus 2% with 1% common exchange fee. """
        admin.set_wallet_amount(balance=28190000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=10000000, in_currency='UAH', out_currency='USD')
        admin.set_user_data(user_email=user1.email, bonus_points=5000000000, is_customfee=False, user=user1)
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=20000000, is_active=True)
        user1.convert_calc(in_amount=None, out_amount=1.01, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert_calc['in_amount'] == '28.2'
        assert user1.resp_convert_calc['out_amount'] == '1.01'
        user1.convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert['in_amount'] == '27.92'
        assert user1.resp_convert['out_amount'] == '1'
        assert user1.resp_convert['rate'] == ['27.91791', '1']
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=False)
        # assert admin.get_user(email=user1.email)['bonus_points'] == 6000000000

    @pytest.mark.skip(reason='Disabled exchange')
    def test_exchange_23(self):
        """ Exchange BTC to USD: selling 0.055 BTC with collected personal bonus 1% and 2% common exchange fee for first user.
            Exchange BTC to USD: selling 0.055 BTC without collected personal bonus and 2% common exchange fee for second user. """
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user2.email)
        admin.set_rate_exchange(rate=3580654100000, fee=20000000, in_currency='BTC', out_currency='USD')
        admin.set_user_data(user_email=user1.email, bonus_points=5000000000, is_customfee=False, user=user1)
        admin.set_user_data(user_email=user2.email, bonus_points=0, is_customfee=False, user=user2)
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=True)
        user1.convert_calc(in_amount=0.055, out_amount=None, in_curr='BTC', out_curr='USD')
        user2.convert_calc(in_amount=0.055, out_amount=None, in_curr='BTC', out_curr='USD')
        assert user1.resp_convert_calc['in_amount'] == '0.055'
        assert user1.resp_convert_calc['out_amount'] == '194.96'
        assert user2.resp_convert_calc['in_amount'] == '0.055'
        assert user2.resp_convert_calc['out_amount'] == '192.99'
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=False)

    @pytest.mark.skip(reason='Disabled exchange')
    def test_exchange_24(self):
        """ Exchange BTC to USD: buying 150 USD with 2% common exchange fee, with not enough personal collected points:
            4.99 personal points, 5 points need. """
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        admin.set_rate_exchange(rate=3580654100000, fee=20000000, in_currency='BTC', out_currency='USD')
        admin.set_user_data(user_email=user1.email, bonus_points=4990000000, is_customfee=False, user=user1)
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=True)
        user1.convert_calc(in_amount=None, out_amount=150, in_curr='BTC', out_curr='USD')
        assert user1.resp_convert_calc['in_amount'] == '0.04274673'
        assert user1.resp_convert_calc['out_amount'] == '150'
        assert user1.resp_convert_calc['rate'] == ['1', '3509.04101']
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=False)

    def test_exchange_25(self):
        """ Exchange UAH to USD: buying 1 USD with common fee 3%, personal fee 1%, personal collected bonus 1%. """
        admin.set_wallet_amount(balance=100000000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=30000000, in_currency='UAH', out_currency='USD')
        admin.set_user_data(user_email=user1.email, is_customfee=True, user=user1, bonus_points=5000000000)
        admin.set_personal_exchange_fee(is_active=True, in_currency='UAH', out_currency='USD',
                                        email=user1.email, fee=10000000)
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=True)
        user1.convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert['in_amount'] == '28.49'
        assert user1.resp_convert['out_amount'] == '1'
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=False)
        admin.set_user_data(user_email=user1.email, is_customfee=False, user=user1)

    def test_exchange_26(self):
        """ Exchange USD to UAH: selling 1 USD without common fee, with operations bonus 1%. """
        admin.set_wallet_amount(balance=1000000000, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=0, in_currency='USD', out_currency='UAH')
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=True)
        user1.convert_params(in_curr='USD', out_curr='UAH')
        assert user1.resp_convert_params['out_max'] == '84599.7'
        assert user1.resp_convert_params['rate'] == ['1', '28.1999']
        user1.convert(in_amount=1, out_amount=None, in_curr='USD', out_curr='UAH')
        assert user1.resp_convert['in_amount'] == '1'
        assert user1.resp_convert['out_amount'] == '28.48'
        assert user1.resp_convert['bonus_type'] == 'amount'
        user1.convert_get(lid=user1.convert_lid)
        assert user1.resp_convert_get['in_amount'] == '1'
        assert user1.resp_convert_get['out_amount'] == '28.48'
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=False)

    @pytest.mark.skip(reason='Disabled exchange')
    def test_exchange_27(self, _personal_user_data):
        """ Exchange USD to BTC: buying 0.0001 BTC with personal exchange fee, with operation bonus 1% """
        admin.set_wallet_amount(balance=1000000000, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=3580654100000, fee=0, in_currency='USD', out_currency='BTC')
        admin.set_personal_exchange_fee(is_active=True, in_currency='USD', out_currency='BTC', email=user1.email,
                                        fee=10000000)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=True)
        user1.convert(in_amount=None, out_amount=0.0001, in_curr='USD', out_curr='BTC')
        assert user1.resp_convert['in_amount'] == '0.37'
        assert user1.resp_convert['out_amount'] == '0.0001'
        admin.set_personal_exchange_fee(is_active=False, in_currency='USD', out_currency='BTC', email=user1.email)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=False)

    @pytest.mark.skip(reason='Disabled exchange')
    def test_exchange_28(self, _personal_user_data):
        """ Exchange USD to BTC: buying 0.55 BTC with common fee 3%, personal fee 0.5%, with operations bonus 2% """
        admin.set_wallet_amount(balance=2000000000000, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=3580654100000, fee=3000000000, in_currency='USD', out_currency='BTC')
        admin.set_personal_exchange_fee(is_active=True, in_currency='USD', out_currency='BTC', email=user1.email,
                                        fee=5000000)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=20000000, is_active=True)
        user1.convert(in_amount=None, out_amount=0.55, in_curr='USD', out_curr='BTC')
        assert user1.resp_convert['in_amount'] == '1979.21'
        assert user1.resp_convert['out_amount'] == '0.55'
        admin.set_personal_exchange_fee(is_active=False, in_currency='USD', out_currency='BTC', email=user1.email)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=False)

    def test_exchange_29(self):
        """ Exchange UAH to USD: buying 1 USD with common fee 3%, personal collected bonus 0.5%,
            with operations bonus 1% """
        admin.set_wallet_amount(balance=100000000000, currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=30000000, in_currency='UAH', out_currency='USD')
        admin.set_user_data(user_email=user1.email, bonus_points=5000000000, is_customfee=False, user=user1)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=True)
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=5000000, is_active=True)
        user1.convert_calc(out_amount=1, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert_calc['in_amount'] == '28.77'
        assert user1.resp_convert_calc['out_amount'] == '1'
        assert user1.resp_convert_calc['rate'] == ['28.7639', '1']
        assert user1.resp_convert_calc['bonus_amount'] == '0.01'
        user1.convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert['in_amount'] == '28.77'
        assert user1.resp_convert['out_amount'] == '1'
        assert user1.resp_convert['rate'] == ['28.7639', '1']
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=False)
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=False)

    def test_exchange_30(self, _personal_user_data):
        """ Exchange UAH to USD: buying 1 USD with common fee 3%, personal fee 1%, with operations bonus 1% """
        admin.set_wallet_amount(balance=50000000000, currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=30000000, in_currency='UAH', out_currency='USD')
        admin.set_personal_exchange_fee(is_active=True, in_currency='UAH', out_currency='USD',
                                        email=user1.email, fee=10000000)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=True)
        user1.convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert['in_amount'] == '28.49'
        assert user1.resp_convert['out_amount'] == '1'
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=False)
        admin.set_personal_exchange_fee(is_active=False, in_currency='UAH', out_currency='USD', email=user1.email)

    @pytest.mark.skip(reason='Deasable exchange')
    def test_exchange_31(self):
        """ Exchange USD to BTC: selling 10 USD with common fee 3%, personal fee 0%, with operations bonus 10% """
        admin.set_wallet_amount(balance=10000000000, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=3580654100000, fee=30000000, in_currency='USD', out_currency='BTC')
        admin.set_user_data(user_email=user1.email, is_customfee=True, user=user1)
        admin.set_personal_exchange_fee(is_active=True, in_currency='USD', out_currency='BTC', email=user1.email)
        admin.set_bonus_level(name='1usd', points=None, amount=10000000000, surplus=100000000, is_active=True)
        user1.convert_calc(in_amount=10, in_curr='USD', out_curr='BTC')
        assert user1.resp_convert_calc['in_amount'] == '10'
        assert user1.resp_convert_calc['out_amount'] == '0.00279278'
        user1.convert(in_amount=10, in_curr='USD', out_curr='BTC')
        assert user1.resp_convert['in_amount'] == '10'
        assert user1.resp_convert['out_amount'] == '0.00279278'
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=100000000, is_active=False)
        admin.set_personal_exchange_fee(is_active=False, in_currency='UAH', out_currency='USD', email=user1.email)
        admin.set_user_data(user_email=user1.email, is_customfee=False, user=user1)


    def test_exchange_32(self):
        """ Exchange USD to UAH: buying 65 UAH with common fee 5%, personal exchange fee 3%, with personal collected bonus 0.1%,
            with operations bonus 0.5% """
        admin.set_wallet_amount(balance=2550000000, currency='USD', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='UAH', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=50000000, in_currency='USD', out_currency='UAH')
        admin.set_user_data(user_email=user1.email, is_customfee=True, user=user1, bonus_points=5000000000)
        admin.set_personal_exchange_fee(is_active=True, in_currency='USD', out_currency='UAH',
                                        email=user1.email, fee=30000000)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=5000000, is_active=True)
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=1000000, is_active=True)
        user1.convert_calc(out_amount=70, in_curr='USD', out_curr='UAH')
        assert user1.resp_convert_calc['in_amount'] == '2.56'
        assert user1.resp_convert_calc['out_amount'] == '70'
        assert user1.resp_convert_calc['rate'] == ['1', '27.3539']
        user1.convert_calc(out_amount=65, in_curr='USD', out_curr='UAH')
        assert user1.resp_convert_calc['in_amount'] == '2.38'
        assert user1.resp_convert_calc['out_amount'] == '65'
        assert user1.resp_convert_calc['rate'] == ['1', '27.3539']
        user1.convert(in_amount=None, out_amount=65, in_curr='USD', out_curr='UAH')
        assert user1.resp_convert['in_amount'] == '2.38'
        assert user1.resp_convert['out_amount'] == '65'
        assert user1.resp_convert['tp'] == 'convert'
        user1.get_balances(curr='USD')
        assert user1.resp_balances['USD'] == '0.17'
        admin.set_personal_exchange_fee(is_active=False, in_currency='UAH', out_currency='USD', email=user1.email)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=5000000, is_active=False)
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=1000000, is_active=False)
        admin.set_user_data(user_email=user1.email, is_customfee=False, user=user1)


    def test_exchange_33(self):
        """ FAIL exchange similar currency: USD to USD. """
        user1.convert(in_amount=None, out_amount=1, in_curr='USD', out_curr='USD')
        assert user1.resp_convert['exception'] == 'UnavailExchange'


    def test_exchange_34(self):
        """ FAIL exchange to bad currency: UAH to UDS. """
        user1.convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='UDS')
        assert user1.resp_convert['exception'] == 'InvalidCurrency'


    def test_exchange_35(self):
        """ FAIL exchange from bad currency: UDS to UAH. """
        user1.convert(in_amount=None, out_amount=1, in_curr='UDS', out_curr='UAH')
        assert user1.resp_convert['exception'] == 'InvalidCurrency'

    @pytest.mark.skip(reason='Not inactive currency')
    def test_exchange_36(self):
        """ FAIL exchange from not active currency: BCHABC to ETH. """
        user1.convert(in_amount=None, out_amount=1, in_curr='BCH', out_curr='UAH')
        assert user1.resp_convert['exception'] == 'InvalidCurrency'

    @pytest.mark.skip(reason='Not inactive currency')
    def test_exchange_37(self):
        """ FAIL exchange to not active currency: USD to BCH. """
        user1.convert(in_amount=None, out_amount=1, in_curr='USD', out_curr='BCH')
        assert user1.resp_convert['exception'] == 'InvalidCurrency'


    def test_exchange_38(self):
        """ FAIL exchange by not active pair: RUB to USD. """
        user1.convert(in_amount=1, out_amount=None, in_curr='RUB', out_curr='USD')
        assert user1.resp_convert['exception'] == 'UnavailExchange'


    def test_exchange_39(self):
        """ FAIL exchange: selling amount more then user have on account. """
        admin.set_wallet_amount(balance=5000000000, currency='USD', email=user1.email)
        user1.convert(in_amount=5.01, out_amount=None, in_curr='USD', out_curr='UAH')
        assert user1.resp_convert['exception'] == 'InsufficientFunds'


    def test_exchange_40(self):
        """ FAIL exchange: out_amount, in_curr don't need to buying with all fee. """
        admin.set_wallet_amount(balance=28480000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=10000000, in_currency='UAH', out_currency='USD')
        user1.convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert['exception'] == 'InsufficientFunds'


    def test_exchange_41(self):
        """ FAIL exchange: in_curr less than tech_min exchange's table. """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user1.email)
        admin.set_tech_limits_exchange(tech_min=1000000000, tech_max=1000000000, in_currency='UAH', out_currency='USD')
        user1.convert(in_amount=0.99, out_amount=None, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert['exception'] == 'AmountTooSmall'
        admin.set_tech_limits_exchange(tech_min=10000000, tech_max=3000000000000, in_currency='UAH', out_currency='USD')


    def test_exchange_42(self):
        """ FAIL exchange: in_curr more than tech_max exchange's table. """
        admin.set_wallet_amount(balance=6000000000, currency='UAH', email=user1.email)
        admin.set_tech_limits_exchange(tech_min=1000000000, tech_max=5000000000, in_currency='UAH', out_currency='USD')
        user1.convert(in_amount=5.01, out_amount=None, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert['exception'] == 'AmountTooBig'
        admin.set_tech_limits_exchange(tech_min=10000000, tech_max=3000000000000, in_currency='UAH', out_currency='USD')


    def test_exchange_43(self):
        """ Convert without session token. """
        admin.set_wallet_amount(balance=50000000000, currency='UAH', email=user1.email)
        response = requests.post(url=user1.wapi_url, json={'method': 'create', 'model': 'convert',
                                                           'data': {'externalid': user1.ex_id(), 'in_amount': None,
                                                                    'out_amount': 1, 'in_curr': 'UAH',
                                                                    'out_curr': 'USD'}},
                                 verify=False)
        assert json.loads(response.text)['exception'] == 'Unauth'


    def test_exchange_44(self):
        """ Convert with wrong session token. """
        admin.set_wallet_amount(balance=50000000000, currency='UAH', email=user1.email)
        response = requests.post(url=user1.wapi_url, json={'method': 'create', 'model': 'convert',
                                                           'data': {'externalid': user1.ex_id(), 'in_amount': None,
                                                                    'out_amount': 1, 'in_curr': 'UAH',
                                                                    'out_curr': 'USD'}},
                                 headers={'x-token': '1'}, verify=False)
        assert json.loads(response.text)['exception'] == 'Unauth'


    def test_exchange_45(self):
        """ Convert without external_id parameter. """
        admin.set_wallet_amount(balance=50000000000, currency='UAH', email=user1.email)
        response = requests.post(url=user1.wapi_url, json={'method': 'create', 'model': 'convert',
                                                           'data': {'in_amount': None, 'out_amount': 1,
                                                                    'in_curr': 'UAH', 'out_curr': 'USD'}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'InvalidParam'

    @pytest.mark.skip # Not stable
    def test_exchange_46(self):
        """ Convert with existing external_id parameter. """
        admin.set_wallet_amount(balance=50000000000, currency='UAH', email=user1.email)
        response = requests.post(url=user1.wapi_url,
                                 json={'method': 'create', 'model': 'convert',
                                       'data': {'externalid': user1.resp_convert_list['data'][0]['externalid'],
                                                'in_amount': None, 'out_amount': 1, 'in_curr': 'UAH',
                                                'out_curr': 'USD'}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'Unique'


    def test_exchange_47(self):
        """ Convert without in_curr """
        admin.set_wallet_amount(balance=50000000000, currency='UAH', email=user1.email)
        response = requests.post(url=user1.wapi_url, json={'method': 'create', 'model': 'convert',
                                                           'data': {'externalid': user1.ex_id(), 'in_amount': None,
                                                                    'out_amount': 1, 'out_curr': 'USD'}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'InvalidParam'


    def test_exchange_48(self):
        """ Convert without out_curr. """
        admin.set_wallet_amount(balance=50000000000, currency='UAH', email=user1.email)
        response = requests.post(url=user1.wapi_url, json={'method': 'create', 'model': 'convert',
                                                           'data': {'externalid': user1.ex_id(), 'in_amount': None,
                                                                    'out_amount': 1, 'in_curr': 'UAH'}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'InvalidParam'


    def test_exchange_49(self):
        """ Convert without in_amount and out_amount. """
        admin.set_wallet_amount(balance=50000000000, currency='UAH', email=user1.email)
        response = requests.post(url=user1.wapi_url, json={'method': 'create', 'model': 'convert',
                                                           'data': {'externalid': user1.ex_id(),
                                                                    'in_curr': 'UAH', 'out_curr': 'USD'}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'InvalidAmount'


    def test_exchange_50(self):
        """ Convert with excess parameter in data field. """
        admin.set_wallet_amount(balance=50000000000, currency='UAH', email=user1.email)
        response = requests.post(url=user1.wapi_url, json={'method': 'create', 'model': 'convert',
                                                           'data': {'externalid': user1.ex_id(), 'in_amount': None,
                                                                    'out_amount': 1, 'in_curr': 'UAH',
                                                                    'out_curr': 'USD', 'par': 123}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'InvalidParam'

    @pytest.mark.skip(reason='Enable excess parameter in common field')
    def test_exchange_51(self):
        """ Convert with excess parameter in common field. """
        admin.set_wallet_amount(balance=50000000000, currency='UAH', email=user1.email)
        response = requests.post(url=user1.wapi_url, json={'method': 'create', 'model': 'convert', 'par': 123,
                                                           'data': {'externalid': user1.ex_id(), 'in_amount': None,
                                                                    'out_amount': 1, 'in_curr': 'UAH',
                                                                    'out_curr': 'USD'}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'InvalidParam'


    def test_exchange_52(self):
        """ Convert with wrong amount format. """
        admin.set_wallet_amount(balance=50000000000, currency='UAH', email=user1.email)
        user1.convert(in_amount=10.111, out_amount=None, in_curr='UAH', out_curr='USD')
        assert user1.resp_convert['exception'] == 'InvalidAmountFormat'

