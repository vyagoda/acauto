import pytest
from user.user import User
from user.uplink import Uplink
from admin.admin_action import Admin


@pytest.yield_fixture(scope='session', autouse=True)
def create_session():
    print('\n\t Start session')
    global admin, user1, user2, user3
    admin = Admin()
    user1 = User('user1')
    user2 = User('user2')
    user3 = User('user3')
    user1.authorization_by_email(user1.email, pwd='123456')
    user2.authorization_by_email(user2.email, pwd='123456')
    user3.authorization_by_email(user3.email, pwd='123456')

    '------------------------------------SETTING TECH LIMIT EXCHANGE-----------------------------------'
    admin.set_tech_limits_exchange(tech_min=10000000, tech_max=3000000000000, in_currency='UAH', out_currency='USD')
    admin.set_tech_limits_exchange(tech_min=10000000, tech_max=3000000000000, in_currency='USD', out_currency='UAH')
    admin.set_tech_limits_exchange(tech_min=10000000, tech_max=3000000000000, in_currency='RUB', out_currency='UAH')
    admin.set_tech_limits_exchange(tech_min=10000000, tech_max=3000000000000, in_currency='RUB', out_currency='USD')
    admin.set_tech_limits_exchange(tech_min=10000000, tech_max=3000000000000, in_currency='USD', out_currency='BTC')
    admin.set_tech_limits_exchange(tech_min=1000, tech_max=100000000000, in_currency='BTC', out_currency='USD')
    admin.set_tech_limits_exchange(tech_min=10000, tech_max=2000000000, in_currency='BTC', out_currency='UAH')
    admin.set_tech_limits_exchange(tech_min=10000, tech_max=2000000000, in_currency='ETH', out_currency='BTC')
    admin.set_tech_limits_exchange(tech_min=10000, tech_max=2000000000, in_currency='BTC', out_currency='ETH')

    '------------------------------------SETTING CURRENCY DATA-----------------------------------------'
    admin.set_currency_data(is_crypto=False, admin_min=10000000, admin_max=3000000000000)
    admin.set_currency_data(is_crypto=True, admin_min=1000, admin_max=3000000000)
    
    '------------------------------------SETTING PAYWAY CURRENCY DATA----------------------------------'
    admin.set_pwcurrency_data(currency='UAH', is_active=True, tech_min=10000000, tech_max=3000000000000)
    admin.set_pwcurrency_data(currency='USD', is_active=True, tech_min=10000000, tech_max=3000000000000)
    admin.set_pwcurrency_data(currency='RUB', is_active=True, tech_min=10000000, tech_max=3000000000000)
    admin.set_pwcurrency_data(currency='BTC', is_active=True, tech_min=10000, tech_max=3000000000000)
    admin.set_pwcurrency_data(currency='LTC', is_active=True, tech_min=10000, tech_max=3000000000000)
    admin.set_pwcurrency_data(currency='ETH', is_active=True, tech_min=10000, tech_max=3000000000000)
    admin.set_pwcurrency_data(currency='BCHABC', is_active=True, tech_min=10000, tech_max=3000000000000)
    admin.set_pwcurrency_data(currency='USDT', is_active=True, tech_min=10000, tech_max=3000000000000)

    
    '------------------------------------CREATING BONUS LEVELS-----------------------------------------'
    admin.delete_bonuses()
    admin.create_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=False)
    admin.create_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=False)
    
    '------------------------------------CREATING PARTNER LEVELS ---------------------------------------'

    admin.delete_promo_levels()
    admin.create_partner(name='First', points=50000000000, surplus=200000000, gr_surplus=100000000, total=50000000000)
    admin.create_partner(name='Second', points=None, surplus=400000000, gr_surplus=300000000, total=100000000000)
    admin.create_partner(name='Third', points=200000000000, surplus=600000000, gr_surplus=500000000, total=200000000000)


    yield admin, user1, user2, user3
    '-----------------------------------------------------------------------------------------------------'
    admin.delete_bonuses()
    admin.delete_promo_levels()
    admin.delete_sessions('@mailinator.com')
    admin.set_st(name='dev_login', data={'value': {'value': False}})
    print('\n\t End session')


@pytest.yield_fixture(scope='class')
def _uplink():
    uplink = Uplink()
    yield uplink


@pytest.yield_fixture(scope='class')
def create_bonus_level():
    print('\n\t Start bonus level testing')
    admin.delete_bonuses()
    admin.create_bonus_level(name='Bonus 1', points=5000000000, amount=100000000000, surplus=50000000, is_active=True)
    admin.create_bonus_level(name='Bonus 2', points=10000000000, amount=150000000000, surplus=100000000, is_active=True)
    admin.create_bonus_level(name='Bonus 3', points=15000000000, amount=200000000000, surplus=150000000, is_active=True)
    yield
    admin.delete_bonuses()
    admin.create_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=False)
    admin.create_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=False)
    print('\n\t Finish bonus level testing')


@pytest.yield_fixture(scope='function')
def _drop_email(email='anycashuser1000@mailinator.com'):
    yield
    print('\n\t Start drop email')
    user1.authorization_by_email(email=email, pwd='123456')
    try:
        user1.confirm_registration(key=user1.confirm_key, code=admin.get_onetime_code(email=email), user=user1)
    except IndexError:
        pass
    user1.update_email(email=user1.email)
    user1.confirm_registration(key=user1.confirm_key, code=admin.get_onetime_code(email=email), user=user1)
    try:
        user1.second_confirm(key=user1.confirm_key, code=admin.get_onetime_code(email=email))
    except IndexError:
        pass
    user1.authorization_by_email(email=user1.email, pwd='123456')
    try:
        user1.confirm_registration(key=user1.confirm_key, code=admin.get_onetime_code(email=user1.email), user=user1)
    except IndexError:
        pass
    print('\n\t Finish drop email')


@pytest.yield_fixture(scope='function')
def _drop_pwd(email='anycashuser1@mailinator.com'):
    yield
    user1.update_pwd(pwd='123456')
    try:
        user1.confirm_registration(key=user1.confirm_key, code=admin.get_onetime_code(email=email), user=user1)
    except IndexError:
        pass
    user1.authorization_by_email(email=email, pwd='123456')
    try:
        user1.confirm_registration(key=user1.confirm_key, code=admin.get_onetime_code(email=email), user=user1)
    except IndexError:
        pass


@pytest.yield_fixture(scope='function')
def _disable_personal_fee():
    yield
    admin.disable_personal_fee(owner_id=user1.id)


@pytest.yield_fixture(scope='function')
def _disable_2type(email='anycashuser1@mailinator.com'):
    yield
    user1.authorization_by_email(email=email, pwd='123456')
    user1.confirm_registration(code=admin.get_onetime_code(email=email), key=user1.confirm_key, user=user1)
    user1.set_2type(tp=None)
    user1.confirm_registration(code=admin.get_onetime_code(email=email), key=user1.confirm_key, user=user1)
    print('\n disable 2type')


@pytest.yield_fixture(scope='function')
def _delete_auth2type_token(email='@mailinator.com'):
    yield
    admin.delete_auth2type_token(email=email)
    print('\n auth2type token deleted')


@pytest.yield_fixture(scope='function')
def _delete_user(email='anycashuser100@mailinator.com'):
    yield
    admin.delete_user(email=email)
    print('\n User Deleted')


@pytest.yield_fixture(scope='class')
def _delete_sessions(email='anycashuser'):
    yield
    admin.delete_sessions(email=email)
    user1.authorization_by_email(email=user1.email, pwd='123456')
    user2.authorization_by_email(email=user2.email, pwd='123456')
    user2.authorization_by_email(email=user3.email, pwd='123456')


@pytest.yield_fixture(scope='function')
def _authorization(email='anycashuser1@mailinator.com'):
    yield
    user1.authorization_by_email(email=email, pwd='123456')


@pytest.yield_fixture(scope='function')
def _disable_custom_fee():
    yield
    admin.set_user_data(user_email=user1.email, is_customfee=False, user=user1)


@pytest.yield_fixture(scope='function')
def _personal_user_data():
    print('\n\t Start exchange with personal fee to user1')
    admin.set_user_data(user_email=user1.email, is_customfee=True, user=user1)
    yield
    admin.set_user_data(user_email=user1.email, is_customfee=False, user=user1)
    print('\n\t End exchange with personal fee to user1')


@pytest.yield_fixture(scope='function')
def _personal_user2_data():
    print('\n\t Start exchange with personal fee to user2')
    admin.set_user_data(user_email=user2.email, is_customfee=True, user=user2)
    yield
    admin.set_user_data(user_email=user2.email, is_customfee=False, user=user2)
    print('\n\t End exchange with personal fee to user2')


@pytest.yield_fixture(scope='class')
def _partners():
    print('\n Start partners')
    global ref1, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9
    ref1 = User('ref1')
    ref2 = User('ref2')
    ref3 = User('ref3')
    ref4 = User('ref4')
    ref5 = User('ref5')
    ref6 = User('ref6')
    ref7 = User('ref7')
    ref8 = User('ref8')
    ref9 = User('ref9')
    ref1.authorization_by_email(ref1.email, pwd='123456')
    ref2.authorization_by_email(ref2.email, pwd='123456')
    ref3.authorization_by_email(ref3.email, pwd='123456')
    ref4.authorization_by_email(ref4.email, pwd='123456')
    ref5.authorization_by_email(ref5.email, pwd='123456')
    ref6.authorization_by_email(ref6.email, pwd='123456')
    ref7.authorization_by_email(ref7.email, pwd='123456')
    ref8.authorization_by_email(ref8.email, pwd='123456')
    ref9.authorization_by_email(ref9.email, pwd='123456')

    yield admin, ref1, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9


@pytest.yield_fixture(scope='function')
def _personal_referal_data():
    print('\n\t Start exchange with personal fee to ref5')
    admin.set_user_data(user_email=ref5.email, is_customfee=True, user=ref5)
    yield
    admin.set_user_data(user_email=ref5.email, is_customfee=False, user=user2)
    print('\n\t End exchange with personal fee to ref5')


@pytest.yield_fixture(scope='function')
def _create_referals():
    print('\n Start users')

    global us1, us2, us3, us4, us5, us6, us7
    us1 = User()
    us1.confirm_registration(key=us1.registration('us1@mailinator.com', pwd='Ac*123'),
                             code=admin.get_onetime_code(email='us1@mailinator.com'), user=us1)
    us2 = User()
    us2.confirm_registration(key=us2.registration('us2@mailinator.com', pwd='Ac*123'),
                             code=admin.get_onetime_code(email='us2@mailinator.com'), user=us2)
    us3 = User()
    us3.confirm_registration(key=us3.registration('us3@mailinator.com', pwd='Ac*123'),
                             code=admin.get_onetime_code(email='us3@mailinator.com'), user=us3)
    us4 = User()
    us4.confirm_registration(key=us4.registration('us4@mailinator.com', pwd='Ac*123'),
                             code=admin.get_onetime_code(email='us4@mailinator.com'), user=us4)
    us5 = User()
    us5.confirm_registration(key=us5.registration('us5@mailinator.com', pwd='Ac*123'),
                             code=admin.get_onetime_code(email='us5@mailinator.com'), user=us5)
    us6 = User()
    us6.confirm_registration(key=us6.registration('us6@mailinator.com', pwd='Ac*123'),
                             code=admin.get_onetime_code(email='us6@mailinator.com'), user=us6)
    us7 = User()
    us7.confirm_registration(key=us7.registration('us7@mailinator.com', pwd='Ac*123'),
                             code=admin.get_onetime_code(email='us7@mailinator.com'), user=us7)

    yield admin, us1, us2, us3, us4, us5, us6, us7


@pytest.yield_fixture(scope='class')
def _delete_referals():
    yield
    print('\n Finish users')
    admin.delete_promocodes(us7.promocodes)
    admin.delete_user(email='us7@mailinator.com')
    admin.delete_promocodes(us6.promocodes)
    admin.delete_user(email='us6@mailinator.com')
    admin.delete_promocodes(us5.promocodes)
    admin.delete_user(email='us5@mailinator.com')
    admin.delete_promocodes(us4.promocodes)
    admin.delete_user(email='us4@mailinator.com')
    admin.delete_promocodes(us3.promocodes)
    admin.delete_user(email='us3@mailinator.com')
    admin.delete_promocodes(us2.promocodes)
    admin.delete_user(email='us2@mailinator.com')
    admin.delete_promocodes(us1.promocodes)
    admin.delete_user(email='us1@mailinator.com')


@pytest.yield_fixture(scope='function')
def _delete_referal():
    yield
    print('\n Delete referal')
    admin.delete_user(email='us8@mailinator.com')

