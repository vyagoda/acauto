import pytest
from user.user import User


class TestBonusPointsAmounting:

    def test_0(self, create_session):
        """ Warm """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session


    def test_bonus_1(self):
        """ Exchange UAH to USD: baying 1 USD and getting 1 bonus points """
        admin.set_wallet_amount(balance=30000000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(fee=0, rate=28199900000, in_currency='UAH', out_currency='USD')
        bonus_points_before = admin.get_user(email=user1.email)['bonus_points']
        user1.convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD')
        admin.set_order_status(lid=user1.convert_lid, status=0)
        admin.set_order_status(lid=user1.convert_lid, status=100)
        bonus_points_after = admin.get_user(email=user1.email)['bonus_points']
        assert (bonus_points_after - bonus_points_before) == 1000000000


    def test_bonus_2(self):
        """ Exchange UAH to USD: selling 15 UAH and getting 0.53 bonus points """
        admin.set_wallet_amount(balance=20000000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(fee=0, rate=28199900000, in_currency='UAH', out_currency='USD')
        bonus_points_before = admin.get_user(email=user1.email)['bonus_points']
        user1.convert(in_amount=15, out_amount=None, in_curr='UAH', out_curr='USD')
        admin.set_order_status(lid=user1.convert_lid, status=0)
        admin.set_order_status(lid=user1.convert_lid, status=100)
        bonus_points_after = admin.get_user(email=user1.email)['bonus_points']
        assert (bonus_points_after - bonus_points_before) == 530000000


    def test_bonus_3(self):
        """ Exchange ETH to BTC: baying 10 UAH and getting 0.35 bonus points """
        admin.set_wallet_amount(balance=300000000000, currency='RUB', email=user1.email)
        admin.set_rate_exchange(fee=0, rate=2370000000, in_currency='RUB', out_currency='UAH')
        admin.set_rate_exchange(fee=0, rate=66950000000, in_currency='RUB', out_currency='USD')
        bonus_points_before = admin.get_user(email=user1.email)['bonus_points']
        user1.convert(in_amount=None, out_amount=10, in_curr='RUB', out_curr='UAH')
        admin.set_order_status(lid=user1.convert_lid, status=0)
        admin.set_order_status(lid=user1.convert_lid, status=100)
        bonus_points_after = admin.get_user(email=user1.email)['bonus_points']
        assert (bonus_points_after - bonus_points_before) == 350000000


    def test_bonus_4(self):
        """ Payin 28.2 with exchange UAH to USD and getting 1 bonus points """
        admin.set_rate_exchange(fee=0, rate=28199900000, in_currency='UAH', out_currency='USD')
        bonus_points_before = admin.get_user(email=user1.email)['bonus_points']
        user1.payin(payway='visamc', amount=28.2, in_curr='UAH', out_curr='USD')
        admin.set_order_status(lid=user1.payin_lid, status=0)
        admin.set_order_status(lid=user1.payin_lid, status=100)
        bonus_points_after = admin.get_user(email=user1.email)['bonus_points']
        assert (bonus_points_after - bonus_points_before) == 1000003546


    def test_bonus_5(self):
        """ Payout 42.30 UAH with exchange to USD and getting 1.5 bonus points"""
        admin.set_wallet_amount(balance=50000000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(fee=0, rate=28199900000, in_currency='UAH', out_currency='USD')
        bonus_points_before = admin.get_user(email=user1.email)['bonus_points']
        user1.payout(payway='payeer', payout_wallet='P123456789', amount=1.5, in_curr='UAH', out_curr='USD')
        admin.set_order_status(lid=user1.payout_lid, status=0)
        admin.set_order_status(lid=user1.payout_lid, status=100)
        bonus_points_after = admin.get_user(email=user1.email)['bonus_points']
        assert (bonus_points_after - bonus_points_before) == 1500000000


    def test_bonus_6(self):
        """ Transfer 28.2 UAH with exchange to USD and getting 1 bonus points """
        admin.set_wallet_amount(balance=30000000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(fee=0, rate=28199900000, in_currency='UAH', out_currency='USD')
        bonus_points_before = admin.get_user(email=user1.email)['bonus_points']
        user1.transfer(amount=1, in_curr='UAH', out_curr='USD', email_user=user2.email)
        bonus_points_after = admin.get_user(email=user1.email)['bonus_points']
        assert (bonus_points_after - bonus_points_before) == 1000000000

    @pytest.mark.skip  # disabled exchanging during transfer by cheque
    def test_bonus_7(self):
        """ Transfer 28.2 UAH with exchange to USD by cheque and getting 1 bonus points """
        admin.set_wallet_amount(balance=30000000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(fee=0, rate=28199900000, in_currency='UAH', out_currency='USD')
        user1.transfer(amount=1, in_curr='UAH', out_curr='USD', email_user=None)
        bonus_points_before = admin.get_user(email=user1.email)['bonus_points']
        user2.cash_cheque(cheque=User.cheque)
        bonus_points_after = admin.get_user(email=user1.email)['bonus_points']
        assert (bonus_points_after - bonus_points_before) == 1000000000

    @pytest.mark.skip  # disabled exchanging during transfer by cheque
    def test_bonus_8(self):
        """ Transfer 28.2 UAH with exchange to USD by cheque, cashing by owner and getting 1 bonus points """
        admin.set_wallet_amount(balance=30000000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(fee=0, rate=28199900000, in_currency='UAH', out_currency='USD')
        user1.transfer(amount=1, in_curr='UAH', out_curr='USD', email_user=None)
        bonus_points_before = admin.get_user(email=user1.email)['bonus_points']
        user1.cash_cheque(cheque=User.cheque)
        bonus_points_after = admin.get_user(email=user1.email)['bonus_points']
        assert (bonus_points_after - bonus_points_before) == 1000000000

    def test_bonus_9(self):
        """ Payout 42.30 UAH without any exchange don't getting bonus points. """
        admin.set_wallet_amount(balance=50000000000, currency='UAH', email=user1.email)
        bonus_points_before = admin.get_user(email=user1.email)['bonus_points']
        user1.payout(payway='visamc', payout_wallet='4444444444444448', amount=42.3, in_curr='UAH', out_curr='UAH')
        admin.set_order_status(lid=user1.payout_lid, status=0)
        admin.set_order_status(lid=user1.payout_lid, status=100)
        bonus_points_after = admin.get_user(email=user1.email)['bonus_points']
        assert bonus_points_after == bonus_points_before

    def test_bonus_10(self):
        """ Payin 28.2 without any exchange and don't getting bonus points. """
        bonus_points_before = admin.get_user(email=user1.email)['bonus_points']
        user1.payin(payway='visamc', amount=28.2, in_curr='UAH', out_curr='UAH')
        admin.set_order_status(lid=user1.payin_lid, status=0)
        admin.set_order_status(lid=user1.payin_lid, status=100)
        bonus_points_after = admin.get_user(email=user1.email)['bonus_points']
        assert bonus_points_after == bonus_points_before












