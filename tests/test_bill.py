import pytest
import requests
import json
from user.user import User


class TestBillWithoutExchange:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_bill_1(self):
        """ Create common bill on 0.01 UAH without any fee and cashing by other user. """
        admin.set_wallet_amount(balance=0, currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user2.email)
        admin.set_fee(tp=40, currency_id=admin.currency['UAH'])
        user1.bill_calc(amount=0.01, currency='UAH')
        assert user1.response_bill_calc == {'account_amount': '0.01', 'equiv_amount': '0', 'in_amount': '0.01',
                                            'in_fee_amount': '0', 'orig_amount': '0.01', 'out_amount': '0.01',
                                            'out_fee_amount': '0'}
        user1.bill_create(amount=0.01, curr='UAH', tgt=None)
        assert user1.response_bill_create['account_amount'] == '0.01'
        assert user1.response_bill_create['in_amount'] == '0.01'
        assert user1.response_bill_create['out_amount'] == '0.01'
        assert user1.response_bill_create['in_curr'] == 'UAH'
        assert user1.response_bill_create['status'] == 'new'
        assert user1.response_bill_create['tgt'] is None
        user1.get_balances(curr='UAH')
        assert user1.resp_balances['UAH'] == '0'
        user1.bill_list(currency='UAH')
        assert user1.resp_bill_list['data'][0]['status'] == 'new'
        user2.bill_pay(in_curr='UAH', bill_token=User.bill)
        assert user2.response_bill_pay == {'fee': '0', 'in_amount': '0.01', 'in_curr': 'UAH', 'out_amount': '0.01',
                                           'out_curr': 'UAH', 'payee': 'AC178', 'status': 'done'}
        user1.get_balances(curr='UAH')
        assert user1.resp_balances['UAH'] == '0.01'
        user2.get_balances(curr='UAH')
        assert user2.resp_balances['UAH'] == '0.99'

    def test_bill_2(self):
        """ Create personal bill on 1 USD without any fee and cashing by other user. """
        admin.set_wallet_amount(balance=0, currency='USD', email=user2.email)
        admin.set_wallet_amount(balance=3000000000000, currency='USD', email=user1.email)
        admin.set_fee(tp=40, currency_id=admin.currency['USD'])
        user2.bill_params(currency='USD')
        assert user2.response_bill_params == {'in_curr': 'USD', 'is_convert': False, 'max': '3000', 'min': '0.01',
                                              'in_fee': {'add': '0', 'max': '0',
                                                         'method': 'ceil', 'min': '0', 'mult': '0'},
                                              'out_curr': 'USD',
                                              'rate': ['1', '1']}
        assert user2.response_bill_params['min_balance_limit'] == '0.01'
        user2.bill_create(amount=3000, curr='USD', tgt=user1.email)
        assert user2.response_bill_create['account_amount'] == '3000'
        assert user2.response_bill_create['out_amount'] == '3000'
        assert user2.response_bill_create['tgt'] == 'AC178'
        assert user2.response_bill_create['tp'] == 'billpayeefee'
        assert user2.response_bill_create['owner'] == 'AC179'
        user1.bill_pay(in_curr='USD', bill_token=User.bill)
        assert user1.response_bill_pay == {'fee': '0', 'in_amount': '3000', 'in_curr': 'USD', 'out_amount': '3000',
                                           'out_curr': 'USD', 'payee': 'AC179', 'status': 'done'}
        user2.bill_list(currency='USD')
        assert user2.response_bill_list['data'][0]['lid'] == user2.bill_lid

    def test_bill_3(self):
        """ Create personal bill on 0.001 BTC without any fee and cashing by other user. """
        admin.set_wallet_amount(balance=0, currency='BTC', email=user1.email)
        admin.set_wallet_amount(balance=2000000000, currency='BTC', email=user2.email)
        admin.set_fee(tp=40, currency_id=admin.currency['BTC'])
        user1.bill_create(amount=0.001, curr='BTC', tgt=user2.email)
        assert user1.response_bill_create['account_amount'] == '0.001'
        assert user1.response_bill_create['in_amount'] == '0.001'
        assert user1.response_bill_create['tgt'] == 'AC179'
        assert user1.response_bill_create['tp'] == 'bill'
        user2.bill_pay(in_curr='BTC', bill_token=User.bill)
        assert user2.response_bill_pay == {'fee': '0', 'in_amount': '0.001', 'in_curr': 'BTC', 'out_amount': '0.001',
                                           'out_curr': 'BTC', 'payee': 'AC178', 'status': 'done'}
        user1.get_balances(curr='BTC')
        assert user1.resp_balances['BTC'] == '0.001'
        user2.get_balances(curr='BTC')
        assert user2.resp_balances['BTC'] == '1.999'

    def test_bill_4(self):
        """ Create common bill on 50.55 RUB with common fee for operation 10% and 2 RUB and caching by other user.
            Fee for bill owner. """
        admin.set_wallet_amount(balance=0, currency='RUB', email=user1.email)
        admin.set_wallet_amount(balance=50540000000, currency='RUB', email=user2.email)
        admin.set_fee(tp=40, currency_id=admin.currency['RUB'], add=2000000000, mult=100000000)
        user1.bill_calc(amount=50.55, currency='RUB')
        assert user1.response_bill_calc['account_amount'] == '43.49'
        assert user1.response_bill_calc['in_amount'] == '50.55'
        assert user1.response_bill_calc['out_amount'] == '50.55'
        assert user1.response_bill_calc['in_fee_amount'] == '7.06'
        assert user1.response_bill_calc['out_fee_amount'] == '7.06'
        user1.bill_create(amount=50.55, curr='RUB', tgt=None)
        assert user1.response_bill_create['account_amount'] == '43.49'
        assert user1.response_bill_create['in_fee_amount'] == '7.06'
        assert user1.response_bill_create['out_fee_amount'] == '7.06'
        assert user1.response_bill_create['in_amount'] == '50.55'
        assert user1.response_bill_create['out_amount'] == '50.55'
        user2.bill_pay(in_curr='RUB', bill_token=User.bill)
        assert user2.response_bill_pay['exception'] == 'InsufficientFunds'
        admin.set_wallet_amount(balance=100000000000, currency='RUB', email=user2.email)
        user2.bill_pay(in_curr='RUB', bill_token=User.bill)
        assert user2.response_bill_pay == {'fee': '0', 'in_amount': '50.55', 'in_curr': 'RUB', 'out_amount': '50.55',
                                           'out_curr': 'RUB', 'payee': 'AC178', 'status': 'done'}
        user1.get_balances(curr='RUB')
        assert user1.resp_balances['RUB'] == '43.49'
        user2.get_balances(curr='RUB')
        assert user2.resp_balances['RUB'] == '49.45'

    def test_bill_5(self):
        """ Create personal bill on 0.055 LTC with common fee for operation 0.005 LTC and caching by other user.
            Fee by operation for payer. """
        admin.set_wallet_amount(balance=0, currency='LTC', email=user2.email)
        admin.set_wallet_amount(balance=1000000000, currency='LTC', email=user1.email)
        admin.set_fee(tp=40, currency_id=admin.currency['LTC'], add=5000000)
        user2.bill_calc(amount=0.055, currency='LTC')
        assert user2.response_bill_calc['account_amount'] == '0.055'
        assert user2.response_bill_calc['in_amount'] == '0.055'
        assert user2.response_bill_calc['out_amount'] == '0.055'
        assert user2.response_bill_calc['in_fee_amount'] == '0.005'
        assert user2.response_bill_calc['out_fee_amount'] == '0.005'
        user2.bill_create(amount=0.055, curr='LTC', tgt=user1.email)
        assert user2.response_bill_create['account_amount'] == '0.055'
        assert user2.response_bill_create['in_fee_amount'] == '0.005'
        assert user2.response_bill_create['out_fee_amount'] == '0.005'
        assert user2.response_bill_create['in_amount'] == '0.055'
        assert user2.response_bill_create['out_amount'] == '0.055'
        user2.bill_get(lid=user2.bill_lid)
        assert user2.response_bill_get['status'] == 'new'
        user1.bill_pay(in_curr='LTC', bill_token=User.bill)
        assert user1.response_bill_pay == {'fee': '0.005', 'in_amount': '0.055', 'in_curr': 'LTC', 'out_amount': '0.055',
                                           'out_curr': 'LTC', 'payee': 'AC179', 'status': 'done'}
        user2.get_balances(curr='LTC')
        assert user2.resp_balances['LTC'] == '0.055'
        user1.get_balances(curr='LTC')
        assert user1.resp_balances['LTC'] == '0.94'
        user2.bill_get(lid=user2.bill_lid)
        assert user2.response_bill_get['status'] == 'done'

    def test_bill_6(self, _personal_user2_data):
        """ Create common bill on 27 UAH with common fee 10% and 5 UAH and with personal fee 5.03% and 3 UAH,
            cashing by other user with fee by operation for payer. """
        admin.set_wallet_amount(balance=0, currency='UAH', email=user2.email)
        admin.set_wallet_amount(balance=50000000000, currency='UAH', email=user1.email)
        admin.set_fee(tp=40, currency_id=admin.currency['UAH'], add=5000000000, mult=100000000)
        admin.set_fee(tp=40, currency_id=admin.currency['UAH'], add=3000000000, mult=50300000, owner_id=user2.id,
                      is_active=True)
        user2.bill_params(currency='UAH')
        assert user2.response_bill_params == {'in_curr': 'UAH', 'is_convert': False, 'max': '3000', 'min': '0.01',
                                              'in_fee': {'add': '3', 'max': '0', 'method': 'ceil',
                                                         'min': '0', 'mult': '0.0503'},
                                              'out_curr': 'UAH', 'rate': ['1', '1']}
        assert user2.response_bill_params['min_balance_limit'] == '3.02'
        user2.bill_create(amount=27, curr='UAH', tgt=None)
        assert user2.response_bill_create['account_amount'] == '27'
        assert user2.response_bill_create['in_fee_amount'] == '4.36'
        assert user2.response_bill_create['out_fee_amount'] == '4.36'
        assert user2.response_bill_create['in_amount'] == '27'
        assert user2.response_bill_create['out_amount'] == '27'
        user1.bill_pay(in_curr='UAH', bill_token=User.bill)
        assert user1.response_bill_pay == {'fee': '4.36', 'in_amount': '27', 'in_curr': 'UAH', 'out_amount': '27',
                                           'out_curr': 'UAH', 'payee': 'AC179', 'status': 'done'}
        user2.get_balances(curr='UAH')
        assert user2.resp_balances['UAH'] == '27'
        user1.get_balances(curr='UAH')
        assert user1.resp_balances['UAH'] == '18.64'
        admin.set_fee(tp=40, currency_id=admin.currency['UAH'], owner_id=user2.id, is_active=False)

    def test_bill_7(self, _personal_user_data):
        """ Create personal bill on 0.0003 BTC with common fee 25% and 0.0002 UAH and with personal fee 15% and 0.0001 USD,
            cashing by other user with fee by operation bill owner. """
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        admin.set_wallet_amount(balance=400000, currency='BTC', email=user2.email)
        admin.set_fee(tp=40, currency_id=admin.currency['BTC'], add=200000, mult=250000000)
        admin.set_fee(tp=40, currency_id=admin.currency['BTC'], add=100000, mult=150000000, owner_id=user1.id,
                      is_active=True)
        user1.bill_create(amount=0.0003, curr='BTC', tgt=user2.email)
        assert user1.response_bill_create['account_amount'] == '0.000155'
        assert user1.response_bill_create['in_fee_amount'] == '0.000145'
        assert user1.response_bill_create['out_fee_amount'] == '0.000145'
        assert user1.response_bill_create['in_amount'] == '0.0003'
        assert user1.response_bill_create['out_amount'] == '0.0003'
        user2.bill_pay(in_curr='BTC', bill_token=User.bill)
        assert user2.response_bill_pay == {'fee': '0', 'in_amount': '0.0003', 'in_curr': 'BTC', 'out_amount': '0.0003',
                                           'out_curr': 'BTC', 'payee': 'AC178', 'status': 'done'}
        user1.get_balances(curr='BTC')
        assert user1.resp_balances['BTC'] == '1.000155'
        user2.get_balances(curr='BTC')
        assert user2.resp_balances['BTC'] == '0.0001'
        admin.set_fee(tp=40, currency_id=admin.currency['BTC'], owner_id=user1.id, is_active=False)


class TestBillWithExchange:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_bill_8(self):
        """ Create common bill on 3 UAH with cashing other user from USD with exchange fee 1%, without fee for
            operations. """
        admin.set_tech_limits_exchange(tech_min=110000000, tech_max=3000000000000, in_currency='USD', out_currency='UAH')
        admin.set_wallet_amount(balance=0, currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=1000000000, currency='USD', email=user2.email)
        admin.set_rate_exchange(rate=28199900000, fee=10000000, in_currency='USD', out_currency='UAH')
        admin.set_fee(tp=40, currency_id=admin.currency['UAH'])
        user1.bill_create(amount=3, curr='UAH', tgt=None)
        assert user1.response_bill_create['account_amount'] == '3'
        assert user1.response_bill_create['in_fee_amount'] == '0'
        assert user1.response_bill_create['out_fee_amount'] == '0'
        assert user1.response_bill_create['in_amount'] == '3'
        assert user1.response_bill_create['out_amount'] == '3'
        user2.bill_pay(in_curr='USD', bill_token=User.bill)
        assert user2.response_bill_pay['fee'] == '0'
        assert user2.response_bill_pay['in_amount'] == '0.11'
        assert user2.response_bill_pay['in_curr'] == 'USD'
        assert user2.response_bill_pay['out_amount'] == '3'
        assert user2.response_bill_pay['out_curr'] == 'UAH'
        admin.set_tech_limits_exchange(tech_min=10000000, tech_max=3000000000000, in_currency='USD', out_currency='UAH')

    def test_bill_9(self):
        """ Create personal bill on 10 USD with cashing other user from BTC with exchange fee 5.5%, without fee for
            operations. """
        admin.set_wallet_amount(balance=0, currency='USD', email=user2.email)
        admin.set_wallet_amount(balance=1000000000, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=3580654100000, fee=55000000, in_currency='BTC', out_currency='USD')
        admin.set_fee(tp=40, currency_id=admin.currency['USD'])
        user2.bill_create(amount=10, curr='USD', tgt=user1.email)
        assert user2.response_bill_create['account_amount'] == '10'
        assert user2.response_bill_create['tgt'] == 'AC178'
        assert user2.response_bill_create['status'] == 'new'
        user1.bill_pay(in_curr='BTC', bill_token=User.bill)
        assert user1.response_bill_pay['fee'] == '0'
        assert user1.response_bill_pay['in_amount'] == '0.00295533'
        assert user1.response_bill_pay['in_curr'] == 'BTC'
        assert user1.response_bill_pay['out_amount'] == '10'
        assert user1.response_bill_pay['out_curr'] == 'USD'
        user1.bill_get(lid=user2.bill_lid)
        assert user1.response_bill_get['exception'] == 'NotFound'

    def test_bill_10(self):
        """ Create common bill on 55 UAH with cashing other user from RUB with common exchange fee 3%,
            with operations bonus 1 USD = 1%, with common fee by operation 5 UAH and 5.5 %. Fee for bill owner. """
        admin.set_tech_limits_exchange(tech_min=10000000, tech_max=136900000000, in_currency='RUB', out_currency='UAH')
        admin.set_wallet_amount(balance=0, currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=137000000000, currency='RUB', email=user2.email)
        admin.set_rate_exchange(rate=2440200000, fee=30000000, in_currency='RUB', out_currency='UAH')
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=True)
        admin.set_fee(tp=40, currency_id=admin.currency['UAH'], add=5000000000, mult=55000000)
        user1.bill_create(amount=55, curr='UAH', tgt=None)
        assert user1.response_bill_create['account_amount'] == '46.97'
        assert user1.response_bill_create['out_amount'] == '55'
        assert user1.response_bill_create['out_curr'] == 'UAH'
        assert user1.response_bill_create['in_fee_amount'] == '8.03'
        assert user1.response_bill_create['out_fee_amount'] == '8.03'
        assert user1.response_bill_create['tgt'] == 'None'
        user2.bill_pay(in_curr='RUB', bill_token=User.bill)
        assert user2.response_bill_pay['fee'] == '0'
        assert user2.response_bill_pay['in_amount'] == '136.90'
        assert user2.response_bill_pay['in_curr'] == 'RUB'
        assert user2.response_bill_pay['out_amount'] == '55'
        assert user2.response_bill_pay['out_curr'] == 'UAH'
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=False)
        admin.set_tech_limits_exchange(tech_min=10000000, tech_max=3000000000000, in_currency='RUB', out_currency='UAH')

    def test_bill_11(self):
        """ Create personal bill on 2.3 LTC with cashing other user from BTC with common exchange fee 3%,
            with collected bonus 5 points = 1%, with common fee by operation 0.5 LTC and 8%. Fee for payer. """
        admin.set_wallet_amount(balance=0, currency='LTC', email=user2.email)
        admin.set_wallet_amount(balance=29796000, currency='BTC', email=user1.email)
        admin.set_rate_exchange(rate=102191500000, fee=30000000, in_currency='BTC', out_currency='LTC')
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=True)
        admin.set_user_data(user_email=user1.email, is_customfee=False, user=user1, bonus_points=5000000000)
        admin.set_fee(tp=40, currency_id=admin.currency['LTC'], add=500000000, mult=80000000)
        user2.bill_create(amount=2.3, curr='LTC', tgt=user1.email)
        assert user2.response_bill_create['account_amount'] == '2.3'
        assert user2.response_bill_create['in_amount'] == '2.3'
        assert user2.response_bill_create['out_amount'] == '2.3'
        assert user2.response_bill_create['in_fee_amount'] == '0.684'
        assert user2.response_bill_create['out_fee_amount'] == '0.684'
        user1.bill_pay(in_curr='BTC', bill_token=User.bill)
        assert user1.response_bill_pay['exception'] == 'InsufficientFunds'
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        user1.bill_pay(in_curr='BTC', bill_token=User.bill)
        assert user1.response_bill_pay['fee'] == '0.00682992'
        assert user1.response_bill_pay['in_amount'] == '0.02296609'
        assert user1.response_bill_pay['out_amount'] == '2.3'
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=False)

    def test_bill_12(self, _personal_user_data):
        """ Create common bill on 10 USD with cashing other user from UAH, with common exchange fee 5%,
            with operations bonus 1 USD = 1%, with personal exchange fee for payer 3%, with common fee for operations
            8% and 2.15 USD, with personal exchange fee for operations 5% and 1.15 USD. Fee for payer. Fee for operations
            from bill owner. """
        admin.set_wallet_amount(balance=0, currency='USD', email=user2.email)
        admin.set_wallet_amount(balance=350000000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=50000000, in_currency='UAH', out_currency='USD')
        admin.set_bonus_level(name='1usd', points=None, amount=5000000000, surplus=10000000, is_active=True)
        admin.set_fee(tp=40, currency_id=admin.currency['USD'], add=2150000000, mult=80000000)
        admin.set_fee(tp=40, currency_id=admin.currency['USD'], add=1150000000, mult=50000000, owner_id=user2.id,
                      is_active=True)
        admin.set_personal_exchange_fee(is_active=True, in_currency='UAH', out_currency='USD', email=user1.email,
                                        fee=30000000)
        user2.bill_calc(amount=10, currency='USD')
        assert user2.response_bill_calc == {'account_amount': '10', 'equiv_amount': '10', 'in_amount': '10',
                                            'in_fee_amount': '1.65', 'orig_amount': '10', 'out_amount': '10',
                                            'out_fee_amount': '1.65'}
        user2.bill_create(amount=10, curr='USD', tgt=None)
        assert user2.response_bill_create['account_amount'] == '10'
        assert user2.response_bill_create['in_amount'] == '10'
        assert user2.response_bill_create['out_amount'] == '10'
        assert user2.response_bill_create['in_fee_amount'] == '1.65'
        assert user2.response_bill_create['out_fee_amount'] == '1.65'
        user1.bill_pay(in_curr='UAH', bill_token=User.bill)
        assert user1.response_bill_create['fee'] == '47.93'
        assert user2.response_bill_create['in_amount'] == '290.46'
        assert user2.response_bill_create['out_amount'] == '10'
        admin.set_bonus_level(name='1usd', points=None, amount=5000000000, surplus=10000000, is_active=False)
        admin.set_personal_exchange_fee(is_active=False, in_currency='UAH', out_currency='USD', email=user1.email)
        admin.set_fee(tp=40, currency_id=admin.currency['USD'], owner_id=user2.id, is_active=False)

    def test_bill_13(self,):
        """ Create personal order on 1.2 BTC with cashing from LTC, with common exchange fee 5%,
            with collected bonus 5 points = 1 %, with personal exchange fee 3%, with common fee for operation
            3% and 0.003 BTC, with personal fee for operation 1.5 % and 0.002 BTC. Fee for bill owner. """
        admin.set_wallet_amount(balance=0, currency='BTC', email=user1.email)
        admin.set_wallet_amount(balance=150000000000, currency='LTC', email=user2.email)
        admin.set_rate_exchange(rate=102191500000, fee=50000000, in_currency='LTC', out_currency='BTC')
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=True)
        admin.set_user_data(user_email=user2.email, is_customfee=True, user=user2, bonus_points=5000000000)
        admin.set_personal_exchange_fee(is_active=True, in_currency='LTC', out_currency='BTC', email=user2.email,
                                        fee=30000000)
        admin.set_fee(tp=40, currency_id=admin.currency['BTC'], add=2150000000, mult=80000000)
        admin.set_fee(tp=40, currency_id=admin.currency['BTC'], add=1150000000, mult=50000000, owner_id=user1.id,
                      is_active=True)
        user1.bill_params(currency='BTC')
        assert user1.bill_params == {'in_curr': 'BTC', 'is_convert': False, 'max': '3', 'min': '0.00028',
                                     'in_fee': {'add': '0.0002', 'max': '0', 'method': 'ceil', 'min': '0',
                                                'mult': '0.25'},
                                     'out_curr': 'BTC', 'rate': ['1', '1']}
        user1.bill_calc(amount=1.2, currency='BTC')
        assert user1.response_bill_calc['account_amount'] == '1.18'
        assert user1.response_bill_calc['in_amount'] == '1.2'
        assert user1.response_bill_calc['out_amount'] == '1.2'
        assert user1.response_bill_calc['in_fee_amount'] == '0.02'
        assert user1.response_bill_calc['out_fee_amount'] == '0.02'
        user1.bill_create(amount=1.2, curr='BTC', tgt=user2.email)
        assert user1.response_bill_create['account_amount'] == '1.18'
        assert user1.response_bill_create['in_amount'] == '1.2'
        assert user1.response_bill_create['out_amount'] == '1.2'
        assert user1.response_bill_create['in_fee_amount'] == '0.02'
        assert user1.response_bill_create['out_fee_amount'] == '0.02'
        user2.bill_pay(in_curr='LTC', bill_token=User.bill)
        assert user2.response_bill_pay['fee'] == '0'
        assert user2.response_bill_pay['in_amount'] == '126.308694'
        assert user2.response_bill_pay['out_amount'] == '1.2'
        admin.set_personal_exchange_fee(is_active=False, in_currency='LTC', out_currency='BTC', email=user2.email)
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=False)
        admin.set_fee(tp=40, currency_id=admin.currency['BTC'], owner_id=user1.id, is_active=False)
        admin.set_user_data(user_email=user2.email, is_customfee=False, user=user2, bonus_points=0)


class TestRevokeBill:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_bill_14(self):
        """ Revoke created common bill. Fee for bill owner. """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user2.email)
        admin.set_fee(tp=40, currency_id=admin.currency['UAH'])
        user1.bill_create(amount=1, curr='UAH', tgt=None)
        user1.bill_revoke(lid=user1.bill_lid)
        assert user1.response_bill_revoke['status'] == 'reject'
        user1.bill_get(lid=user1.bill_lid)
        assert user1.response_bill_get['status'] == 'reject'
        user2.bill_pay(in_curr='UAH', bill_token=User.bill)
        assert user2.response_bill_pay['exception'] == 'InvalidToken'

    def test_bill_15(self):
        """ Revoke created personal bill. Fee for bill owner. """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user2.email)
        admin.set_fee(tp=40, currency_id=admin.currency['UAH'])
        user1.bill_create(amount=1, curr='UAH', tgt=user2.email)
        user1.bill_revoke(lid=user1.bill_lid)
        user1.bill_list('UAH')
        assert user1.response_bill_list['data'][0]['status'] == 'reject'
        user2.bill_pay(in_curr='UAH', bill_token=User.bill)
        assert user2.response_bill_pay['exception'] == 'InvalidToken'

    def test_bill_16(self):
        """ Revoke created common bill. Fee for payer. """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user1.email)
        admin.set_fee(tp=40, currency_id=admin.currency['UAH'])
        user2.bill_create(amount=1, curr='UAH', tgt=None)
        user2.bill_revoke(lid=user2.bill_lid)
        assert user2.response_bill_revoke['status'] == 'reject'
        user1.bill_pay(in_curr='UAH', bill_token=User.bill)
        assert user1.response_bill_pay['exception'] == 'InvalidToken'

    def test_bill_17(self):
        """ Revoke created personal bill. Fee for payer. """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user1.email)
        admin.set_fee(tp=40, currency_id=admin.currency['UAH'])
        user2.bill_create(amount=1, curr='UAH', tgt=user1.email)
        user2.bill_revoke(lid=user2.bill_lid)
        user1.bill_pay(in_curr='UAH', bill_token=User.bill)
        assert user1.response_bill_pay['exception'] == 'InvalidToken'

    def test_bill_18(self):
        """ Two times revoke one bill. """
        user1.bill_create(amount=1, curr='UAH', tgt=None)
        admin.set_fee(tp=40, currency_id=admin.currency['UAH'])
        user1.bill_revoke(lid=user1.bill_lid)
        user1.bill_revoke(lid=user1.bill_lid)
        assert user1.response_bill_revoke['exception'] == 'Forbidden'

    def test_bill_19(self):
        """ Revoke cashed bill. """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user2.email)
        admin.set_fee(tp=40, currency_id=admin.currency['UAH'])
        user1.bill_create(amount=1, curr='UAH', tgt=None)
        user2.bill_pay(in_curr='UAH', bill_token=User.bill)
        user1.bill_revoke(lid=user1.bill_lid)
        assert user1.response_bill_revoke['exception'] == 'Forbidden'


class TestWrongBill:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_bill_20(self):
        """ Create bill with amount less then tech min by currency table. """
        admin.set_currency_data(currency='UAH', admin_min=6010000000, admin_max=3000000000000)
        admin.set_fee(tp=40, currency_id=admin.currency['UAH'], add=3000000000, mult=100000000)
        user1.bill_create(curr='UAH', amount=10, tgt=None)
        assert user1.response_bill_create['exception'] == 'AmountTooSmall'
        admin.set_currency_data(currency='UAH', admin_min=10000000, admin_max=3000000000000)

    def test_bill_21(self):
        """ Create bill with amount more then tech max by currency table. """
        admin.set_fee(tp=40, currency_id=admin.currency['UAH'], add=3000000000, mult=100000000)
        user1.bill_create(curr='UAH', amount=3340, tgt=None)
        assert user1.response_bill_create['exception'] == 'AmountTooBig'

    def test_bill_22(self):
        """ Create bill and cashing him from curr, amount less then tech min exchange table. """
        admin.set_tech_limits_exchange(tech_min=120000000, tech_max=3000000000000, in_currency='USD', out_currency='UAH')
        admin.set_wallet_amount(balance=1000000000, currency='USD', email=user2.email)
        admin.set_rate_exchange(rate=28199900000, fee=10000000, in_currency='USD', out_currency='UAH')
        admin.set_fee(tp=40, currency_id=admin.currency['UAH'])
        user1.bill_create(amount=3, curr='UAH', tgt=None)
        user2.bill_pay(in_curr='USD', bill_token=User.bill)
        assert user2.response_bill_pay['exception'] == 'AmountTooSmall'
        admin.set_tech_limits_exchange(tech_min=10000000, tech_max=3000000000000, in_currency='USD',
                                       out_currency='UAH')

    def test_bill_23(self):
        """ Create bill and cashing him from curr, amount more then max exchange table. """
        admin.set_tech_limits_exchange(tech_min=10000000, tech_max=100000000, in_currency='USD', out_currency='UAH')
        admin.set_wallet_amount(balance=1000000000, currency='USD', email=user2.email)
        admin.set_rate_exchange(rate=28199900000, fee=10000000, in_currency='USD', out_currency='UAH')
        admin.set_fee(tp=40, currency_id=admin.currency['UAH'])
        user1.bill_create(amount=3, curr='UAH', tgt=None)
        user2.bill_pay(in_curr='USD', bill_token=User.bill)
        assert user2.response_bill_pay['exception'] == 'AmountTooBig'
        admin.set_tech_limits_exchange(tech_min=10000000, tech_max=3000000000000, in_currency='USD',
                                       out_currency='UAH')

    def test_bill_24(self):
        """ Create bill with amount less then seed currency. """
        user1.bill_create(amount=0.009, curr='UAH', tgt=None)
        assert user1.response_bill_create['exception'] == 'InvalidAmountFormat'

    def test_bill_25(self):
        """ Create personal bill for not real user. """
        user1.bill_create(amount=1, curr='UAH', tgt='555@mailinator.com')
        assert user1.response_bill_create['exception'] == 'InvalidContact'

    def test_bill_26(self):
        """ Create bill without amount parameter. """
        response = requests.post(url=user1.wapi_url,
                                 json={'method': 'create', 'model': 'bill',
                                       'data': {'externalid': user1.ex_id(), 'curr': 'UAH',
                                                'tgt': None}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'TypeError'

    def test_bill_27(self):
        """ Cashing personal bill wrong payer, and two times cashing one bill by payer. """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user2.email)
        admin.set_fee(tp=40, currency_id=admin.currency['UAH'])
        user1.bill_create(curr='UAH', amount=0.5, tgt=user2.email)
        user3.bill_pay(in_curr='UAH', bill_token=User.bill)
        assert user3.response_bill_pay['exception'] == 'Unauth'
        user2.bill_pay(in_curr='UAH', bill_token=User.bill)
        user2.bill_pay(in_curr='UAH', bill_token=User.bill)
        assert user2.response_bill_pay['exception'] == 'InvalidToken'

    def test_bill_28(self):
        """ Cashing common and personal bill by owner. """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user1.email)
        admin.set_fee(tp=40, currency_id=admin.currency['UAH'])
        user1.bill_create(curr='UAH', amount=1, tgt=user1.email)
        user1.bill_pay(in_curr='UAH', bill_token=User.bill)
        assert user1.response_bill_pay['status'] == 'done'
        user1.bill_create(curr='UAH', amount=1, tgt=None)
        user1.bill_pay(in_curr='UAH', bill_token=User.bill)
        assert user1.response_bill_pay['status'] == 'done'
        user1.bill_create(curr='UAH', amount=1, tgt=user2.email)
        user1.bill_pay(in_curr='UAH', bill_token=User.bill)
        assert user1.response_bill_pay['exception'] == 'InvalidToken'
        user1.get_balances(curr='UAH')
        assert user1.resp_balances['UAH'] == '1'

    def test_bill_29(self):
        """ Create bill without externalid parameter. """
        response = requests.post(url=user1.wapi_url,
                                 json={'method': 'create', 'model': 'bill',
                                       'data': {'amount': 1, 'curr': 'UAH',
                                                'tgt': None}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'TypeError'

    def test_bill_30(self):
        """ Create bill with not real currency. """
        user1.bill_create(curr='UHA', amount=1, tgt=None)
        assert user1.response_bill_create['exception'] == 'InvalidCurrency'

    @pytest.mark.skip
    def test_bill_31(self):
        """ Create bill with not active currency. """
        user1.bill_create(curr='BCH', amount=0.0001)
        assert user1.response_bill_create['exception'] == 'InvalidCurrency'

    def test_bill_32(self):
        """ Create and cashing bill without user token. """
        response = requests.post(url=user1.wapi_url,
                                 json={'method': 'create', 'model': 'bill',
                                       'data': {'externalid': user1.ex_id(), 'amount': 1, 'curr': 'UAH',
                                                'tgt': None}}, verify=False)
        assert json.loads(response.text)['exception'] == 'Unauth'
        admin.set_wallet_amount(1000000000, currency='UAH', email=user2.email)
        admin.set_fee(tp=40, currency_id=admin.currency['UAH'])
        user1.bill_create(curr='UAH', amount=0.01)
        response = requests.post(url=user2.wapi_url,
                                 json={'method': 'pay', 'model': 'bill',
                                       'data': {'in_curr': 'UAH', 'token': User.bill}},
                                 verify=False)
        assert json.loads(response.text)['exception'] == 'Unauth'

    @pytest.mark.skip  # excess parameter in data is ok
    def test_bill_33(self):
        """ Create and cashing bill with excess parameter. """
        response = requests.post(url=user1.wapi_url,
                                 json={'method': 'create', 'model': 'bill',
                                       'data': {'externalid': user1.ex_id(), 'amount': 1, 'curr': 'UAH',
                                                'tgt': None, 'psw': 'abc'}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'TypeError'
        admin.set_wallet_amount(1000000000, currency='UAH', email=user2.email)
        admin.set_fee(tp=40, currency_id=admin.currency['UAH'])
        user1.bill_create(curr='UAH', amount=0.01)
        response = requests.post(url=user2.wapi_url,
                                 json={'method': 'pay', 'model': 'bill',
                                       'data': {'in_curr': 'UAH', 'token': User.bill, 'psw': 'abc'}},
                                 headers=user2.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'TypeError'

    def test_bill_34(self):
        """ Cashing with exchange from not real currency. """
        user1.bill_create(curr='UAH', amount=1)
        user2.bill_pay(in_curr='UDS', bill_token=User.bill)
        assert user2.response_bill_pay['exception'] == 'InvalidCurrency'

    @pytest.mark.skip # not active currency in system
    def test_bill_35(self):
        """ Cashing with exchange from not active currency. """
        user1.bill_create(curr='UAH', amount=1)
        user2.bill_pay(in_curr='BCH', bill_token=User.bill)
        assert user2.response_bill_pay['exception'] == 'InvalidCurrency'

    def test_bill_36(self):
        """ Cashing with exchange by not active pair. """
        user1.bill_create(curr='BTC', amount=1)
        user2.bill_pay(in_curr='USD', bill_token=User.bill)
        assert user2.response_bill_pay['exception'] == 'UnavailExchange'

