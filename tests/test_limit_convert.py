import pytest
import time
from tests.tools import bl, orig
from math import floor, ceil



class TestLimitExchange:
    """ Testing creating and finishing limit convert. """


    def test_0(self, create_session):
        """ Warm """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session


    def test_1(self, _disable_custom_fee):
        """ Baying USD and UAH, course in admin and in requests are equal and course in request
            more than course in admin. """
        admin.set_wallet_amount(balance=bl(100), currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=bl(28.1999), fee=0, in_currency='UAH', out_currency='USD')
        admin.set_rate_exchange(rate=bl(28.1999), fee=0, in_currency='USD', out_currency='UAH')
        user1.limit_convert(in_amount=28.2, out_amount=1, in_curr='UAH', out_curr='USD')
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'
        user1.get_balances(curr='UAH')
        assert user1.resp_balances['UAH'] == '100'
        user1.get_balances(curr='USD')
        assert user1.resp_balances['USD'] == '0'
        admin.set_wallet_amount(balance=bl(10), currency='USD', email=user1.email)
        user1.limit_convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD', rate='28.1999',
                            rate_isdir=True)
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'
        user1.limit_convert(in_amount=None, out_amount='28.19', in_curr='USD', out_curr='UAH', rate='28.1999',
                            rate_isdir=False)
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'
        admin.set_rate_exchange(rate=bl(28.1999), fee=10000000, in_currency='UAH', out_currency='USD')
        admin.set_rate_exchange(rate=bl(28.1999), fee=10000000, in_currency='USD', out_currency='UAH')
        user1.limit_convert(in_amount=28.49, out_amount=1, in_curr='UAH', out_curr='USD')
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'
        user1.limit_convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD', rate='28.48191', rate_isdir=True)
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'
        user1.limit_convert(in_amount=None, out_amount=27.91, in_curr='USD', out_curr='UAH', rate='27.91789',
                            rate_isdir=False)
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'
        admin.set_bonus_level(name='1usd', points=None, amount=bl(1), surplus=10000000, is_active=True)
        user1.limit_convert(in_amount=28.77, out_amount=1.02, in_curr='UAH', out_curr='USD')
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'
        user1.limit_convert(in_amount=None, out_amount=1.02, in_curr='UAH', out_curr='USD', rate='28.1999', rate_isdir=True)
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'
        user1.limit_convert(in_amount=None, out_amount=28.76, in_curr='USD', out_curr='UAH', rate='28.1999',
                            rate_isdir=False)
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'
        admin.set_user_data(user_email=user1.email, is_customfee=True, user=user1)
        admin.set_personal_exchange_fee(is_active=True, in_currency='UAH', out_currency='USD', email=user1.email, fee=5000000)
        admin.set_personal_exchange_fee(is_active=True, in_currency='USD', out_currency='UAH', email=user1.email,
                                        fee=5000000)
        user1.limit_convert(in_amount=15.89, out_amount=0.56, in_curr='UAH', out_curr='USD')
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'
        user1.limit_convert(in_amount=None, out_amount=0.56, in_curr='UAH', out_curr='USD', rate='28.34091', rate_isdir=True)
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'
        user1.limit_convert(in_amount=None, out_amount=28.05, in_curr='USD', out_curr='UAH', rate='28.05889',
                            rate_isdir=False)
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'
        admin.set_bonus_level(name='1usd', is_active=False)


    def test_2(self):
        """ Baying USD and UAH, course in request better then in admin on 0.01,
            course in admin better then in uplink's course. Exchange wasn't finished. Revoke order. """
        admin.set_wallet_amount(balance=bl(100), currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user1.email)
        admin.uplink_sync()
        uplink_dr_course = ceil(admin.get_rate(in_curr=admin.currency['UAH'], out_curr=admin.currency['USD']) * 100) / 100
        uplink_rev_course = floor(admin.get_rate(in_curr=admin.currency['USD'], out_curr=admin.currency['UAH']) * 100) / 100
        admin.set_rate_exchange(fee=0, rate=bl(uplink_dr_course - 0.01), in_currency='UAH', out_currency='USD')
        admin.set_rate_exchange(fee=0, rate=bl(uplink_rev_course + 0.01), in_currency='USD', out_currency='UAH')
        user1.limit_convert(in_amount=orig(bl(uplink_dr_course) - bl(0.02)), out_amount=1, in_curr='UAH', out_curr='USD')
        assert float(user1.resp_limit_convert['in_amount']) == orig(bl(uplink_dr_course) - bl(0.02))
        admin.uplink_sync()
        user1.get_balances('UAH')
        assert float(user1.resp_balances['UAH']) == orig(bl(100) - (bl(uplink_dr_course) - bl(0.02)))
        user1.get_balances('USD')
        assert user1.resp_balances['USD'] == '0'
        user1.revoke_convert(lid=user1.limit_order_lid)
        user1.get_balances('UAH')
        assert user1.resp_balances['UAH'] == '100'
        user1.get_balances('USD')
        assert user1.resp_balances['USD'] == '0'
        assert admin.get_order(lid=user1.limit_order_lid)['status'] == 120
        ''' CREATING LIMIT ORDER BY OUT_AMOUNT AND RATE. '''
        user1.limit_convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD',
                            rate=str(orig(bl(uplink_dr_course) - bl(0.02))), rate_isdir=True)
        assert float(user1.resp_limit_convert['in_amount']) == orig(bl(uplink_dr_course) - bl(0.02))
        admin.uplink_sync()
        user1.get_balances('UAH')
        assert float(user1.resp_balances['UAH']) == orig(bl(100) - (bl(uplink_dr_course) - bl(0.02)))
        user1.get_balances('USD')
        assert user1.resp_balances['USD'] == '0'
        user1.revoke_convert(lid=user1.limit_order_lid)
        user1.get_balances('UAH')
        assert user1.resp_balances['UAH'] == '100'
        user1.get_balances('USD')
        assert user1.resp_balances['USD'] == '0'
        assert admin.get_order(lid=user1.limit_order_lid)['status'] == 120
        admin.set_wallet_amount(balance=bl(10), currency='USD', email=user1.email)
        user1.limit_convert(in_amount=None, out_amount=orig(bl(uplink_rev_course) + bl(0.02)), in_curr='USD',
                            out_curr='UAH', rate=str(orig(bl(uplink_rev_course) + bl(0.02))), rate_isdir=False)
        assert user1.resp_limit_convert['in_amount'] == '1'
        admin.uplink_sync()
        assert admin.get_order(lid=user1.limit_order_lid)['status'] == 0
        user1.revoke_convert(lid=user1.limit_order_lid)


    def test_3(self):
        """ Baying: create three late order and finished firsts two of them when course changed
            and equal with second order. Last order not finished and revoked. """
        admin.set_wallet_amount(balance=bl(200), currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user1.email)
        admin.uplink_sync()
        uplink_dr_course = ceil(admin.get_rate(in_curr=admin.currency['UAH'], out_curr=admin.currency['USD']) * 100) / 100
        uplink_rev_course = floor(admin.get_rate(in_curr=admin.currency['USD'], out_curr=admin.currency['UAH']) * 100) / 100
        admin.set_rate_exchange(fee=0, rate=bl(uplink_dr_course + 0.02), in_currency='UAH', out_currency='USD')
        user1.limit_convert(in_amount=orig(bl(uplink_dr_course) + bl(0.01)), out_amount=1, in_curr='UAH', out_curr='USD')
        user1.limit_convert(in_amount=uplink_dr_course, out_amount=1, in_curr='UAH', out_curr='USD')
        user1.limit_convert(in_amount=orig(bl(uplink_dr_course) - bl(0.01)), out_amount=1, in_curr='UAH', out_curr='USD')
        admin.uplink_sync()
        user1.get_balances('UAH')
        assert float(user1.resp_balances['UAH']) == round(orig(bl(200) - ((bl(uplink_dr_course) + bl(0.01))
                                                          + bl(uplink_dr_course) + (bl(uplink_dr_course) - bl(0.01)))), 2)
        user1.get_balances('USD')
        assert user1.resp_balances['USD'] == '2'
        assert admin.get_order(lid=user1.limit_order_lid)['status'] == 0
        user1.revoke_convert(lid=user1.limit_order_lid)
        assert admin.get_order(lid=user1.limit_order_lid)['status'] == 120
        ''' CREATING LIMIT ORDER BY OUT_AMOUNT AND RATE. '''
        admin.set_rate_exchange(fee=0, rate=bl(uplink_dr_course + 0.02), in_currency='UAH', out_currency='USD')
        user1.limit_convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD',
                            rate=str(orig(bl(uplink_dr_course) + bl(0.01))), rate_isdir=True)
        assert user1.resp_limit_convert['in_amount'] == str(orig(bl(uplink_dr_course) + bl(0.01)))
        assert user1.resp_limit_convert['out_amount'] == '1'
        user1.limit_convert(in_amount=None, out_amount=1.3, in_curr='UAH', out_curr='USD',
                            rate=str(uplink_dr_course), rate_isdir=True)
        assert user1.resp_limit_convert['in_amount'] == str(ceil(uplink_dr_course * 1.3 * 100) / 100)
        assert user1.resp_limit_convert['out_amount'] == '1.3'
        user1.limit_convert(in_amount=None, out_amount=0.7, in_curr='UAH', out_curr='USD',
                            rate=str(orig(bl(uplink_dr_course) - bl(0.01))), rate_isdir=True)
        assert user1.resp_limit_convert['in_amount'] == str(ceil(orig(bl(uplink_dr_course) - bl(0.01)) * 0.7 * 100) / 100)
        assert user1.resp_limit_convert['out_amount'] == '0.7'
        admin.uplink_sync()
        user1.get_balances('USD')
        assert user1.resp_balances['USD'] == '4.3'
        assert admin.get_order(lid=user1.limit_order_lid)['status'] == 0
        user1.revoke_convert(lid=user1.limit_order_lid)
        assert admin.get_order(lid=user1.limit_order_lid)['status'] == 120
        admin.set_rate_exchange(fee=0, rate=bl(uplink_rev_course - 0.02), in_currency='USD', out_currency='UAH')
        admin.set_wallet_amount(balance=bl(10), currency='USD', email=user1.email)
        user1.limit_convert(in_amount=None, out_amount=orig(bl(uplink_rev_course) - bl(0.01)), in_curr='USD',
                            out_curr='UAH', rate=str(orig(bl(uplink_rev_course) - bl(0.01))), rate_isdir=False)
        assert user1.resp_limit_convert['in_amount'] == '1'
        assert user1.resp_limit_convert['out_amount'] == str(orig(bl(uplink_rev_course) - bl(0.01)))
        user1.limit_convert(in_amount=None, out_amount=35, in_curr='USD',
                            out_curr='UAH', rate=str(uplink_rev_course), rate_isdir=False)
        assert user1.resp_limit_convert['in_amount'] == str(ceil(35 / uplink_rev_course * 100) / 100)
        assert user1.resp_limit_convert['out_amount'] == '35'
        user1.limit_convert(in_amount=None, out_amount=orig(bl(uplink_rev_course) + bl(0.01)), in_curr='USD',
                            out_curr='UAH', rate=str(orig(bl(uplink_rev_course) + bl(0.01))), rate_isdir=False)
        assert user1.resp_limit_convert['in_amount'] == '1'
        assert user1.resp_limit_convert['out_amount'] == str(orig(bl(uplink_rev_course) + bl(0.01)))
        admin.uplink_sync()
        assert admin.get_order(lid=user1.limit_order_lid)['status'] == 0
        user1.revoke_convert(lid=user1.limit_order_lid)


    def test_4(self, _disable_custom_fee):
        """ Buying: create late order with exchange fee, bonus and personal exchange fee. """
        admin.set_wallet_amount(balance=bl(400), currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user1.email)
        admin.uplink_sync()
        uplink_course = ceil(admin.get_rate(in_curr=admin.currency['UAH'], out_curr=admin.currency['USD']) * 100) / 100
        admin.set_rate_exchange(fee=10000000, rate=bl(uplink_course + 0.02), in_currency='UAH', out_currency='USD')

        in_amount = ceil(orig((bl(uplink_course) + bl(uplink_course * 0.01) + bl(0.01))) * 100) / 100

        user1.limit_convert(in_amount=in_amount, out_amount=1, in_curr='UAH', out_curr='USD')
        user1.limit_convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD', rate=str(in_amount), rate_isdir=True)
        admin.uplink_sync()
        user1.get_balances('USD')
        assert user1.resp_balances['USD'] == '2'
        admin.set_bonus_level(name='1usd', points=None, amount=bl(1), surplus=10000000, is_active=True)
        admin.set_rate_exchange(fee=20000000, rate=bl(uplink_course + 0.02), in_currency='UAH', out_currency='USD')
        user1.limit_convert(in_amount=in_amount, out_amount=1, in_curr='UAH', out_curr='USD')
        user1.limit_convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD', rate=str(in_amount), rate_isdir=True)
        admin.uplink_sync()
        user1.get_balances('USD')
        assert user1.resp_balances['USD'] == '4'
        admin.set_user_data(user_email=user1.email, is_customfee=True, user=user1)
        admin.set_personal_exchange_fee(in_currency='UAH', out_currency='USD', is_active=True, fee=10000000, email=user1.email)
        admin.set_personal_exchange_fee(in_currency='USD', out_currency='UAH', is_active=True, fee=10000000, email=user1.email)
        admin.set_rate_exchange(fee=30000000, rate=bl(uplink_course + 0.02), in_currency='UAH', out_currency='USD')
        user1.limit_convert(in_amount=in_amount, out_amount=1, in_curr='UAH', out_curr='USD')
        user1.limit_convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD', rate=str(in_amount), rate_isdir=True)
        admin.uplink_sync()
        user1.get_balances('USD')
        assert user1.resp_balances['USD'] == '6'
        admin.set_bonus_level(name='1usd', points=None, amount=bl(1), surplus=10000000, is_active=False)


    def test_5(self, _disable_custom_fee):
        """ Create exchange UAH to USD: late order not finished - course from uplink + all fee from admin is more
            than requests creating order. """
        admin.set_wallet_amount(balance=bl(100), currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user1.email)
        admin.set_bonus_level(name='1usd', points=None, amount=bl(1), surplus=10000000, is_active=True)
        admin.set_user_data(user_email=user1.email, is_customfee=True, user=user1)
        admin.set_personal_exchange_fee(in_currency='UAH', out_currency='USD', is_active=True, fee=10000000, email=user1.email)
        admin.uplink_sync()
        uplink_course = ceil(admin.get_rate(in_curr=admin.currency['UAH'], out_curr=admin.currency['USD']) * 100) / 100
        admin.set_rate_exchange(fee=30000000, rate=bl(uplink_course - 0.01), in_currency='UAH', out_currency='USD')
        in_amount = ceil(orig((bl(uplink_course) + (bl(uplink_course * 0.01))) - bl(0.02)) * 100) / 100
        user1.limit_convert(in_amount=in_amount, out_amount=1, in_curr='UAH', out_curr='USD')
        admin.uplink_sync()
        assert admin.get_order(lid=user1.limit_order_lid)['status'] == 0
        user1.revoke_convert(lid=user1.limit_order_lid)
        admin.set_rate_exchange(fee=30000000, rate=bl(uplink_course - 0.01), in_currency='UAH', out_currency='USD')
        user1.limit_convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD', rate=str(in_amount),
                            rate_isdir=True)
        admin.uplink_sync()
        user1.get_balances('USD')
        assert user1.resp_balances['USD'] == '0'
        assert admin.get_order(lid=user1.limit_order_lid)['status'] == 0
        user1.revoke_convert(lid=user1.limit_order_lid)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=False)


    def test_6(self, _disable_custom_fee):
        """ Selling: sell USD and UAH, course in admin and in requests are equal,
            course in admin is better than in request. """
        admin.set_wallet_amount(balance=bl(2), currency='USD', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='UAH', email=user1.email)
        admin.set_rate_exchange(rate=bl(28.1999), fee=0, in_currency='USD', out_currency='UAH')
        user1.limit_convert(in_amount=1, out_amount=28.19, in_curr='USD', out_curr='UAH')
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'
        user1.get_balances(curr='USD')
        assert user1.resp_balances['USD'] == '2'
        user1.get_balances(curr='UAH')
        assert user1.resp_balances['UAH'] == '0'
        admin.set_rate_exchange(rate=bl(28.1999), fee=10000000, in_currency='USD', out_currency='UAH')
        user1.limit_convert(in_amount=1, out_amount=27.91, in_curr='USD', out_curr='UAH')
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'
        admin.set_bonus_level(name='1usd', points=None, amount=bl(1), surplus=10000000, is_active=True)
        user1.limit_convert(in_amount=1, out_amount=None, in_curr='USD', out_curr='UAH', rate='28.1999', rate_isdir=False)
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'
        admin.set_user_data(user_email=user1.email, is_customfee=True, user=user1)
        admin.set_personal_exchange_fee(is_active=True, in_currency='USD', out_currency='UAH', email=user1.email,
                                        fee=5000000)
        user1.limit_convert(in_amount=0.56, out_amount=15.70, in_curr='USD', out_curr='UAH')
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'
        user1.limit_convert(in_amount=0.56, out_amount=None, in_curr='USD', out_curr='UAH', rate='28.05889',
                            rate_isdir=False)
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'
        admin.set_bonus_level(name='1usd', is_active=False)


    def test_7(self):
        """ Selling: sell UAH and USD, course in request better then in admin on 0.01, course in admin better
            then in uplink's course. Exchange wasn't finished. Revoke order. """
        admin.set_wallet_amount(balance=bl(1), currency='USD', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='UAH', email=user1.email)
        admin.uplink_sync()
        uplink_dr_course = ceil(admin.get_rate(in_curr=admin.currency['UAH'], out_curr=admin.currency['USD']) * 100) / 100
        uplink_rev_course = floor(admin.get_rate(in_curr=admin.currency['USD'], out_curr=admin.currency['UAH']) * 100) / 100
        admin.set_rate_exchange(fee=0, rate=bl(uplink_rev_course + 0.01), in_currency='USD', out_currency='UAH')
        admin.set_rate_exchange(fee=0, rate=bl(uplink_dr_course - 0.01), in_currency='UAH', out_currency='USD')
        user1.limit_convert(in_amount=1, out_amount=None, in_curr='USD', out_curr='UAH',
                            rate=str(orig(bl(uplink_rev_course) + bl(0.02))), rate_isdir=False)
        admin.uplink_sync()
        user1.get_balances('USD')
        assert user1.resp_balances['USD'] == '0'
        user1.get_balances('UAH')
        assert user1.resp_balances['UAH'] == '0'
        user1.revoke_convert(lid=user1.limit_order_lid)
        user1.get_balances('USD')
        assert user1.resp_balances['USD'] == '1'
        user1.get_balances('UAH')
        assert user1.resp_balances['UAH'] == '0'
        admin.set_wallet_amount(balance=bl(100), currency='UAH', email=user1.email)
        user1.limit_convert(in_amount=orig(bl(uplink_dr_course) - bl(0.02)), out_amount=None, in_curr='UAH', out_curr='USD',
                            rate=str(orig(bl(uplink_dr_course) - bl(0.02))), rate_isdir=True)
        admin.uplink_sync()
        assert admin.get_order(lid=user1.limit_order_lid)['status'] == 0
        user1.revoke_convert(lid=user1.limit_order_lid)


    def test_8(self):
        """ Reverse exchange: create three late order and finished firsts two of them when course changed
            and equal with second order. Last order not finished and revoked. """
        admin.set_wallet_amount(balance=bl(3), currency='USD', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='UAH', email=user1.email)
        admin.uplink_sync()
        uplink_course = floor(admin.get_rate(in_curr=admin.currency['USD'], out_curr=admin.currency['UAH']) * 100) / 100
        admin.set_rate_exchange(fee=0, rate=bl(uplink_course - 0.02), in_currency='USD', out_currency='UAH')
        user1.limit_convert(in_amount=1, out_amount=orig(bl(uplink_course) - bl(0.01)), in_curr='USD', out_curr='UAH')
        user1.limit_convert(in_amount=1, out_amount=uplink_course, in_curr='USD', out_curr='UAH')
        user1.limit_convert(in_amount=1, out_amount=orig(bl(uplink_course) + bl(0.01)), in_curr='USD', out_curr='UAH')
        admin.uplink_sync()
        user1.get_balances('USD')
        assert user1.resp_balances['USD'] == '0'
        user1.get_balances('UAH')
        assert float(user1.resp_balances['UAH']) == orig(bl(uplink_course) + (bl(uplink_course) - bl(0.01)))
        assert admin.get_order(lid=user1.limit_order_lid)['status'] == 0
        user1.revoke_convert(lid=user1.limit_order_lid)
        assert admin.get_order(lid=user1.limit_order_lid)['status'] == 120
        user1.get_balances('USD')
        assert user1.resp_balances['USD'] == '1'
        admin.set_wallet_amount(balance=bl(3.2), currency='USD', email=user1.email)
        admin.set_rate_exchange(fee=0, rate=bl(uplink_course - 0.02), in_currency='USD', out_currency='UAH')
        user1.limit_convert(in_amount=1, out_amount=None, in_curr='USD',
                            out_curr='UAH', rate=str(orig(bl(uplink_course) - bl(0.01))), rate_isdir=False)
        assert user1.resp_limit_convert['in_amount'] == '1'
        assert user1.resp_limit_convert['out_amount'] == str(orig(bl(uplink_course) - bl(0.01)))
        user1.limit_convert(in_amount=1.7, out_amount=None, in_curr='USD', out_curr='UAH',
                            rate=str(uplink_course), rate_isdir=False)
        assert user1.resp_limit_convert['in_amount'] == '1.7'
        assert user1.resp_limit_convert['out_amount'] == str(floor(uplink_course * 1.7 * 100) / 100)
        user1.limit_convert(in_amount=0.5, out_amount=None, in_curr='USD',
                            out_curr='UAH', rate=str(orig(bl(uplink_course) + bl(0.01))), rate_isdir=False)
        assert user1.resp_limit_convert['in_amount'] == '0.5'
        assert user1.resp_limit_convert['out_amount'] == str(floor(orig(bl(uplink_course) + bl(0.01)) * 0.5 * 100) / 100)
        admin.uplink_sync()
        user1.get_balances('USD')
        assert user1.resp_balances['USD'] == '0'
        assert admin.get_order(lid=user1.limit_order_lid)['status'] == 0
        user1.revoke_convert(lid=user1.limit_order_lid)


    def test_9(self):
        """ Create two late orders with expiry data. First of them was finished and second was rejected by time. """
        admin.set_wallet_amount(balance=bl(200), currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user1.email)
        admin.uplink_sync()
        uplink_course = ceil(admin.get_rate(in_curr=admin.currency['UAH'], out_curr=admin.currency['USD']) * 100) / 100
        admin.set_rate_exchange(fee=0, rate=bl(uplink_course + 0.02), in_currency='UAH', out_currency='USD')
        user1.limit_convert(in_amount=orig(bl(uplink_course) + bl(0.01)), out_amount=1, in_curr='UAH',
                            out_curr='USD', rate=None, rate_isdir=None, expiry=None)
        user1.limit_convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD', rate=str(uplink_course),
                            rate_isdir=True, expiry='5s')
        time.sleep(80)
        admin.uplink_sync()
        assert admin.get_order(lid=user1.limit_order_lid)['status'] == 120
        user1.get_balances('USD')
        assert user1.resp_balances['USD'] == '1'


    def test_10(self):
        """ Create order with out_amount and long rate: UAH to USD, checking rounding. """
        admin.set_wallet_amount(balance=bl(200), currency='UAH', email=user1.email)
        admin.set_rate_exchange(fee=0, rate=28199900000, in_currency='UAH', out_currency='USD')
        user1.limit_convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD', rate='28.199978',
                            rate_isdir=True)
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'
        user1.revoke_convert(lid=user1.limit_order_lid)


    def test_11(self):
        """ Create order with out_amount and long rate: USD to UAH, checking rounding. """
        admin.set_wallet_amount(balance=bl(2), currency='USD', email=user1.email)
        admin.set_rate_exchange(fee=0, rate=bl(28.1999), in_currency='USD', out_currency='UAH')
        user1.limit_convert(in_amount=None, out_amount=28.19, in_curr='USD', out_curr='UAH', rate='28.199999',
                            rate_isdir=False)
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'


    def test_12(self):
        """ Ban on finish late order after revoked him. """
        admin.set_wallet_amount(balance=bl(200), currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user1.email)
        admin.uplink_sync()
        uplink_course = ceil(admin.get_rate(in_curr=admin.currency['UAH'], out_curr=admin.currency['USD']) * 100) / 100
        admin.set_rate_exchange(fee=0, rate=bl(uplink_course + 0.02), in_currency='UAH', out_currency='USD')
        user1.limit_convert(in_amount=orig(bl(uplink_course) + bl(0.01)), out_amount=1, in_curr='UAH', out_curr='USD')
        user1.revoke_convert(lid=user1.limit_order_lid)
        admin.uplink_sync()
        assert admin.get_order(lid=user1.limit_order_lid)['status'] == 120
        user1.get_balances(curr='UAH')
        assert user1.resp_balances['UAH'] == '200'
        user1.get_balances(curr='USD')
        assert user1.resp_balances['USD'] == '0'


    def test_13(self):
        """ Create late order with not enough amount on balance with all fee. """
        admin.set_wallet_amount(balance=bl(28.47), currency='UAH', email=user1.email)
        admin.set_rate_exchange(fee=10000000, rate=bl(28.1999), in_currency='UAH', out_currency='USD')
        user1.limit_convert(in_amount=28.48, out_amount=1, in_curr='UAH', out_curr='USD')
        assert user1.resp_limit_convert['exception'] == 'InsufficientFunds'
        user1.limit_convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD', rate='28.4818',
                            rate_isdir=True)
        assert user1.resp_limit_convert['exception'] == 'InsufficientFunds'
        user1.get_balances('UAH')
        assert user1.resp_balances['UAH'] == '28.47'


    def test_14(self):
        """ Create late order with not real in_curr. """
        user1.limit_convert(in_amount=1, out_amount=1, in_curr='UHA', out_curr='USD')
        assert user1.resp_limit_convert['exception'] == 'InvalidCurrency'
        user1.limit_convert(in_amount=None, out_amount=1, in_curr='UHA', out_curr='USD', rate='1', rate_isdir=True)
        assert user1.resp_limit_convert['exception'] == 'InvalidCurrency'


    def test_15(self):
        """ Create late order with not real out_curr. """
        user1.limit_convert(in_amount=1, out_amount=1, in_curr='UAH', out_curr='UDS')
        assert user1.resp_limit_convert['exception'] == 'InvalidCurrency'
        user1.limit_convert(in_amount=None, out_amount=1, in_curr='USD', out_curr='UHA', rate='1', rate_isdir=False)
        assert user1.resp_limit_convert['exception'] == 'InvalidCurrency'


    def test_16(self):
        """ Create late order without in_curr value. """
        user1.limit_convert(in_amount=None, out_amount=1, in_curr='UAH', out_curr='USD')
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'


    def test_17(self):
        """ Create late order without out_curr value. """
        user1.limit_convert(in_amount=1, out_amount=None, in_curr='UAH', out_curr='USD')
        assert user1.resp_limit_convert['exception'] == 'InvalidParam'

    
    def test_18(self):
        """ Create late order with wrong format value in in_amount field. """
        user1.limit_convert(in_amount=1.115, out_amount=1, in_curr='UAH', out_curr='USD')
        assert user1.resp_limit_convert['exception'] == 'InvalidAmountFormat'


    def test_19(self):
        """ Create late order with wrong format value in out_amount field. """
        user1.limit_convert(in_amount=1, out_amount=0.555, in_curr='UAH', out_curr='USD')
        assert user1.resp_limit_convert['exception'] == 'InvalidAmountFormat'


    def test_20(self):
        """ Create late order with in_curr less then tech_min in exchange table. """
        admin.set_wallet_amount(balance=bl(1), currency='UAH', email=user1.email)
        admin.set_tech_limits_exchange(tech_min=bl(1), tech_max=bl(3000), in_currency='UAH', out_currency='USD')
        admin.set_wallet_amount(balance=bl(1), currency='UAH', email=user1.email)
        user1.limit_convert(in_amount=0.99, out_amount=0.3, in_curr='UAH', out_curr='USD')
        assert user1.resp_limit_convert['exception'] == 'AmountTooSmall'


    def test_21(self):
        """ Create late order with in_curr more then tech_max in exchange table. """
        admin.set_wallet_amount(balance=bl(2000), currency='UAH', email=user1.email)
        admin.set_tech_limits_exchange(tech_min=bl(1), tech_max=bl(1000), in_currency='UAH', out_currency='USD')
        user1.limit_convert(in_amount=1000.01, out_amount=300.01, in_curr='UAH', out_curr='USD')
        assert user1.resp_limit_convert['exception'] == 'AmountTooBig'


    def test_22(self):
        """ Revoke late order from not owner. """
        admin.set_wallet_amount(balance=bl(100), currency='UAH', email=user1.email)
        admin.set_rate_exchange(fee=0, rate=bl(28.1999), in_currency='UAH', out_currency='USD')
        user1.limit_convert(in_amount=28.18, out_amount=1, in_curr='UAH', out_curr='USD')
        user2.revoke_convert(lid=user1.limit_order_lid)
        assert user2.resp_revoke_convert['exception'] == 'InvalidParam'
        user1.revoke_convert(lid=user1.limit_order_lid)

