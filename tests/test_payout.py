import pytest
import requests
import json


class TestPayoutWithoutExchange:

    def test_0(self, create_session):
        """ Warm """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session



    def test_payout_1(self, _disable_2type):
        """ Payout 1 UAH to VISAMC. """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user1.email)
        admin.set_fee(payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'], tp=10)
        user1.payout_calc(payway='visamc', amount=1.01, in_curr='UAH', out_curr='UAH')
        assert user1.resp_payout_calc['account_amount'] == '1.01'
        assert user1.resp_payout_calc['out_amount'] == '1.01'
        user1.set_2type(tp='0')
        user1.payout(payway='visamc', payout_wallet='4444444444444448', amount=1, in_curr='UAH', out_curr='UAH')
        assert user1.resp_payout['exception'] == 'Auth2Required'
        user1.confirm_registration(key=user1.payout_2type, code=admin.get_onetime_code(user1.email), user=user1)
        assert user1.resp_confirm['account_amount'] == '1'
        assert user1.resp_confirm['in_amount'] == '1'
        assert user1.resp_confirm['out_amount'] == '1'
        # user1.get_balances(curr='UAH')
        # assert user1.resp_balances['UAH'] == '0'


    def test_payout_2(self):
        """ Payout 1.02 RUB to QIWI. """
        admin.set_wallet_amount(balance=2000000000, currency='RUB', email=user1.email)
        admin.set_fee(payway_id=admin.payway['qiwi'], currency_id=admin.currency['RUB'], tp=10)
        user1.payout_params(payway='qiwi', in_curr='RUB', out_curr='RUB')
        assert user1.resp_payout_params['min'] == '0.01'
        assert user1.resp_payout_params['max'] == '3000'
        assert user1.resp_payout_params['min_balance_limit'] == '0.01'
        user1.payout(payway='qiwi', payout_wallet='+380660000000', amount=1.02, in_curr='RUB', out_curr='RUB')
        assert user1.resp_payout['account_amount'] == '1.02'
        # user1.get_balances(curr='RUB')
        # assert user1.resp_balances['RUB'] == '0.98'


    def test_payout_3(self):
        """ Payout 0.99999 BTC to btc. """
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        admin.set_fee(payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'], tp=10)
        user1.payout(payway='btc', payout_wallet=user1.btc_wallet, amount=0.99999, in_curr='BTC', out_curr='BTC')
        assert user1.resp_payout['account_amount'] == '0.99999'
        assert user1.resp_payout['in_curr'] == 'BTC'
        assert user1.resp_payout['out_curr'] == 'BTC'


    def test_payout_4(self):
        """ Payout 10 USD to ADVCASH with common percent fee 20% """
        admin.set_wallet_amount(balance=11990000000, currency='USD', email=user1.email)
        admin.set_fee(mult=200000000, payway_id=admin.payway['advcash'], currency_id=admin.currency['USD'], tp=10)
        user1.payout_calc(payway='advcash', amount=10, in_curr='USD', out_curr='USD')
        assert user1.resp_payout_calc['account_amount'] == '12'
        assert user1.resp_payout_calc['in_amount'] == '10'
        user1.payout_params(payway='advcash', in_curr='USD', out_curr='USD')
        assert user1.resp_payout_params['min'] == '0.01'
        assert user1.resp_payout_params['max'] == '3000'
        assert user1.resp_payout_params['min_balance_limit'] == '0.02'
        admin.set_wallet_amount(balance=15000000000, currency='USD', email=user1.email)
        user1.payout(payway='advcash', payout_wallet='U111122223333', amount=10, in_curr='USD', out_curr='USD')
        assert user1.resp_payout['account_amount'] == '12'
        # user1.get_balances(curr='USD')
        # assert user1.resp_balances['USD'] == '3'

    @pytest.mark.skip(reason='Disabled currency ETH')
    def test_payout_5(self):
        """ Payout 0.9 ETH to eth with common percent fee 0.01% """
        admin.set_wallet_amount(balance=1000000000, currency='ETH', email=user1.email)
        admin.set_fee(mult=100000, payway_id=admin.payway['eth'], currency_id=admin.currency['ETH'], tp=10)
        user1.payout(payway='eth', payout_wallet=user1.eth_wallet, amount=0.9, in_curr='ETH', out_curr='ETH')
        assert user1.resp_payout['account_amount'] == '0.90009'
        # user1.get_balances(curr='ETH')
        # assert user1.resp_balances['ETH'] == '0.09991'


    def test_payout_6(self, _personal_user_data):
        """ Payout 10 RUB to VTB24 with personal persent fee 20% """
        admin.set_wallet_amount(balance=15000000000, currency='RUB', email=user1.email)
        admin.set_fee(mult=200000000, payway_id=admin.payway['vtb24'], currency_id=admin.currency['RUB'],
                      owner_id=user1.id, tp=10, is_active=True)
        user1.payout_calc(payway='vtb24', amount=10, in_curr='RUB', out_curr='RUB')
        assert user1.resp_payout_calc['account_amount'] == '12'
        assert user1.resp_payout_calc['out_amount'] == '10'
        user1.payout(payway='vtb24', payout_wallet='5599005001737100', amount=10, in_curr='RUB', out_curr='RUB')
        assert user1.resp_payout['account_amount'] == '12'
        assert user1.resp_payout['in_amount'] == '10'
        assert user1.resp_payout['out_amount'] == '10'
        # user1.get_balances(curr='RUB')
        # assert user1.resp_balances['RUB'] == '3'
        admin.set_fee(payway_id=admin.payway['vtb24'], currency_id=admin.currency['RUB'], owner_id=user1.id, tp=10,
                      is_active=False)


    def test_payout_7(self, _personal_user_data):
        """ Payout 0.9 BTC to btc with personal persent fee 0.01% """
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        admin.set_fee(mult=100000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'], owner_id=user1.id,
                      tp=10, is_active=True)
        user1.payout_params(payway='btc', in_curr='BTC', out_curr='BTC')
        assert user1.resp_payout_params['min'] == '0.00001'
        assert user1.resp_payout_params['max'] == '3000'
        assert user1.resp_payout_params['min_balance_limit'] == '0.00001001'
        user1.payout(payway='btc', payout_wallet=user1.btc_wallet, amount=0.9, in_curr='BTC', out_curr='BTC')
        assert user1.resp_payout['account_amount'] == '0.90009'
        assert user1.resp_payout['in_amount'] == '0.9'
        assert user1.resp_payout['out_amount'] == '0.9'
        admin.set_fee(payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'], owner_id=user1.id, tp=10,
                      is_active=False)

    @pytest.mark.skip(reason='Disabled functional')
    def test_payout_8(self):
        """ Payout 10 USD to CASH_MOSCOW with common absolute fee 5.55 USD """
        admin.set_wallet_amount(balance=16000000000, currency='USD', email=user1.email)
        admin.set_fee(add=5550000000, payway_id=admin.payway['cash_moscow'], currency_id=admin.currency['USD'], tp=10)
        user1.payout(payway='cash_moscow', payout_wallet='+380661234567', amount=10, in_curr='USD', out_curr='USD')
        assert user1.resp_payout['account_amount'] == '15.55'
        assert user1.resp_payout['in_amount'] == '10'
        assert user1.resp_payout['out_amount'] == '10'


    def test_payout_9(self):
        """ Payout 0.9 BTC to btc with common absolute fee 0.0006 BTC """
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        admin.set_fee(add=600000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'], tp=10)
        user1.payout_calc(payway='btc', in_curr='BTC', out_curr='BTC', amount=0.9995)
        assert user1.resp_payout_calc['account_amount'] == '1.0001'
        assert user1.resp_payout_calc['in_fee_amount'] == '0.0006'
        user1.payout(payway='btc', payout_wallet=user1.btc_wallet, amount=0.9, in_curr='BTC', out_curr='BTC')
        assert user1.resp_payout['account_amount'] == '0.9006'
        assert user1.resp_payout['in_amount'] == '0.9'
        assert user1.resp_payout['out_amount'] == '0.9'


    def test_payout_10(self, _personal_user_data):
        """ Payout 10 RUB to WEBMONEY with common percent fee 10% and personal percent fee 5% """
        admin.set_wallet_amount(balance=20000000000, currency='RUB', email=user1.email)
        admin.set_fee(mult=100000000,  payway_id=admin.payway['webmoney'], currency_id=admin.currency['RUB'], tp=10)
        admin.set_fee(mult=50000000,  payway_id=admin.payway['webmoney'], currency_id=admin.currency['RUB'],
                      owner_id=user1.id, tp=10, is_active=True)
        user1.payout(payway='webmoney', payout_wallet='R123456789011', amount=10, in_curr='RUB', out_curr='RUB')
        assert user1.resp_payout['account_amount'] == '10.5'
        admin.set_fee(payway_id=admin.payway['webmoney'], currency_id=admin.currency['RUB'], owner_id=user1.id,
                      tp=10, is_active=False)


    def test_payout_11(self, _personal_user_data):
        """ Payout 0.9 BTC to btc with common percent fee 10% and personal percent fee 5%. """
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        admin.set_fee(mult=100000000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'], tp=10)
        admin.set_fee(mult=50000000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'],
                      owner_id=user1.id, tp=10, is_active=True)
        user1.payout(payway='btc', payout_wallet=user1.btc_wallet, amount=0.9, in_curr='BTC', out_curr='BTC')
        assert user1.resp_payout['account_amount'] == '0.945'
        assert user1.resp_payout['in_fee_amount'] == '0.045'
        assert user1.resp_payout['out_fee_amount'] == '0.045'
        admin.set_fee(payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'], owner_id=user1.id, tp=10,
                      is_active=False)


    def test_payout_12(self):
        """ Payout 10 UAH to MONOBANK with common percent fee 10% and common absolute fee 5 UAH. """
        admin.set_wallet_amount(balance=15000000000, currency='UAH', email=user1.email)
        admin.set_fee(mult=100000000, add=5000000000, payway_id=admin.payway['monobank'],
                      currency_id=admin.currency['UAH'], tp=10)
        user1.payout_calc(payway='monobank', amount=10, in_curr='UAH', out_curr='UAH')
        assert user1.resp_payout_calc['in_fee_amount'] == '6'
        assert user1.resp_payout_calc['out_fee_amount'] == '6'
        admin.set_wallet_amount(balance=20000000000, currency='UAH', email=user1.email)
        user1.payout(payway='monobank', payout_wallet='4731185606613054', amount=10, in_curr='UAH', out_curr='UAH')
        assert user1.resp_payout['account_amount'] == '16'
        assert user1.resp_payout['out_amount'] == '10'


    def test_payout_13(self):
        """ Payout 0.9 ETH to eth with common percent fee 10% and common absolute fee 0.002 ETH. """
        admin.set_wallet_amount(balance=1000000000, currency='ETH', email=user1.email)
        admin.set_fee(mult=100000000, add=2000000, payway_id=admin.payway['eth'], currency_id=admin.currency['ETH'], tp=10)
        user1.payout_params(payway='eth', in_curr='ETH', out_curr='ETH')
        assert user1.resp_payout_params['min'] == '0.00001'
        assert user1.resp_payout_params['max'] == '3000'
        assert user1.resp_payout_params['out_curr'] == 'ETH'
        user1.payout(payway='eth', payout_wallet=user1.eth_wallet, amount=0.9, in_curr='ETH', out_curr='ETH')
        assert user1.resp_payout['account_amount'] == '0.992'
        assert user1.resp_payout['in_curr'] == 'ETH'
        assert user1.resp_payout['out_curr'] == 'ETH'


    def test_payout_14(self, _personal_user_data):
        """ Payout 10 RUB to QIWI with common percent fee 10% and personal absolute fee 5 UAH. """
        admin.set_wallet_amount(balance=20000000000, currency='RUB', email=user1.email)
        admin.set_fee(mult=100000000, payway_id=admin.payway['qiwi'], currency_id=admin.currency['RUB'], tp=10)
        admin.set_fee(add=5000000000, payway_id=admin.payway['qiwi'], currency_id=admin.currency['RUB'],
                      owner_id=user1.id, tp=10, is_active=True)
        user1.payout(payway='qiwi', payout_wallet='+380660000000', amount=10, in_curr='RUB', out_curr='RUB')
        assert user1.resp_payout['account_amount'] == '15'
        admin.set_fee(payway_id=admin.payway['qiwi'], currency_id=admin.currency['RUB'], owner_id=user1.id,
                      tp=10, is_active=False)


    def test_payout_15(self, _personal_user_data):
        """ Payout 0.9 BTC to btc with common percent fee 10% and personal absolute fee 0.002 BTC. """
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        admin.set_fee(mult=100000000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'], tp=10)
        admin.set_fee(add=2000000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'],
                      owner_id=user1.id, tp=10, is_active=True)
        user1.payout(payway='btc', payout_wallet=user1.btc_wallet, amount=0.9, in_curr='BTC', out_curr='BTC')
        assert user1.resp_payout['account_amount'] == '0.902'
        admin.set_fee(payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'], owner_id=user1.id, tp=10,
                      is_active=False)


    def test_payout_16(self, _personal_user_data):
        """ Payout 10 UAH to VISA MC with personal percent fee 10% and personal absolute fee 5 UAH. """
        admin.set_wallet_amount(balance=20000000000, currency='UAH', email=user1.email)
        admin.set_fee(mult=100000000, add=5000000000, payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'],
                      owner_id=user1.id, tp=10, is_active=True)
        user1.payout(payway='visamc', payout_wallet='4444444444444448', amount=10, in_curr='UAH', out_curr='UAH')
        assert user1.resp_payout['account_amount'] == '16'
        admin.set_fee(payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'], owner_id=user1.id, tp=10,
                      is_active=False)


    def test_payout_17(self, _personal_user_data):
        """ Payout 0.9 BTC to btc with personal percent fee 10% and personal absolute fee 0.002 BTC. """
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        admin.set_fee(mult=100000000, add=2000000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'],
                      owner_id=user1.id, tp=10, is_active=True)
        user1.payout(payway='btc', payout_wallet=user1.btc_wallet, amount=0.9, in_curr='BTC', out_curr='BTC')
        assert user1.resp_payout['account_amount'] == '0.992'
        admin.set_fee(payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'], owner_id=user1.id, tp=10,
                      is_active=False)


    def test_payout_18(self, _personal_user_data):
        """ Payout 10 USD to PERFECT MONEY with common percent fee 10% and common absolute fee 3 USD
            with personal percent fee 5% and personal absolute fee 4 USD. """
        admin.set_wallet_amount(balance=20000000000, currency='USD', email=user1.email)
        admin.set_fee(mult=100000000, add=3000000000, payway_id=admin.payway['perfect'], currency_id=admin.currency['USD'],
                      tp=10)
        admin.set_fee(mult=50000000, add=4000000000, payway_id=admin.payway['perfect'], currency_id=admin.currency['USD'],
                      owner_id=user1.id, tp=10)
        user1.payout_params(payway='perfect', in_curr='USD', out_curr='USD')
        assert user1.resp_payout_params['min'] == '0.01'
        assert user1.resp_payout_params['max'] == '3000'
        user1.payout(payway='perfect', payout_wallet='U1234567', amount=10, in_curr='USD', out_curr='USD')
        assert user1.resp_payout['account_amount'] == '14.5'
        admin.set_fee(payway_id=admin.payway['perfect'], currency_id=admin.currency['USD'], owner_id=user1.id, tp=10,
                      is_active=False)


    def test_payout_19(self, _personal_user_data):
        """ Payout 0.9 BTC to btc with common percent fee 10% and common absolute fee 0.2 BTC
            with personal percent fee 5% and personal absolute fee 0.1 BTC """
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        admin.set_fee(mult=100000000, add=200000000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'],
                      tp=10)
        admin.set_fee(mult=50000000, add=100000000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'],
                      owner_id=user1.id, tp=10)
        user1.payout_params(payway='btc', in_curr='BTC', out_curr='BTC')
        assert user1.resp_payout_params['max'] == '3000'
        assert user1.resp_payout_params['min'] == '0.00001'
        assert user1.resp_payout_params['out_fee'] == {'add': '0.1', 'max': '0', 'method': 'ceil', 'min': '0',
                                                       'mult': '0.05'}
        assert user1.resp_payout_params['in_curr_balance'] == '1'
        user1.payout_calc(payway='btc', amount=0.9, in_curr='BTC', out_curr='BTC')
        assert user1.resp_payout_calc['account_amount'] == '1.045'
        admin.set_wallet_amount(balance=2000000000, currency='BTC', email=user1.email)
        user1.payout(payway='btc', payout_wallet=user1.btc_wallet, amount=0.9, in_curr='BTC', out_curr='BTC')
        assert user1.resp_payout['account_amount'] == '1.045'
        admin.set_fee(payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'], owner_id=user1.id,
                      tp=10, is_active=False)


    def test_payout_20(self):
        """ Payout 3.37 USD to PAYEER from UAH with common persent fee 2% and common absolute fee 1 USD
            with exchange fee 5%. """
        admin.set_wallet_amount(balance=140000000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=50000000, in_currency='UAH', out_currency='USD')
        admin.set_fee(mult=20000000, add=1000000000, payway_id=admin.payway['payeer'], currency_id=admin.currency['USD'],
                      tp=10)
        user1.payout_params(payway='payeer', in_curr='UAH', out_curr='USD')
        assert user1.resp_payout_params['out_fee'] == {'add': '1', 'max': '0', 'min': '0', 'method': 'ceil', 'mult': '0.02'}
        assert user1.resp_payout_params['in_fee'] == {'add': '29.61', 'mult': '0.02'}
        user1.payout(payway='payeer', payout_wallet='P123456789', amount=3.37, in_curr='UAH', out_curr='USD')
        assert user1.resp_payout['account_amount'] == '131.48'
        assert user1.resp_payout['in_amount'] == '99.79'
        assert user1.resp_payout['out_amount'] == '3.37'

    @pytest.mark.skip(reason='Disabled exchange ')
    def test_payout_21(self):
        """ Payout 550 UAH to VISA MC from BTC with common percent fee 5% and common absolute fee 50.07 UAH
            with exchange fee 5%. """
        admin.set_wallet_amount(balance=500000000, currency='BTC', email=user1.email)
        admin.set_rate_exchange(rate=17919315600000, fee=50000000, in_currency='BTC', out_currency='UAH')
        admin.set_fee(mult=50000000, add=50070000000, payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'],
                      tp=10)
        user1.payout(payway='visamc', payout_wallet='4444444444444448', amount=550, in_curr='BTC', out_curr='UAH')
        assert user1.resp_payout['account_amount'] == '0.03686526'
        assert user1.resp_payout['in_amount'] == '0.03230857'
        assert user1.resp_payout['out_amount'] == '550'
        assert user1.resp_payout['in_fee_amount'] == '0.00455669'
        assert user1.resp_payout['out_fee_amount'] == '77.57'


    def test_payout_22(self, _personal_user_data):
        """ Payout 3.37 USD to payeer from UAH with common persent fee 3% and common absolute fee 2 USD
        with personal persent fee 2% and personal absolute fee 1 USD with common exchange fee 5%. """
        admin.set_wallet_amount(balance=140000000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=50000000, in_currency='UAH', out_currency='USD')
        admin.set_fee(mult=30000000, add=2000000000, payway_id=admin.payway['payeer'], currency_id=admin.currency['USD'],
                      tp=10)
        admin.set_fee(mult=20000000, add=1000000000, payway_id=admin.payway['payeer'], currency_id=admin.currency['USD'],
                      owner_id=user1.id, tp=10)
        admin.set_personal_exchange_fee(is_active=False, in_currency='UAH', out_currency='USD', email=user1.email)
        user1.payout_calc(payway='payeer', amount=3.37, in_curr='UAH', out_curr='USD')

        user1.payout(payway='payeer', payout_wallet='P123456789', amount=3.37, in_curr='UAH', out_curr='USD')
        assert user1.resp_payout['account_amount'] == '131.48'
        assert user1.resp_payout['in_amount'] == '99.79'
        assert user1.resp_payout['out_amount'] == '3.37'
        user1.payout_get(lid=user1.payout_lid)
        assert user1.resp_payout_get['account_amount'] == '131.48'
        assert user1.resp_payout_get['payway_name'] == 'payeer'
        assert user1.resp_payout_get['in_curr'] == 'UAH'
        admin.set_fee(payway_id=admin.payway['payeer'], currency_id=admin.currency['USD'], owner_id=user1.id, tp=10,
                      is_active=False)
        admin.set_fee(mult=30000000, add=2000000000, payway_id=admin.payway['payeer'], currency_id=admin.currency['USD'],
                      tp=10, is_active=False)
        user1.payout_params(payway='payeer', in_curr='UAH', out_curr='USD')
        assert user1.resp_payout_params['out_fee'] == {}
        admin.set_fee(mult=30000000, add=2000000000, payway_id=admin.payway['payeer'], currency_id=admin.currency['USD'],
                      tp=10)

    @pytest.mark.skip(reason='Disabled exchange ')
    def test_payout_23(self, _personal_user_data):
        """ Payout 550 UAH to VISA MC from BTC with common percent fee 10% and common absolute fee 70 UAH with personal
            percent fee 5% and personal absolute fee 50.07 UAH with exchange fee 5%. """
        admin.set_wallet_amount(balance=500000000, currency='BTC', email=user1.email)
        admin.set_rate_exchange(rate=17919315600000, fee=50000000, in_currency='BTC', out_currency='UAH')
        admin.set_fee(mult=100000000, add=70000000000, payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'],
                      tp=10)
        admin.set_fee(mult=50000000, add=50070000000, payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'],
                      owner_id=user1.id, tp=10)
        user1.payout(payway='visamc', payout_wallet='4444444444444448', amount=550, in_curr='BTC', out_curr='UAH')
        assert user1.resp_payout['account_amount'] == '0.03686526'
        assert user1.resp_payout['in_amount'] == '0.03230857'
        assert user1.resp_payout['out_amount'] == '550'
        assert user1.resp_payout['in_fee_amount'] == '0.00455669'
        assert user1.resp_payout['out_fee_amount'] == '77.57'
        admin.set_fee(payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'], owner_id=user1.id, tp=10,
                      is_active=False)


    def test_payout_24(self, _personal_user_data):
        """ Payout 3.37 USD to PAYEER from UAH with common persent fee 3% and common absolute fee 2 USD
            with personal persent fee 2% and personal absolute fee 1 USD with exchange fee 5%
            and operation bonus 1 USD = 1%. """
        admin.set_wallet_amount(balance=140000000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=50000000, in_currency='UAH', out_currency='USD')
        admin.set_fee(mult=30000000, add=2000000000, payway_id=admin.payway['payeer'], currency_id=admin.currency['USD'],
                      tp=10)
        admin.set_fee(mult=20000000, add=1000000000, payway_id=admin.payway['payeer'], currency_id=admin.currency['USD'],
                      owner_id=user1.id, tp=10)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=True)
        user1.payout(payway='payeer', payout_wallet='P123456789', amount=3.37, in_curr='UAH', out_curr='USD')
        assert user1.resp_payout['account_amount'] == '131.48'
        admin.set_fee(payway_id=admin.payway['payeer'], currency_id=admin.currency['USD'], owner_id=user1.id, tp=10, is_active=False)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=False)

    @pytest.mark.skip(reason='Disabled exchange. ')
    def test_payout_25(self, _personal_user_data):
        """ Payout 550 UAH to VISA MC from BTC with common percent fee 10% and common absolute fee 70 UAH
            with personal percent fee 5% and personal absolute fee 50.07 UAH with exchange fee 6.5%
            and collected bonus 5 points = 1.5%. """
        admin.set_wallet_amount(balance=500000000, currency='BTC', email=user1.email)
        admin.set_rate_exchange(rate=17919315600000, fee=65000000, in_currency='BTC', out_currency='UAH')
        admin.set_fee(mult=100000000, add=70000000000, payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'],
                      tp=10)
        admin.set_fee(mult=50000000, add=50070000000, payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'],
                      owner_id=user1.id, tp=10)
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=15000000, is_active=True)
        admin.set_user_data(user_email=user1.email, is_customfee=False, bonus_points=5000000000, user=user1)
        user1.payout(payway='visamc', payout_wallet='4444444444444448', amount=550, in_curr='BTC', out_curr='UAH')
        assert user1.resp_payout['account_amount'] == '0.03686526'
        assert user1.resp_payout['in_amount'] == '0.03230857'
        assert user1.resp_payout['out_amount'] == '550'
        assert user1.resp_payout['in_fee_amount'] == '0.00455669'
        assert user1.resp_payout['out_fee_amount'] == '77.57'
        admin.set_fee(payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'], owner_id=user1.id, tp=10,
                      is_active=False)
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=15000000, is_active=False)

    @pytest.mark.skip(reason='Disabled exchange. ')
    def test_payout_26(self, _personal_user_data):
        """ Payout 0.5 ETH from BTC with common percent fee 15% and common absolute fee 0.02 ETH with personal percent
            fee 10% and personal absolute fee 0.01 ETH with exchange fee 6%, personal exchange fee 5%
            and operation bonus 1 USD = 1% """
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        admin.set_rate_exchange(rate=26690000000, fee=60000000, in_currency='BTC', out_currency='ETH')
        admin.set_fee(mult=150000000, add=20000000, payway_id=admin.payway['eth'], currency_id=admin.currency['ETH'],
                      tp=10)
        admin.set_fee(mult=100000000, add=10000000, payway_id=admin.payway['eth'], currency_id=admin.currency['ETH'],
                      owner_id=user1.id, tp=10)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=True)
        admin.set_personal_exchange_fee(is_active=True, in_currency='BTC', out_currency='ETH', email=user1.email, fee=50000000)
        user1.payout(payway='eth', payout_wallet=user1.eth_wallet, amount=0.5, in_curr='BTC', out_curr='ETH')
        assert user1.resp_payout['account_amount'] == '0.02208594'
        assert user1.resp_payout['in_amount'] == '0.01971959'
        assert user1.resp_payout['out_amount'] == '0.5'
        assert user1.resp_payout['in_fee_amount'] == '0.00236635'
        assert user1.resp_payout['out_fee_amount'] == '0.06'
        user1.payout_list(in_curr='BTC', out_curr='ETH')
        assert user1.resp_payout_list['data'][0]['account_amount'] == '0.02208594'
        assert user1.resp_payout_list['data'][0]['bonus_surplus'] == '0'
        assert user1.resp_payout_list['data'][0]['payway_name'] == 'eth'
        admin.set_fee(payway_id=admin.payway['eth'], currency_id=admin.currency['ETH'], owner_id=user1.id, tp=10,
                      is_active=False)
        admin.set_personal_exchange_fee(is_active=False, in_currency='BTC', out_currency='ETH', email=user1.email, fee=50000000)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=False)

    @pytest.mark.skip(reason='Disabled exchange. ')
    def test_payout_27(self, _personal_user_data):
        """ Payout 0.03 BTC from ETH with common percent fee 15% and common absolute fee 0.001 BTC with personal percent
            fee 10% and personal absolute fee 0.0005 BTC with exchange fee 7%, personal exchange fee 5%
            and collected bonus 5 points = 1% """
        admin.set_wallet_amount(balance=1000000000, currency='ETH', email=user1.email)
        admin.set_rate_exchange(rate=26690000000, fee=70000000, in_currency='ETH', out_currency='BTC')
        admin.set_fee(mult=150000000, add=1000000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'],
                      tp=10)
        admin.set_fee(mult=100000000, add=500000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'],
                      owner_id=user1.id, tp=10)
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=True)
        admin.set_user_data(user_email=user1.email, is_customfee=True, user=user1, bonus_points=5000000000)
        admin.set_personal_exchange_fee(is_active=True, in_currency='ETH', out_currency='BTC', email=user1.email, fee=50000000)
        user1.payout_calc(payway='btc', amount=0.03, in_curr='ETH', out_curr='BTC')
        assert user1.resp_payout_calc['in_amount'] == '0.840735'
        assert user1.resp_payout_calc['out_amount'] == '0.03'
        user1.payout_params(payway='btc', in_curr='ETH', out_curr='BTC')
        assert user1.resp_payout_params['out_fee'] == {'add': '0.0005', 'max': '0', 'method': 'ceil', 'min': '0',
                                                       'mult': '0.1'}
        assert user1.resp_payout_params['in_fee'] == {'add': '0.01401225', 'mult': '0.1'}
        user1.payout(payway='btc', payout_wallet=user1.btc_wallet, amount=0.03, in_curr='ETH', out_curr='BTC')
        assert user1.resp_payout['account_amount'] == '0.93882075'
        assert user1.resp_payout['in_amount'] == '0.840735'
        assert user1.resp_payout['out_amount'] == '0.03'
        assert user1.resp_payout['in_fee_amount'] == '0.09808575'
        assert user1.resp_payout['out_fee_amount'] == '0.0035'
        admin.set_fee(payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'], owner_id=user1.id, tp=10,
                      is_active=False)
        admin.set_personal_exchange_fee(is_active=False, in_currency='ETH', out_currency='BTC', email=user1.email, fee=50000000)
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=False)
        admin.set_user_data(user_email=user1.email, is_customfee=False, user=user1)


    def test_payout_28(self):
        """ List all payouts. """
        user1.payout_list(in_curr='USD', out_curr='USD', first=0, count=1000)
        currency_ls = [(ls['in_curr'], ls['out_curr']) for ls in user1.resp_payout_list['data']]
        tp_ls = [ls['tp'] for ls in user1.resp_payout_list['data']]
        assert not ('USD', 'USD') not in currency_ls and ('USD', 'USD') in currency_ls
        assert not 'payout' not in tp_ls and 'payout' in tp_ls

    @pytest.mark.skip
    def test_payout_29(self):
        """ List payouts with in_curr. """
        user1.payout_list(in_curr='USD', out_curr=None, first=0, count=1000)
        currency_ls = [ls['in_curr'] for ls in user1.resp_payout_list['data']]
        assert not 'USD' not in currency_ls and 'USD' in currency_ls

    @pytest.mark.skip
    def test_payout_30(self):
        """ List payouts with in_curr. """
        user1.payout_list(in_curr=None, out_curr='UAH', first=0, count=1000)
        currency_ls = [ls['out_curr'] for ls in user1.resp_payout_list['data']]
        assert not 'UAH' not in currency_ls and 'UAH' in currency_ls

    @pytest.mark.skip
    def test_payout_31(self):
        """ List payouts with 2 elements. """
        user1.payout_list(in_curr=None, out_curr='UAH', first=0, count=2)
        currency_ls = [ls['out_curr'] for ls in user1.resp_payout_list['data']]
        assert not 'UAH' not in currency_ls and 'UAH' in currency_ls
        assert len(user1.resp_payout_list['data']) == 2

    def test_payout_32(self, _personal_user_data):
        """ Payout amount, more than user has on his account with all fee """
        admin.set_wallet_amount(balance=15490000000, currency='UAH', email=user1.email)
        admin.set_fee(mult=100000000, add=70000000000, payway_id=admin.payway['visamc'],
                      currency_id=admin.currency['UAH'], tp=10)
        admin.set_fee(mult=50000000, add=5000000000, payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'],
                      owner_id=user1.id, tp=10)
        user1.payout(payway='visamc', payout_wallet='4444444444444448', amount=10, in_curr='UAH', out_curr='UAH')
        assert user1.resp_payout['exception'] == 'InsufficientFunds'
        admin.set_fee(payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'], owner_id=user1.id, tp=10,
                      is_active=False)

    def test_payout_33(self):
        """ Payout amount less than tech_min and more than tech_max by pwcurrency table.
            Payout amount equal with tech_min and tech_max by pwcurrency table. """
        admin.set_wallet_amount(balance=40000000000, currency='USD', email=user1.email)
        admin.set_fee(payway_id=admin.payway['payeer'], currency_id=admin.currency['USD'], tp=10)
        admin.set_pwcurrency_data(currency='USD', tech_min=1000000000, tech_max=10000000000, is_active=True)
        user1.payout(payway='payeer', payout_wallet='P123456789', amount=0.99, in_curr='USD', out_curr='USD')
        assert user1.resp_payout['exception'] == 'AmountTooSmall'
        user1.payout(payway='payeer', payout_wallet='P123456789', amount=10.01, in_curr='USD', out_curr='USD')
        assert user1.resp_payout['exception'] == 'AmountTooBig'
        user1.payout(payway='payeer', payout_wallet='P123456789', amount=1, in_curr='USD', out_curr='USD')
        assert user1.resp_payout['account_amount'] == '1'
        user1.payout(payway='payeer', payout_wallet='P123456789', amount=10, in_curr='USD', out_curr='USD')
        assert user1.resp_payout['account_amount'] == '10'
        admin.set_pwcurrency_data(currency='USD', tech_min=10000000, tech_max=3000000000000, is_active=True)

    def test_payout_34(self):
        """ Payout not real currency """
        user1.payout(payway='payeer', payout_wallet='P123456789', amount='1', in_curr='UDS', out_curr='UDS')
        assert user1.resp_payout['exception'] == 'InvalidCurrency'

    def test_payout_35(self):
        """ Payout not real payway """
        admin.set_wallet_amount(balance=1000000000, currency='USD', email=user1.email)
        user1.payout(payway='paywer', payout_wallet='P123456789', amount='1', in_curr='USD', out_curr='USD')
        assert user1.resp_payout['exception'] == 'InvalidPayway'

    def test_payout_36(self):
        """ Payout by not active payway by pwcurrency table """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user1.email)
        admin.set_pwcurrency_data(currency='UAH', is_active=False, tech_min=10000000, tech_max=2000000000000)
        user1.payout(payway='visamc', payout_wallet='4444444444444448', amount=0.01, in_curr='UAH', out_curr='UAH')
        assert user1.resp_payout['exception'] == 'InactiveCurrency'
        admin.set_pwcurrency_data(currency='UAH', is_active=True, tech_min=10000000, tech_max=2000000000000)

    def test_payout_37(self):
        """ Payout with exchange from not real currency """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user1.email)
        user1.payout(payway='visamc', payout_wallet='4444444444444448', amount=0.01, in_curr='UHA', out_curr='UAH')
        assert user1.resp_payout['exception'] == 'InvalidCurrency'

    def test_payout_38(self):
        """ Payout with exchange to not real currency """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user1.email)
        user1.payout(payway='visamc', payout_wallet='4444444444444448', amount=0.01, in_curr='UAH', out_curr='UHA')
        assert user1.resp_payout['exception'] == 'InvalidCurrency'


    @pytest.mark.skip(reason='Not deactiveted')
    def test_payout_39(self):
        """ Payout with exchange by not active pair. """
        admin.set_wallet_amount(balance=100000000, currency='BCH', email=user1.email)
        admin.set_fee(payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'], tp=10)
        user1.payout(payway='visamc', payout_wallet='4444444444444448', amount=5, in_curr='BCHABC', out_curr='UAH')
        assert user1.resp_payout['exception'] == 'UnavailExchange'

    def test_payout_40(self):
        """ Payout with exchange to payway with invalid currency for him. """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user1.email)
        user1.payout(payway='payeer', payout_wallet='P123456789', amount=1, in_curr='UAH', out_curr='UAH')
        assert user1.resp_payout['exception'] == 'InvalidCurrency'

    def test_payout_41(self):
        """ Request without external_id """
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        response = requests.post(url=user1.wapi_url,
                                 json={'method': 'create', 'model': 'payout',
                                       'data': {'userdata': {'payee': user1.btc_wallet},
                                                'payway': 'btc', 'amount': 0.1, 'in_curr': 'BTC', 'out_curr': 'BTC'}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'InvalidParam'

    @pytest.mark.skip(reason='Not stable')
    def test_payout_42(self):
        """ Request with existing external_id """
        admin.set_wallet_amount(balance=2000000000, currency='USD', email=user1.email)
        response = requests.post(url=user1.wapi_url, json={'method': 'create', 'model': 'payout',
                                                           'data': {'externalid': user1.resp_payout_list['data'][0]['externalid'],
                                                                    'userdata': {'payee': user1.btc_wallet},
                                                                    'payway': 'payeer', 'amount': 1, 'in_curr': 'USD',
                                                                    'out_curr': 'USD'}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'InvalidField'

    def test_payout_43(self):
        """ Request without userdata field """
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        response = requests.post(url=user1.wapi_url, json={'method': 'create', 'model': 'payout',
                                                           'data': {'externalid': user1.ex_id(),
                                                                    'payway': 'btc', 'amount': 0.1, 'in_curr': 'BTC',
                                                                    'out_curr': 'BTC'}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'InvalidField'

    def test_payout_44(self):
        """ Request without out_curr field """
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        response = requests.post(url=user1.wapi_url, json={'method': 'create', 'model': 'payout',
                                                           'data': {'externalid': user1.ex_id(),
                                                                    'userdata': {'payee': user1.btc_wallet},
                                                                    'payway': 'btc', 'amount': 0.1, 'in_curr': 'BTC'}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'InvalidCurrency'

    def test_payout_45(self):
        """ Request without amount field """
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        response = requests.post(url=user1.wapi_url, json={'method': 'create', 'model': 'payout',
                                                           'data': {'externalid': user1.ex_id(),
                                                                    'userdata': {'payee': user1.btc_wallet},
                                                                    'payway': 'btc', 'in_curr': 'BTC',
                                                                    'out_curr': 'BTC'}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'InvalidParam'

    def test_payout_46(self):
        """ Request without user token """
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        response = requests.post(url=user1.wapi_url, json={'method': 'create', 'model': 'payout',
                                                           'data': {'externalid': user1.ex_id(),
                                                                    'userdata': {'payee': user1.btc_wallet},
                                                                    'payway': 'btc', 'amount': 0.1, 'in_curr': 'BTC',
                                                                    'out_curr': 'BTC'}}, verify=False)
        assert json.loads(response.text)['exception'] == 'Unauth'

    def test_payout_47(self):
        """ Request with wrong user token """
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        response = requests.post(url=user1.wapi_url, json={'method': 'create', 'model': 'payout',
                                                           'data': {'externalid': user1.ex_id(),
                                                                    'userdata': {'payee': user1.btc_wallet},
                                                                    'payway': 'btc', 'in_curr': 'BTC',
                                                                    'out_curr': 'BTC'}},
                                 params={'token': '123'}, verify=False)
        assert json.loads(response.text)['exception'] == 'Unauth'

    def test_payout_48(self):
        """ Request with excess parametr in userdata. """
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        response = requests.post(url=user1.wapi_url, json={'method': 'create', 'model': 'payout',
                                                           'data': {'externalid': user1.ex_id(),
                                                                    'userdata': {'payee': user1.btc_wallet},
                                                                    'payway': 'btc', 'in_curr': 'BTC',
                                                                    'out_curr': 'BTC', 'par': 123}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'InvalidParam'

    def test_payout_49(self):
        """ Request with excess parametr in common field. """
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        response = requests.post(url=user1.wapi_url, json={'method': 'create', 'model': 'payout', 'par': 123,
                                                           'data': {'externalid': user1.ex_id(),
                                                                    'userdata': {'payee': user1.btc_wallet},
                                                                    'payway': 'btc', 'in_curr': 'BTC',
                                                                    'out_curr': 'BTC'}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'InvalidParam'


class TestInCurrencyList:

    def test_0(self, create_session):
        """ Warm """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session


    def test_incurrlist_1(self):
        """ Getting list of currency to payout with exchange to USD on PAYEER. """
        admin.set_wallet_amount(balance=200000000000, currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=1000000000, currency='BTC', email=user1.email)
        admin.set_wallet_amount(balance=200000000000, currency='USD', email=user1.email)
        admin.set_wallet_amount(balance=1000000000, currency='LTC', email=user1.email)
        admin.set_wallet_amount(balance=1000000000, currency='BCHABC', email=user1.email)
        admin.set_wallet_amount(balance=1000000000, currency='ETH', email=user1.email)
        admin.set_wallet_amount(balance=200000000000, currency='RUB', email=user1.email)
        admin.set_wallet_amount(balance=1000000000, currency='USDT', email=user1.email)
        user1.exchange_list(in_curr=None, out_curr='USD')
        _list = [curr['in_curr'] for curr in user1.resp_exchange_list['data']]
        _list.append('USD')
        _list.sort()
        user1.in_curr_list(out_curr='USD', payway='perfect')
        user1.resp_in_curr_list.sort()
        assert user1.resp_in_curr_list == _list


    def test_incurrlist_2(self):
        """ Getting list of currency to payout with exchange to UAH on VISAMC. """
        admin.set_wallet_amount(balance=3000000000000, currency='USD', email=user1.email)
        user1.exchange_list(in_curr=None, out_curr='UAH')
        _list = [curr['in_curr'] for curr in user1.resp_exchange_list['data']]
        _list.append('UAH')
        _list.sort()
        user1.in_curr_list(out_curr='UAH', payway='visamc')
        user1.resp_in_curr_list.sort()
        assert user1.resp_in_curr_list == _list

    def test_incurrlist_3(self):
        """ Getting list for inactive payway. """
        admin.set_payways(name='visamc', is_inactive=True)
        user1.in_curr_list(out_curr='UAH', payway='visamc')
        assert user1.resp_in_curr_list['exception'] == 'InvalidPayway'
        admin.set_payways(name='visamc', is_inactive=False)

    def test_incurrlist_4(self):
        """ Getting list for not real payway. """
        user1.in_curr_list(out_curr='UAH', payway='visam')
        assert user1.resp_in_curr_list['exception'] == 'InvalidPayway'

    def test_incurrlist_5(self):
        """ Getting list for not real currency. """
        user1.in_curr_list(out_curr='UHA', payway='visamc')
        assert user1.resp_in_curr_list['exception'] == 'InvalidCurrency'

    def test_incurrlist_6(self):
        """ Getting list for wrong pair of payway and currency. """
        user1.in_curr_list(out_curr='UAH', payway='qiwi')
        assert user1.resp_in_curr_list['exception'] == 'InvalidCurrency'

    def test_incurrlist_7(self):
        """ Getting list without out_curr parameter. """
        user1.in_curr_list(out_curr=None, payway='qiwi')
        assert user1.resp_in_curr_list['exception'] == 'InvalidCurrency'

    def test_incurrlist_8(self):
        """ Getting list without payway parameter. """
        user1.in_curr_list(out_curr='UAH', payway=None)
        assert user1.resp_in_curr_list['exception'] == 'InvalidPayway'