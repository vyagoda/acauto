from imaplib import IMAP4_SSL
import base64
import email
import time

def getting_email_code():
    connection = IMAP4_SSL(host='imap.gmail.com', port=993)
    connection.login(user='studentsharetest1', password='student-share')
    connection.select('INBOX')
    result, data = connection.uid('search', None, "ALL")
    print(bool(data[0]))
    latest_email_uid = data[0].split()[-1]
    print(latest_email_uid)
    result, data = connection.uid('fetch', latest_email_uid, '(RFC822)')
    raw_email = data[0][1]
    msg = email.message_from_bytes(raw_email)
    for msg in msg.walk():
        if msg.get_content_maintype() == 'text':
            letter = msg.get_payload().encode('utf-8')
            letter = base64.decodebytes(letter).decode('utf-8')
        return


def equal_list(_list, elem):
    print(_list)
    while 1:
        for el in _list:
            if elem == el:
                pass
            else:
                return False
        return True


def bl(numb):
    """ Return number * 1 000 000 000. """
    return numb * 1000000000


def orig(numb):
    """ Return number / 1 000 000 000. """
    return numb / 1000000000



if __name__ == '__main__':
    print(bl(5))
    print(orig(bl(100) - (bl(26.45) - bl(0.02))))
