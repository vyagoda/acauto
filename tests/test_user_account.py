import pytest
from tests.tools import equal_list
from user.user import User


class TestRegistration:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_1(self, _delete_user):
        """ Success registration by email. """
        User.registration(email='anycashuser100@mailinator.com', pwd='Ac*123')
        User.confirm_registration(key=User.confirm_key, code=admin.get_onetime_code('anycashuser100@mailinator.com'))
        assert admin.get_user(email='anycashuser100@mailinator.com')['lvl'] == 20
        assert admin.get_session(email='anycashuser100@mailinator.com') == User.headers['x-token']
        user1.authorization_by_email(email='anycashuser100@mailinator.com', pwd='Ac*123')
        assert user1.resp_authorization['session']['token'] == admin.get_session('anycashuser100@mailinator.com')

    def test_2(self, _delete_user):
        """ Registration to busy not activated email. """
        User.registration(email='anycashuser100@mailinator.com', pwd='Ac*123')
        User.registration(email='anycashuser100@mailinator.com', pwd='Ac*456')
        User.confirm_registration(code=admin.get_onetime_code(email='anycashuser100@mailinator.com'),
                                  key=User.confirm_key)
        assert admin.get_user(email='anycashuser100@mailinator.com')['lvl'] == 20
        assert admin.get_session(email='anycashuser100@mailinator.com') == User.headers['x-token']
        user1.authorization_by_email(email='anycashuser100@mailinator.com', pwd='Ac*123')
        assert user1.resp_authorization['exception'] == 'NotFound'
        user1.authorization_by_email(email='anycashuser100@mailinator.com', pwd='Ac*456')
        assert user1.resp_authorization['session']['token'] == admin.get_session(email='anycashuser100@mailinator.com')

    def test_3(self, _delete_user):
        """ Registration on busy and activated email. """
        User.registration(email='anycashuser100@mailinator.com', pwd='Ac*123')
        User.confirm_registration(code=admin.get_onetime_code(email='anycashuser100@mailinator.com'),
                                  key=User.confirm_key)
        User.registration(email='anycashuser100@mailinator.com', pwd='Ac*123456')
        assert User.resp_registration['exception'] == 'Unique'
        assert User.resp_registration['field'] == 'email'

    def test_4(self, _delete_user):
        """ Registration with double activation. """
        User.registration(email='anycashuser100@mailinator.com', pwd='Ac*123')
        code = admin.get_onetime_code(email='anycashuser100@mailinator.com')
        User.confirm_registration(code=code, key=User.confirm_key)
        User.confirm_registration(code=code, key=User.confirm_key)
        assert User.resp_confirm['exception'] == 'Auth2Drop'
        assert User.resp_confirm['field'] == 'key'

    def test_5(self, _delete_user):
        """ Registration without one time code. """
        User.registration(email='anycashuser100@mailinator.com', pwd='Ac*123')
        User.confirm_registration(code=None, key=User.confirm_key)
        assert User.resp_confirm['exception'] == 'Auth2Wrong'
        assert admin.get_user(email='anycashuser100@mailinator.com')['lvl'] == 10

    def test_6(self, _delete_user):
        """ Registration with wrong one time code. """
        User.registration(email='anycashuser100@mailinator.com', pwd='Ac*123')
        User.confirm_registration(code='000000', key=User.confirm_key)
        assert User.resp_confirm['exception'] == 'Auth2Wrong'
        assert admin.get_user(email='anycashuser100@mailinator.com')['lvl'] == 10

    def test_7(self, _delete_user):
        """ Registration without confirm token. """
        User.registration(email='anycashuser100@mailinator.com', pwd='Ac#123')
        User.confirm_registration(code=admin.get_onetime_code(email='anycashuser100@mailinator.com'), key=None)
        assert admin.get_user(email='anycashuser100@mailinator.com')['lvl'] == 10

    def test_8(self, _delete_user):
        """ Registration with wrong confirm token. """
        User.registration(email='anycashuser100@mailinator.com', pwd='Ac*123')
        User.confirm_registration(code=admin.get_onetime_code(email='anycashuser100@mailinator.com'), key='1')
        assert User.resp_confirm['exception'] == 'Auth2Drop'
        assert User.resp_confirm['field'] == 'key'
        assert admin.get_user(email='anycashuser100@mailinator.com')['lvl'] == 10

    def test_9(self, _delete_user):
        """ Registration with deactivated confirm token. """
        User.registration(email='anycashuser100@mailinator.com', pwd='Ac*123')
        User.cancel_2auth(key=User.confirm_key)
        User.confirm_registration(code=admin.get_onetime_code(email='anycashuser100@mailinator.com'),
                                  key=User.confirm_key)
        assert User.resp_confirm['exception'] == 'Auth2Drop'
        assert User.resp_confirm['field'] == 'key'
        assert admin.get_user(email='anycashuser100@mailinator.com')['lvl'] == 10

    def test_10(self):
        """ Registration without password. """
        User.registration(email='anycashuser100@mailinator.com', pwd=None)
        assert User.resp_registration['exception'] == 'InvalidParam'

    @pytest.mark.skip(reason='Min pass length - 1 symbol')
    def test_11(self):
        """ Registration with password less than 6 symbols. """
        User.registration(email='anycashuser100@mailinator.com', pwd='Ac*12')
        print(User.resp_registration)
        assert User.resp_registration['exception'] == 'InvalidPassLength'
        assert User.resp_registration['field'] == 'pwd'

    def test_12(self):
        """ Registration without email. """
        User.registration(email=None, pwd='Ac*123')
        assert User.resp_registration['exception'] == 'InvalidLoginType'

    def test_13(self):
        """ Registration with wrong format email: without @. """
        User.registration(email='anycashuser100mailinator.com', pwd='Ac*123')
        assert User.resp_registration['exception'] == 'InvalidParam'
        assert admin.get_user(email='anycashuser100@mailinator.com')['exception'] == "User wasn't found"

    def test_14(self):
        """ Registration with wrong format email: double @. """
        User.registration(email='anycashuser100@@mailinator.com', pwd='Ac*123')
        assert User.resp_registration['exception'] == 'InvalidParam'

    def test_15(self):
        """ Registration with wrong format email: without string before @. """
        User.registration(email='@zzz.com', pwd='Ac*123')
        assert User.resp_registration['exception'] == 'InvalidParam'
        assert admin.get_user(email='@zzz.com')['exception'] == "User wasn't found"

    def test_16(self):
        """ Registration with wrong format email: without domain part. """
        User.registration(email='anycashuser100@mailinator', pwd='Ac*123')
        assert User.resp_registration['exception'] == 'InvalidParam'

    def test_17(self):
        """ Registration with wrong format email: without host part. """
        User.registration(email='anycashuser100@.com', pwd='Ac*123')
        assert User.resp_registration['exception'] == 'InvalidParam'


@pytest.mark.usefixtures('_delete_sessions')
class TestAuthorization:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    @pytest.mark.skip(reason=' Avoiding wrong captcha ')
    def test_1(self):
        """ Authorization in to not real account. """
        user1.authorization_by_email(email='161616@mailinator.com', pwd='Ac*123')
        assert user1.resp_authorization['exception'] == 'NotFound'
        assert user1.resp_authorization['field'] == 'email'
        assert user1.resp_authorization['value'] == '161616@mailinator.com'
        assert admin.get_session(email='161616@mailinator.com')['exception'] == "Session wasn't found"

    def test_2(self):
        """ Success authorization. """
        user1.authorization_by_email(email=user1.email, pwd='123456')
        assert user1.resp_authorization['session']['token'] == admin.get_session(email=user1.email)

    def test_3(self):
        """ Authorization with wrong password. """
        user1.authorization_by_email(email=user1.email, pwd='111111')
        assert user1.resp_authorization['exception'] == 'NotFound'
        assert user1.resp_authorization['field'] == 'pwd'
        assert user1.resp_authorization['value'] == '111111'


    def test_4(self, _delete_auth2type_token, _delete_user,):
        """ Authorization in to not activated account. """
        User.registration(email='anycashuser100@mailinator.com', pwd='Ac*123')
        user1.authorization_by_email(email='anycashuser100@mailinator.com', pwd='Ac*123')
        assert user1.resp_authorization['exception'] == 'Auth2Required'
        assert admin.get_user(email='anycashuser100@mailinator.com')['lvl'] == 10

    def test_5(self, _disable_2type):
        """ Authorization with 2 step authorization. """
        user1.set_2type(tp=0)
        user1.authorization_by_email(email=user1.email, pwd='123456')
        User.confirm_registration(code=admin.get_onetime_code(email=user1.email), key=user1.confirm_key)
        user1.headers = User.headers
        assert User.resp_confirm['session']['token'] == admin.get_session(user1.email)

    @pytest.mark.skip(reason=' Avoiding wrong captcha ')
    def test_6(self, _delete_auth2type_token, _disable_2type):
        """ Authorization with 2 step with wrong code. """
        user1.set_2type(tp=0)
        user1.authorization_by_email(email=user1.email, pwd='123456')
        User.confirm_registration(code='000000', key=user1.confirm_key)
        assert User.resp_confirm['exception'] in ['Auth2Wrong', 'Auth2Drop']

    @pytest.mark.skip(reason=' Avoiding wrong captcha ')
    def test_7(self, _delete_auth2type_token, _disable_2type):
        """ Authorization after deactivated 2 step auth token. """
        user1.set_2type(tp=0)
        user1.authorization_by_email(email=user1.email, pwd='123456')
        code = admin.get_onetime_code(email=user1.email)
        User.cancel_2auth(key=user1.confirm_key)
        User.confirm_registration(code=code, key=user1.confirm_key)
        assert User.resp_confirm['exception'] in ['Auth2Wrong', 'Auth2Drop']

    def test_8(self, _authorization):
        """ Logout user. """
        user1.logout()
        user1.renew_session()
        assert user1.resp_renew['exception'] == 'Unauth'


class TestRenewSession:
    """ Checking renew session. """

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_renew_session_1(self):
        """ Renew session. """
        user1.renew_session()
        assert user1.resp_renew['token'] == admin.get_session(email=user1.email)

    def test_renew_session_2(self, _authorization):
        """ Renew session with wrong session token. """
        user1.headers['x-token'] = '1'
        user1.renew_session()
        assert user1.resp_renew['exception'] == 'Unauth'

    def test_renew_session_3(self):
        """ Authorization without password. """
        user1.authorization_by_email(email=user1.email, pwd=None)
        assert user1.resp_authorization['exception'] == 'NotFound'


class TestForgotPassword:
    """ Checking forgot password. """

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_forgot_session_1(self):
        """ Getting session by forgot password. """
        user1.forgot(email=user1.email)
        User.confirm_registration(code=admin.get_onetime_code(email=user1.email), key=user1.forgot_key, user=user1)
        assert user1.resp_confirm['session']['token'] == admin.get_session(email=user1.email)

    def test_forgot_session_2(self, _disable_2type):
        """ Banned on getting session by forgot password with added two step auth. """
        user1.set_2type(tp=0)
        user1.forgot(email=user1.email)
        assert user1.resp_forgot['exception'] == 'Unavailable'

    def test_forgot_session_3(self):
        """ Getting session by not real email. """
        user1.forgot(email='anycashuser100@mailinator.com')
        assert user1.resp_forgot['exception'] == 'NotFound'

    def test_forgot_session_4(self):
        """ Getting session without email. """
        user1.forgot(email=None)
        assert user1.resp_forgot['exception'] == 'InvalidLoginType'


class TestBalances:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_1(self):
        """ Checking fiat currency. Getting balance with float amount. """
        admin.set_wallet_amount(balance=100150000000, currency='UAH', email=user1.email)
        user1.get_balances(curr='UAH')
        assert user1.resp_balances['UAH'] == '100.15'

    def test_2(self):
        """ Checking fiat currency. Getting balance with int amount. """
        admin.set_wallet_amount(balance=55000000000, currency='USD', email=user1.email)
        user1.get_balances(curr='USD')
        assert user1.resp_balances['USD'] == '55'

    def test_3(self):
        """ Checking crypto currency. Getting balance with float amount. """
        admin.set_wallet_amount(balance=10, currency='BTC', email=user1.email)
        user1.get_balances(curr='BTC')
        assert user1.resp_balances['BTC'] == '0.00000001'

    def test_4(self):
        """ Checking crypto currency. Getting balance with int amount. """
        admin.set_wallet_amount(balance=3000000000, currency='LTC', email=user1.email)
        user1.get_balances(curr='LTC')
        assert user1.resp_balances['LTC'] == '3'

    def test_5(self):
        """ Getting not real currency. """
        user1.get_balances(curr='UDS')
        assert user1.resp_balances['exception'] == 'InvalidCurrency'

    def test_6(self):
        """ Getting all currency. """
        for currency in admin.currency:
            user1.get_balances(curr=currency)
            assert list(user1.resp_balances.keys())[0] in admin.currency


@pytest.mark.usefixtures('create_bonus_level')
class TestBonusLevel:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_bonus_level_1(self):
        """ Showing all levels in system. User hasn't bonus points. """
        admin.set_user_data(user_email=user1.email, is_customfee=False, user=user1, bonus_points=0)
        user1.get_bonuses()
        assert user1.response_bonuses['by_amount'] == [{'amount': '100', 'name': 'Bonus 1', 'points': '5', 'surplus': '0.05'},
                                                       {'amount': '150', 'name': 'Bonus 2', 'points': '10', 'surplus': '0.1'},
                                                       {'amount': '200', 'name': 'Bonus 3', 'points': '15', 'surplus': '0.15'}]
        assert user1.response_bonuses['by_points'] == [{'amount': '100', 'name': 'Bonus 1', 'points': '5', 'surplus': '0.05'},
                                                       {'amount': '150', 'name': 'Bonus 2', 'points': '10', 'surplus': '0.1'},
                                                       {'amount': '200', 'name': 'Bonus 3', 'points': '15', 'surplus': '0.15'}]
        assert user1.response_bonuses['points'] == '0'
        assert user1.response_bonuses['surplus'] == '0'

    def test_bonus_level_2(self):
        """ User hasn't enough bonus points to getting min level. """
        admin.set_user_data(user_email=user1.email, is_customfee=False, user=user1, bonus_points=4990000000)
        user1.get_bonuses()
        assert user1.response_bonuses['points'] == '4.99'
        assert user1.response_bonuses['surplus'] == '0'

    def test_bonus_level_3(self):
        """ User has enough bonus points to getting min level. """
        admin.set_user_data(user_email=user1.email, is_customfee=False, user=user1, bonus_points=5000000000)
        user1.get_bonuses()
        assert user1.response_bonuses['points'] == '5'
        assert user1.response_bonuses['surplus'] == '0.05'

    def test_bonus_level_4(self):
        """ User hasn't enough bonus points to getting second level. """
        admin.set_user_data(user_email=user1.email, is_customfee=False, user=user1, bonus_points=9990000000)
        user1.get_bonuses()
        assert user1.response_bonuses['points'] == '9.99'
        assert user1.response_bonuses['surplus'] == '0.05'

    def test_bonus_level_5(self):
        """ User has enough bonus points to getting second level. """
        admin.set_user_data(user_email=user1.email, is_customfee=False, user=user1, bonus_points=10000000000)
        user1.get_bonuses()
        assert user1.response_bonuses['points'] == '10'
        assert user1.response_bonuses['surplus'] == '0.1'

    def test_bonus_level_6(self):
        """ Disabled showing level: field amount and points equall null. """
        admin.set_bonus_level(name='Bonus 1', points=None, amount=100000000000, surplus=50000000, is_active=True)
        admin.set_bonus_level(name='Bonus 2', points=10000000000, amount=None, surplus=100000000, is_active=True)
        user1.get_bonuses()
        assert user1.response_bonuses['by_amount'] == [{'amount': '100', 'name': 'Bonus 1', 'points': None, 'surplus': '0.05'},
                                                       {'amount': '200', 'name': 'Bonus 3', 'points': '15', 'surplus': '0.15'}]
        assert user1.response_bonuses['by_points'] == [{'amount': None, 'name': 'Bonus 2', 'points': '10', 'surplus': '0.1'},
                                                       {'amount': '200', 'name': 'Bonus 3', 'points': '15', 'surplus': '0.15'}]
        admin.set_bonus_level(name='Bonus 1', points=5000000000, amount=100000000000, surplus=50000000, is_active=True)
        admin.set_bonus_level(name='Bonus 2', points=10000000000, amount=150000000000, surplus=100000000, is_active=True)

    def test_bonus_level_7(self):
        """ Bonus level not for user with 0 points when bonus level has NULL in field 'points'. """
        admin.set_user_data(user_email=user1.email, is_customfee=False, user=user1, bonus_points=0)
        admin.set_bonus_level(name='Bonus 3', points=None, amount=200000000000, surplus=150000000, is_active=True)
        user1.get_bonuses()
        assert user1.response_bonuses['surplus'] == '0'
        admin.set_bonus_level(name='Bonus 3', points=15000000000, amount=200000000000, surplus=150000000, is_active=True)


class TestUpdateEmail:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_update_email_1(self, _drop_email):
        """ Update email and authorization by him. """
        user1.update_email(email='anycashuser1000@mailinator.com')
        user1.confirm_registration(code=admin.get_onetime_code(email=user1.email), key=user1.confirm_key, user=user1)
        assert user1.resp_confirm['session']['token'] == admin.get_session(email='anycashuser1000@mailinator.com')
        assert admin.get_user(email='anycashuser1000@mailinator.com')['email'] == 'anycashuser1000@mailinator.com'
        assert admin.get_session(email=user1.email)['exception'] == "Session wasn't found"
        user1.authorization_by_email(email='anycashuser1000@mailinator.com', pwd='123456')
        assert user1.resp_authorization['session']['token'] == admin.get_session(email='anycashuser1000@mailinator.com')

    def test_update_email_2(self, _disable_2type, _drop_email):
        """ Update email with activated two step auth. """
        user1.set_2type(tp=0)
        user1.update_email(email='anycashuser1000@mailinator.com')
        user1.confirm_registration(code=admin.get_onetime_code(user1.email), key=user1.confirm_key, user=user1)
        assert admin.get_user(email='anycashuser1000@mailinator.com')['exception'] == "User wasn't found"
        user1.second_confirm(code=admin.get_onetime_code(user1.email), key=user1.confirm_key)
        assert admin.get_session(email=user1.email)['exception'] == "Session wasn't found"
        assert user1.resp_second_confirm['session']['token'] == admin.get_session(email='anycashuser1000@mailinator.com')
        assert admin.get_user(email='anycashuser1000@mailinator.com')['email'] == 'anycashuser1000@mailinator.com'

    def test_update_email_3(self):
        """ Update email on booked email. """
        user1.update_email(email=user2.email)
        assert user1.resp_update_email['exception'] == 'Forbidden'
        assert admin.get_user(email='anycashuser1@mailinator.com')['email'] == 'anycashuser1@mailinator.com'

    def test_update_email_4(self):
        """ Update email with wrong format: without string before @. """
        user1.update_email(email='@mailinator.com')
        assert user1.resp_update_email['exception'] == 'InvalidField'

    def test_update_email_5(self):
        """ Update email with wrong format: with @@. """
        user1.update_email(email='anycashuser10@@mailinator.com')
        assert user1.resp_update_email['exception'] == 'InvalidField'
        assert admin.get_user(email='anycashuser10@@mailinator.com')['exception'] == "User wasn't found"

    def test_update_email_6(self):
        """ Update email with wrong format: without domain part after @. """
        user1.update_email(email='anycashuser10@')
        assert user1.resp_update_email['exception'] == 'InvalidField'

    def test_update_email_7(self):
        """ Update email with wrong format: without@. """
        user1.update_email(email='anycashuser10mailinator.com')
        assert user1.resp_update_email['exception'] == 'InvalidField'
        assert admin.get_user(email='anycashuser10mailinator.com')['exception'] == "User wasn't found"

    def test_update_email_8(self):
        """ Update email without email string. """
        user1.update_email(email=None)
        assert user1.resp_update_email['exception'] == 'InvalidField'


@pytest.mark.skip(reason='Save password old format (123456) for real user. ')
class TestUpdatePassword:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_update_password_1(self, _drop_pwd):
        """ Update password. """
        user1.update_pwd(pwd='000000')
        assert user1.resp_update_pwd['session']['token'] == admin.get_session(email=user1.email)
        user1.authorization_by_email(email=user1.email, pwd='000000')
        assert user1.resp_authorization['session']['token'] == admin.get_session(email=user1.email)

    def test_update_password_2(self, _disable_2type, _drop_pwd):
        """ Update password with 2 step auth. """
        user1.set_2type(tp=0)
        user1.update_pwd(pwd='000000')
        user1.confirm_registration(code=admin.get_onetime_code(email=user1.email), key=user1.confirm_key, user=user1)
        assert user1.resp_confirm['session']['token'] == admin.get_session(email=user1.email)
        user1.authorization_by_email(email=user1.email, pwd='000000')
        user1.confirm_registration(code=admin.get_onetime_code(email=user1.email), key=user1.confirm_key, user=user1)
        assert user1.resp_confirm['session']['token'] == admin.get_session(email=user1.email)

    def test_update_password_3(self):
        """ Update password with empty password. """
        user1.update_pwd(pwd=None)
        assert user1.resp_update_pwd['exception'] == 'InvalidParam'
        assert user1.resp_update_pwd['field'] == 'pwd'

    def test_update_password_4(self):
        """ Update password with string shorter than 6 symbols. """
        user1.update_pwd(pwd='12345')
        assert user1.resp_update_pwd['exception'] == 'InvalidPassLength'
        assert user1.resp_update_pwd['field'] == 'pwd_len'
        assert admin.get_session(email=user1.email) == user1.params['token']


class TestUpdateLanguage:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_update_lang_1(self):
        """ Update language. """
        user1.update_lang(lang='en')
        assert user1.headers['x-token'] == admin.get_session(email=user1.email)
        assert admin.get_user(email=user1.email)['lang_id'] == admin.lang_id['en']
        user1.update_lang(lang='ru')

    def test_update_lang_2(self):
        """ Update language with wrong lang id. """
        user1.update_lang(lang='bb')
        assert user1.resp_update_lang['exception'] == 'InvalidField'

    def test_update_lang_3(self):
        """ Update language without lang id. """
        user1.update_lang(lang=None)
        assert user1.resp_update_lang['exception'] == 'InvalidField'


class TestUpdateTimezone:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_update_tz_1(self):
        """ Update timezone. """
        user1.update_tz(tz='Australia/Sydney')
        assert admin.get_user(email=user1.email)['timezone'] == 'Australia/Sydney'
        user1.update_tz(tz='Europe/Amsterdam')

    def test_update_tz_2(self):
        """ Update timezone with wrong format. """
        user1.update_tz(tz='Australia')
        assert user1.resp_update_tz['exception'] == 'InvalidParam'
        assert admin.get_user(email=user1.email)['timezone'] == 'Europe/Amsterdam'

    def test_update_tz_3(self):
        """ Update timezone with None value. """
        user1.update_tz(tz=None)
        assert user1.resp_update_tz['exception'] == 'InvalidParam'


@pytest.mark.skip('Canceled method. ')
class TestUpdateSafemode:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_update_safemode_1(self):
        """ Update safemode. """
        user1.update_safemode(safemode=True)
        assert admin.get_user(email=user1.email)['safemode'] is True
        user1.update_safemode(safemode=False)

    def test_update_safemode_2(self):
        """ Update safemode with None in parameter. """
        user1.update_safemode(safemode=None)
        assert admin.get_user(email=user1.email)['safemode'] is False

    @pytest.mark.skip
    def test_update_safemode_3(self):
        """ Update safemode with non boolean parameter: string. """
        user1.update_safemode(safemode='Test')
        assert admin.get_user(email=user1.email)['safemode'] is False


class TestPaywayList:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    @pytest.mark.skip(reason='Maybe bug')
    def test_payway_list_1(self):
        """ Getting all active payway list. """
        user1.payway_list()
        assert len(user1.resp_payway_list) == admin.pwcurrency(is_active=True)[0]['count']

    def test_payway_list_2(self):
        """ Showing payway without payway fee. """
        admin.set_fee(tp=0, payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'])
        user1.payway_list()
        dct = {(pw['payway'], pw['currency'], pw['direction']): pw['fee'] for pw in user1.resp_payway_list}
        assert dct[('visamc', 'UAH', 'in')] == {'max': '0', 'add': '0', 'method': 'ceil', 'mult': '0', 'min': '0'}

    def test_payway_list_3(self):
        """ Showing payway with common payway fee. """
        admin.set_fee(add=1000000000, mult=10000000, _min=500000000, _max=10000000000,
                      tp=0, payway_id=admin.payway['payeer'], currency_id=admin.currency['USD'])
        user1.payway_list()
        dct = {(pw['payway'], pw['currency'], pw['direction']): pw['fee'] for pw in user1.resp_payway_list}
        assert dct[('payeer', 'USD', 'in')] == {'max': '10', 'add': '1', 'method': 'ceil', 'mult': '0.01', 'min': '0.5'}

    def test_payway_list_4(self, _personal_user_data):
        """ Showing payway with common payway fee + personal payway fee. """
        admin.set_fee(add=10000000000, mult=100000000, _min=10000000000, _max=10000000000,
                      tp=10, payway_id=admin.payway['qiwi'], currency_id=admin.currency['RUB'])
        admin.set_fee(add=5000000000, mult=50000000,  tp=10, payway_id=admin.payway['qiwi'],
                      currency_id=admin.currency['RUB'], owner_id=user1.id, is_active=True)
        user1.payway_list()
        dct = {(pw['payway'], pw['currency'], pw['direction']): pw['fee'] for pw in user1.resp_payway_list}
        assert dct[('qiwi', 'RUB', 'out')] == {'max': '0', 'add': '5', 'method': 'ceil', 'mult': '0.05', 'min': '0'}
        admin.set_fee(payway_id=admin.payway['qiwi'], currency_id=admin.currency['RUB'],
                      owner_id=user1.id, is_active=False)


class TestExchangeList:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_exchange_list_1(self):
        """ Get information by course: exchange UAH to USD with base rate 28.1999 and fee 0.05%. """
        admin.set_rate_exchange(rate=28199900000, fee=500000, in_currency='UAH', out_currency='USD')
        user1.exchange_list(in_curr='UAH', out_curr='USD')
        assert user1.resp_exchange_list['data'][0]['fee'] == '0.0005'
        assert user1.resp_exchange_list['data'][0]['i_rate'] == ['28.1999', '1']
        assert user1.resp_exchange_list['data'][0]['rate'] == ['28.214', '1']
        assert user1.resp_exchange_list['data'][0]['in_min'] == '0.01'
        assert user1.resp_exchange_list['data'][0]['in_max'] == '3000'
        assert len(user1.resp_exchange_list['data']) == 1

    def test_exchange_list_2(self):
        """ Get information by course: exchange UAH to all currency with common fee for exchange. """
        admin.set_rate_exchange(rate=2480000000, fee=10000000, in_currency='UAH', out_currency='RUB')
        admin.set_rate_exchange(rate=28199900000, fee=20000000, in_currency='UAH', out_currency='USD')
        user1.exchange_list(in_curr='UAH', out_curr=None)
        dct = {(ex['in_curr'], ex['out_curr']): ex['rate'] for ex in user1.resp_exchange_list['data']}
        assert dct[('UAH', 'USD')] == ['28.7639', '1']
        assert dct[('UAH', 'RUB')] == ['1', '2.4552']

    @pytest.mark.skip(reason='Not include bonus in showing currency exchange. ')
    def test_exchange_list_3(self):
        """ Get information by course: exchange UAH to USD currency with common fee for exchange and bonus. """
        admin.set_rate_exchange(rate=28199900000, fee=20000000, in_currency='UAH', out_currency='USD')
        admin.set_user_data(user_email=user1.email, bonus_points=5000000000, is_customfee=False, user=user1)
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=True)
        user1.exchange_list(in_curr='UAH', out_curr='USD')
        assert user1.resp_exchange_list['data'][0]['fee'] == '0.01'
        assert user1.resp_exchange_list['data'][0]['i_rate'] == ['28.1999', '1']
        assert user1.resp_exchange_list['data'][0]['rate'] == ['28.4819', '1']
        admin.set_user_data(user_email=user1.email, bonus_points=0, is_customfee=False, user=user1)
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=False)

    @pytest.mark.skip(reason='Not include bonus in showing currency exchange. ')
    def test_exchange_list_4(self):
        """ Get information by course: exchange USD to UAH currency with common fee for exchange, with bonus and with
            personal exchange fee. """
        admin.set_rate_exchange(rate=28199900000, fee=30000000, in_currency='USD', out_currency='UAH')
        admin.set_user_data(user_email=user1.email, bonus_points=5000000000, is_customfee=True, user=user1)
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=True)
        admin.set_personal_exchange_fee(is_active=True, in_currency='USD', out_currency='UAH', fee=10000000)
        user1.exchange_list(in_curr='USD', out_curr=None)
        dct = {(ex['in_curr'], ex['out_curr']): ex['rate'] for ex in user1.resp_exchange_list['data']}
        assert dct[('USD', 'UAH')] == ['1', '27.9179']
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=False)
        admin.set_user_data(user_email=user1.email, bonus_points=0, is_customfee=False, user=user1)


class TestUserdata:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_userdata_1(self):
        """ Creating three userdata on different payway and delete them.  """
        user1.userdata_create(payway='qiwi', account='+380663319145', curr='RUB', coment='Рубли киви')
        ac_1 = user1.resp_userdata_create['id']
        assert user1.resp_userdata_create['account'] == '380663319145'
        assert user1.resp_userdata_create['comment'] == 'Рубли киви'
        assert user1.resp_userdata_create['curr'] == 'RUB'
        user1.userdata_create(payway='webmoney', account='Z123456789012', curr='USD', coment='Webmoney-dollars')
        ac_2 = user1.resp_userdata_create['id']
        assert user1.resp_userdata_create['account'] == 'Z123456789012'
        assert user1.resp_userdata_create['payway'] == 'webmoney'
        user1.userdata_create(payway='btc', account='3Bkt33hmeak9HFXywcE54Go1TrULEv638t', curr='BTC', coment='Биткоиныыыыыыыы')
        ac_3 = user1.resp_userdata_create['id']
        assert user1.resp_userdata_create['account'] == '3Bkt33hmeak9HFXywcE54Go1TrULEv638t'
        user1.userdata_delete(oid=ac_1)
        user1.userdata_delete(oid=ac_2)
        user1.userdata_delete(oid=ac_3)
        user1.userdata_get(oid=ac_1)
        assert user1.resp_userdata_get['exception'] == 'NotFound'

    def test_userdata_2(self):
        """ Getting real userdata.  """
        user1.userdata_get(oid='1037938976745400')
        assert user1.resp_userdata_get == {'account': '380663333333', 'comment': 'КиВи', 'ctime': 1556723417354,
                                           'curr': 'RUB', 'id': 1037938976745400, 'payway': 'qiwi'}

    def test_userdata_3(self):
        """ Getting not real userdata. """
        user1.userdata_get(oid='1037938976745433')
        assert user1.resp_userdata_get['exception'] == 'NotFound'

    def test_userdata_4(self):
        """ Getting all userdata by list. """
        user1.userdata_list(payway=None, account=None, curr=None, first=None, count=None)
        assert len(user1.resp_userdata_list['data']) == 3

    def test_userdata_5(self):
        """ Getting all userdata by currency. """
        user1.userdata_list(payway=None, account=None, curr='RUB', first=None, count=None)
        ls = [ls['curr'] for ls in user1.resp_userdata_list['data']]
        assert equal_list(ls, 'RUB')
        assert len(ls) == 2

    def test_userdata_6(self):
        """ Getting all userdata by payway. """
        user1.userdata_list(payway='webmoney', account=None, curr=None, first=None, count=None)
        ls = [ls['payway'] for ls in user1.resp_userdata_list['data']]
        assert equal_list(ls, 'webmoney')
        assert len(ls) == 2

    def test_userdata_7(self):
        """ Getting all userdata by payway and curr. """
        user1.userdata_list(payway='webmoney', account=None, curr='UAH', first=None, count=None)
        ls = [(ls['payway'], ls['curr']) for ls in user1.resp_userdata_list['data']]
        assert equal_list(ls, ('webmoney', 'UAH'))
        assert len(ls) == 1

    def test_userdata_8(self):
        """ Getting userdata by account. """
        user1.userdata_list(payway=None, account='380663333333', curr=None, first=None, count=None)
        assert user1.resp_userdata_list['data'][0]['account'] == '380663333333'
        assert len(user1.resp_userdata_list['data']) == 1

    def test_userdata_9(self):
        """ Getting all userdata by first and count. """
        user1.userdata_list(payway=None, account=None, curr=None, first=0, count=2)
        assert len(user1.resp_userdata_list['data']) == 2

    def test_userdata_10(self):
        user1.userdata_validate(payway='qiwi', account='380663333333')
        assert user1.resp_userdata_validate == '380663333333'


class TestAdress:
    """ Testing crypto adress. """

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_cryptoadress_1(self):
        """ Showing all crypto currency. """
        user1.adress_currencies()
        currencies = admin.currencies()
        ls_crypto = [curr for curr in currencies if currencies[curr]['is_active'] is True and currencies[curr]['is_crypto'] is True]
        user1.resp_adress_currencies.sort()
        ls_crypto.sort()
        assert user1.resp_adress_currencies == ls_crypto

    def test_cryptoadress_2(self):
        """ Getting info by adress. """
        user1.adress_get(oid='844424930183362')
        assert user1.resp_adress_get['in_curr'] == 'BTC'
        assert user1.resp_adress_get['link'] == '3EmYb7qjfmyGDgYHyRLeuLgxkJo5To27e6'
        assert user1.resp_adress_get['name'] == '3EmYb7qjfmyGDgYHyRLeuLgxkJo5To27e6'

    def test_cryptoadress_3(self):
        """ Getting info for not real adress. """
        user1.adress_get(oid='844424930183000')
        assert user1.resp_adress_get['exception'] == 'NotFound'

    def test_cryptoadress_4(self):
        """ Getting info for not owner adress. """
        user1.adress_get(oid='844424930227005')
        assert user1.resp_adress_get['exception'] == 'NotFound'

    def test_cryptoadress_5(self):
        """ Changing name. """
        user1.adress_edit(oid='844424930183362', comment='BTC !@#$%^&*( <> {}')
        assert user1.resp_adress_edit['comment'] == 'BTC !@#$%^&*( <> {}'
        user1.adress_get(oid='844424930183362')
        assert user1.resp_adress_get['comment'] == 'BTC !@#$%^&*( <> {}'
        user1.adress_edit(oid='844424930183362', comment='Биткоин кошелек! ')
        assert user1.resp_adress_edit['comment'] == 'Биткоин кошелек! '
        user1.adress_get(oid='844424930183362')
        assert user1.resp_adress_get['comment'] == 'Биткоин кошелек! '
        user1.adress_edit(oid='844424930183362', comment=None)
        assert user1.resp_adress_edit['comment'] is None
        user1.adress_get(oid='844424930183362')
        assert user1.resp_adress_get['comment'] is None

    @pytest.mark.skip # NEED COMPLETE
    def test_cryptoadress_6(self):
        """ Getting out currencies list. """
        user1.adress_out_currencies(in_curr='BTC')
        assert user1.resp_adress_out_currencies == []

    def test_cryptoadress_7(self):
        """ Getting out currencies with not real in_currency. """
        user1.adress_out_currencies(in_curr='TBC')
        assert user1.resp_adress_out_currencies['exception'] == 'InvalidCurrency'

    @pytest.mark.skip(reason='Disable exchange')
    def test_cryptoadress_8(self):
        """ Params for crypto payin with exchange. """
        user1.adress_params(in_curr='BTC', out_curr='USD')
        assert user1.resp_adress_params['exception'] == 'InvalidCurrency'

    @pytest.mark.skip(reason='Avoiding creating for too much adress ')
    def test_cryptoadress_9(self):
        """ Creating cryptoadress for payin without exchange. """
        user1.adress_create(in_curr='BTC', out_curr='BTC')
        assert user1.resp_adress_create['in_curr'] == 'BTC'
        assert user1.resp_adress_create['status'] == 'new'

    @pytest.mark.skip(reason='Disable exchange')
    def test_cryptoadress_10(self):
        """ Creating cryptoadress for payin with exchange. """
        user1.adress_create(in_curr='BTC', out_curr='USD')
        assert user1.resp_adress_create['in_curr'] == 'BTC'
        assert user1.resp_adress_create['out_curr'] == 'USD'

    def test_cryptoadress_11(self):
        """ Getting all cryptoadress. """
        user1.adress_list(first=None, count=100, in_curr=None, out_curr=None)
        assert len(user1.resp_adress_list['data']) == len(admin.get_cryptoadress(email=user1.email))

    def test_cryptoadress_12(self):
        """ Getting all cryptoadress for in_curr. """
        user1.adress_list(first=None, count=100, in_curr='BTC', out_curr=None)
        ls_crypto = [cr['in_curr'] for cr in user1.resp_adress_list['data']]
        assert equal_list(ls_crypto, 'BTC')
        assert 'BTC' in ls_crypto

    def test_cryptoadress_13(self):
        """ Getting all cryptoadress for in_curr and out_curr. """
        user1.adress_list(first=None, count=100, in_curr='BTC', out_curr='USD')
        ls_crypto = [(cr['in_curr'], cr['out_curr']) for cr in user1.resp_adress_list['data']]
        assert equal_list(ls_crypto, ('BTC', 'USD'))
        assert ('BTC', 'USD') in ls_crypto

    def test_cryptoadress_14(self):
        """ Getting list of two elements. """
        user1.adress_list(first=None, count=2, in_curr='BTC', out_curr=None)
        assert len(user1.resp_adress_list['data']) == 2


@pytest.mark.skip
class TestOrderHistory:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_order_history_1(self):
        """ Showing all orders by users with COUNT parameter. """
        user1.order_history(count=10)
        assert user1.resp_order_history['count'] == 10

    def test_order_history_2(self):
        """ Showing 50 orders by users with TP parameter. """
        user1.order_history(tp='payin', count=50)
        ls = [tp['tp'] for tp in user1.resp_order_history['data']]
        assert not 'payin' not in ls and 'payin' in ls
        assert len(ls) == 50

    def test_order_history_3(self):
        """ Showing 10 orders by user with in_curr='UAH' and out_curr=USD for tp=payin. """
        user1.order_history(tp='payin', in_curr='UAH', out_curr='USD', count=10)
        ls = [(tp['in_curr'], tp['out_curr'], tp['tp']) for tp in user1.resp_order_history['data']]
        assert not ('UAH', 'USD', 'payin') not in ls and ('UAH', 'USD', 'payin') in ls
        assert len(ls) == 10

    def test_order_history_4(self):
        """ Showing 5 orders by user with in_curr='UAH' and out_curr=USD for tp=payin and payway='visamc'. """
        user1.order_history(tp='payin', in_curr='UAH', out_curr='USD', payway='visamc', count=5)
        ls = [(tp['in_curr'], tp['out_curr'], tp['tp'], tp['payway_name']) for tp in user1.resp_order_history['data']]
        assert not ('UAH', 'USD', 'payin', 'visamc') not in ls and ('UAH', 'USD', 'payin', 'visamc')
        assert len(ls) == 5

    def test_order_history_5(self):
        """ Showing all orders by user with in_curr='UAH' and out_curr=USD for tp=payin and payway='visamc' and status
            not done. """
        user1.order_history(tp='payin', in_curr='UAH', out_curr='USD', payway='visamc', final=False)
        ls = [(tp['in_curr'], tp['out_curr'], tp['tp'], tp['payway_name']) for tp in user1.resp_order_history['data']]
        ls_status = [tp['status'] for tp in user1.resp_order_history['data']]
        assert not ('UAH', 'USD', 'payin', 'visamc') not in ls
        assert 'done' not in ls_status

    def test_order_history_6(self):
        """ Showing all orders by user with tp=payin and payway='visamc+btc'. """
        user1.order_history(tp='payout', payway='visamc, btc', final=False)
        ls_payway = [tp['payway_name'] for tp in user1.resp_order_history['data']]
        assert 'visamc' in ls_payway
        assert 'btc' in ls_payway

@pytest.mark.skip
class TestIbillList:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_ibill_list_1(self):
        """ Showing 10 last ibills. """
        user1.ibill_list(count=10)
        assert user1.resp_ibill_list['count'] == 10

    def test_ibill_list_2(self):
        """ Showing 10 ibills for curr 'UAH'. """
        user1.ibill_list(curr='UAH', count=15)
        ls_curr = [tp['curr'] for tp in user1.resp_ibill_list['data']]
        assert not 'UAH' not in ls_curr and 'UAH' in ls_curr
        assert len(ls_curr) == 15

    def test_ibill_list_3(self):
        """ Showing 5 ibills for curr USD and group CONVERT. """
        user1.ibill_list(curr='USD', count=5, group='convert')
        ls = [(tp['curr'], tp['model']) for tp in user1.resp_ibill_list['data']]
        assert not ('USD', 'convert') not in ls and ('USD', 'convert') in ls
        assert len(ls) == 5

    def test_ibill_list_4(self):
        """ Showing all ibills for UAH and group outpay. """
        user1.ibill_list(curr='UAH', first='1', count='10', group='outpay')
        ls = [(tp['curr'], tp['tp']) for tp in user1.resp_ibill_list['data']]
        assert ('UAH', 'transfer') in ls
        assert len(ls) == 10

    def test_ibill_list_5(self):
        """ Showing all ibills by order lid. """
        user1.ibill_list(order_id='3361')
        assert user1.resp_ibill_list['data'][0]['lid'] == 3361
        assert user1.resp_ibill_list['data'][1]['lid'] == 3361
        user1.ibill_list(curr='UAH', order_id='3361')
        assert len(user1.resp_ibill_list['data']) == 2
        assert user1.resp_ibill_list['data'][0]['curr'] == 'UAH'
