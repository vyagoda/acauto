import pytest
import requests
import json


class TestPayinWithoutExchange:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_payin_1(self):
        """ Payin 1 UAH from VISAMC without any fee and exchange. """
        admin.set_fee(payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'], tp=0)
        user1.payin_params(payway='visamc', in_curr='UAH', out_curr='UAH')
        assert user1.response_payin_params['in_fee'] == {'add': '0', 'max': '0', 'method': 'ceil', 'min': '0', 'mult': '0'}
        assert user1.response_payin_params['min'] == '0.01'
        assert user1.response_payin_params['max'] == '3000'
        user1.payin_calc(payway='visamc', amount=3000.01, in_curr='UAH', out_curr='UAH')
        assert user1.response_payin_calc['exception'] == 'AmountTooBig'
        user1.payin(payway='visamc', amount=0.01, in_curr='UAH', out_curr='UAH')
        assert user1.response_payin['account_amount'] == '0.01'
        assert user1.response_payin['in_amount'] == '0.01'
        assert user1.response_payin['out_amount'] == '0.01'
        user1.payin_list(in_curr='UAH', out_curr='UAH')
        assert user1.response_payin_list['data'][0]['account_amount'] == '0.01'
        assert user1.response_payin_list['data'][0]['out_amount'] == '0.01'

    def test_payin_2(self):
        """ Payin 1.02 USD from PAYEER without fee. """
        admin.set_fee(payway_id=admin.payway['payeer'], currency_id=admin.currency['USD'], tp=0)
        user1.payin_calc(payway='payeer', amount=1.02, in_curr='USD', out_curr='USD')
        assert user1.response_payin_calc['account_amount'] == '1.02'
        assert user1.response_payin_calc['in_fee_amount'] == '0'
        assert user1.response_payin_calc['out_fee_amount'] == '0'
        user1.payin(payway='payeer', amount=1.02, in_curr='USD', out_curr='USD')
        assert user1.response_payin['in_amount'] == '1.02'
        assert user1.response_payin['out_amount'] == '1.02'
        assert user1.response_payin['in_curr'] == 'USD'
        assert user1.response_payin['out_curr'] == 'USD'

    def test_payin_3(self):
        """ Payin 0.99999 BTC without any fee and without exchange. """
        admin.set_fee(payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'], tp=0)
        user1.payin(payway='btc', amount=0.99999, in_curr='BTC', out_curr='BTC')
        assert user1.response_payin['account_amount'] == '0.99999'
        assert user1.response_payin['in_amount'] == '0.99999'
        assert user1.response_payin['out_amount'] == '0.99999'
        assert user1.response_payin['out_curr'] == 'BTC'

    @pytest.mark.skip('Inactive privat24 payway')
    def test_payin_4(self):
        """ Payin 10 UAH from PRIVAT24 with common percent fee 20%, without convertation. """
        admin.set_fee(mult=200000000, payway_id=admin.payway['privat24'], currency_id=admin.currency['UAH'])
        user1.payin(payway='privat24', amount=10, in_curr='UAH', out_curr='UAH')
        assert user1.response_payin['account_amount'] == '8'
        assert user1.response_payin['in_amount'] == '10'
        assert user1.response_payin['out_amount'] == '10'
        assert user1.response_payin['in_fee_amount'] == '2'
        assert user1.response_payin['out_fee_amount'] == '2'

    # @pytest.mark.skip # Disabled currency  LTC
    def test_payin_5(self):
        """ Payin 0.9 LTC with common percent fee 0.01%, without convertation. """
        admin.set_fee(mult=100000, payway_id=admin.payway['ltc'], currency_id=admin.currency['LTC'])
        user1.payin(payway='ltc', amount=0.9, in_curr='LTC', out_curr='LTC')
        assert user1.response_payin['account_amount'] == '0.89991'
        assert user1.response_payin['in_amount'] == '0.9'
        assert user1.response_payin['out_amount'] == '0.9'
        assert user1.response_payin['in_fee_amount'] == '0.00009'
        assert user1.response_payin['out_fee_amount'] == '0.00009'

    @pytest.mark.skip(reason='Disabled Cash_kiev payway. ')
    def test_payin_6(self, _personal_user_data):
        """ Payin 10 UAH from CASH_KIEV with personal percent fee 20%, without convertation. """
        admin.set_fee(mult=200000000, payway_id=admin.payway['cash_kiev'], currency_id=admin.currency['UAH'],
                      owner_id=user1.id, is_active=True)
        user1.payin(payway='cash_kiev', amount=10, in_curr='UAH', out_curr='UAH', payee='+380661234567')
        assert user1.response_payin['account_amount'] == '8'
        assert user1.response_payin['in_amount'] == '10'
        assert user1.response_payin['out_amount'] == '10'
        assert user1.response_payin['in_fee_amount'] == '2'
        assert user1.response_payin['out_fee_amount'] == '2'
        admin.set_fee(payway_id=admin.payway['cash_kiev'], currency_id=admin.currency['UAH'], owner_id=user1.id,
                      is_active=False)

    def test_payin_7(self, _personal_user_data):
        """ Payin 0.9 BTC from btc with personal percent fee 0.01%, without convertation. """
        admin.set_fee(mult=100000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'],
                      owner_id=user1.id, is_active=True)
        user1.payin(payway='btc', amount=0.9, in_curr='BTC', out_curr='BTC')
        assert user1.response_payin['account_amount'] == '0.89991'
        assert user1.response_payin['in_amount'] == '0.9'
        assert user1.response_payin['out_amount'] == '0.9'
        assert user1.response_payin['in_fee_amount'] == '0.00009'
        assert user1.response_payin['out_fee_amount'] == '0.00009'
        admin.set_fee(payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'], owner_id=user1.id,
                      is_active=False)

    def test_payin_8(self):
        """ Payin 10 USD from PERFECT with common absolute fee 5.55 USD. """
        admin.set_fee(add=5550000000, payway_id=admin.payway['perfect'], currency_id=admin.currency['USD'])
        user1.payin(payway='perfect', amount=10, in_curr='USD', out_curr='USD')
        assert user1.response_payin['account_amount'] == '4.45'
        assert user1.response_payin['in_curr'] == 'USD'
        assert user1.response_payin['out_curr'] == 'USD'

    def test_payin_9(self, _personal_user_data):
        """ Payin 10 RUB from QIWI with personal absolute fee 0.01 RUB, without convertation. """
        admin.set_fee(add=10000000, payway_id=admin.payway['qiwi'], currency_id=admin.currency['RUB'],
                      owner_id=user1.id, is_active=True)
        user1.payin(payway='qiwi', amount=10, in_curr='RUB', out_curr='RUB', payer='+380661234567', email=user1.email)
        assert user1.response_payin['account_amount'] == '9.99'
        assert user1.response_payin['in_amount'] == '10'
        assert user1.response_payin['out_amount'] == '10'
        assert user1.response_payin['in_fee_amount'] == '0.01'
        assert user1.response_payin['out_fee_amount'] == '0.01'
        admin.set_fee(payway_id=admin.payway['qiwi'], currency_id=admin.currency['RUB'], owner_id=user1.id,
                      is_active=False)

    #@pytest.mark.skip  # Disabled currency  ETH
    def test_payin_10(self, _personal_user_data):
        """ Payin 0.9 ETH with personal absolute fee 0.0006 ETH, without convertation. """
        admin.set_fee(add=600000, payway_id=admin.payway['eth'], currency_id=admin.currency['ETH'],
                      owner_id=user1.id, is_active=True)
        user1.payin_calc(payway='eth', amount=0.9, in_curr='ETH', out_curr='ETH')
        assert user1.response_payin_calc['account_amount'] == '0.8994'
        assert user1.response_payin_calc['in_fee_amount'] == '0.0006'
        user1.payin(payway='eth', amount=0.9, in_curr='ETH', out_curr='ETH')
        assert user1.response_payin['account_amount'] == '0.8994'
        assert user1.response_payin['in_amount'] == '0.9'
        assert user1.response_payin['out_amount'] == '0.9'
        assert user1.response_payin['in_fee_amount'] == '0.0006'
        assert user1.response_payin['out_fee_amount'] == '0.0006'
        admin.set_fee(payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'], owner_id=user1.id,
                      is_active=False)

    def test_payin_11(self, _personal_user_data):
        """ Payin 10 UAH from VISAMC with common percent fee 10% and personal percent fee 5% without convertation. """
        admin.set_fee(mult=50000000, payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'],
                      owner_id=user1.id, is_active=True)
        admin.set_fee(mult=100000000, payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'])
        user1.payin(payway='visamc', amount=10, in_curr='UAH', out_curr='UAH')
        assert user1.response_payin['account_amount'] == '9.5'
        assert user1.response_payin['in_amount'] == '10'
        assert user1.response_payin['out_amount'] == '10'
        assert user1.response_payin['in_fee_amount'] == '0.5'
        assert user1.response_payin['out_fee_amount'] == '0.5'
        admin.set_fee(payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'], owner_id=user1.id,
                      is_active=False)

    @pytest.mark.skip  # Disabled currency  BCH
    def test_payin_12(self, _personal_user_data):
        """ Payin 0.9 BCH with personal percent fee 5% and common percent fee 10%, without convertation. """
        admin.set_fee(mult=50000000, payway_id=admin.payway['bch'], currency_id=admin.currency['BCH'],
                      owner_id=user1.id, is_active=True)
        admin.set_fee(mult=100000000, payway_id=admin.payway['bch'], currency_id=admin.currency['BCH'])
        user1.payin(payway='bch', amount=0.9, in_curr='BCH', out_curr='BCH')
        assert user1.response_payin['account_amount'] == '0.855'
        assert user1.response_payin['in_amount'] == '0.9'
        assert user1.response_payin['out_amount'] == '0.9'
        assert user1.response_payin['out_fee_amount'] == '0.045'
        admin.set_fee(payway_id=admin.payway['bch'], currency_id=admin.currency['BCH'], owner_id=user1.id,
                      is_active=False)

    def test_payin_13(self, _personal_user_data):
        """ Payin 10 RUB from PAYEER with common percent fee 10%, personal absolute fee 5 RUB, without convertation. """
        admin.set_fee(add=5000000000, payway_id=admin.payway['payeer'], currency_id=admin.currency['RUB'],
                      owner_id=user1.id, is_active=True)
        admin.set_fee(mult=100000000, payway_id=admin.payway['payeer'], currency_id=admin.currency['RUB'])
        user1.payin(payway='payeer', amount=10, in_curr='RUB', out_curr='RUB')
        assert user1.response_payin['account_amount'] == '5'
        assert user1.response_payin['in_amount'] == '10'
        assert user1.response_payin['out_amount'] == '10'
        admin.set_fee(payway_id=admin.payway['payeer'], currency_id=admin.currency['RUB'], owner_id=user1.id, is_active=False)

    def test_payin_14(self, _personal_user_data):
        """ Payin 0.9 BTC with personal absolute fee 0.002 BTC and common percent fee 10%, without convertation. """
        admin.set_fee(add=2000000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'],
                      owner_id=user1.id, is_active=True)
        admin.set_fee(mult=100000000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'])
        user1.payin(payway='btc', amount=0.9, in_curr='BTC', out_curr='BTC')
        assert user1.response_payin['account_amount'] == '0.898'
        assert user1.response_payin['in_amount'] == '0.9'
        assert user1.response_payin['out_amount'] == '0.9'
        admin.set_fee(payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'], owner_id=user1.id,
                      is_active=False)

    def test_payin_15(self, _personal_user_data):
        """ Payin 10 USD from ADVCASH with personal absolute fee 5 USD and personal percent fee 10%,
            without convertation. """
        admin.set_fee(mult=100000000, add=5000000000, payway_id=admin.payway['advcash'], currency_id=admin.currency['USD'],
                      owner_id=user1.id, is_active=True)
        user1.payin(payway='advcash', amount=10, in_curr='USD', out_curr='USD')
        assert user1.response_payin['account_amount'] == '4'
        assert user1.response_payin['in_fee_amount'] == '6'
        assert user1.response_payin['out_fee_amount'] == '6'
        user1.payin_get(lid=user1.payin_lid)
        assert user1.response_payin_get['lid'] == user1.payin_lid
        assert user1.response_payin_get['account_amount'] == '4'
        admin.set_fee(payway_id=admin.payway['advcash'], currency_id=admin.currency['USD'], owner_id=user1.id,
                      is_active=False)

    @pytest.mark.skip  # Disabled currency  USDT
    def test_payin_16(self, _personal_user_data):
        """ Payin 10 USDT with common absolute fee 3 USDT and personal absolute fee 4 USDT, without convertation. """
        admin.set_fee(add=3000000000, payway_id=admin.payway['usdt'], currency_id=admin.currency['USDT'])
        admin.set_fee(add=4000000000, payway_id=admin.payway['usdt'], currency_id=admin.currency['USDT'],
                      owner_id=user1.id, is_active=True)
        user1.payin_params(payway='usdt', in_curr='USDT', out_curr='USDT')
        assert user1.response_payin_params['in_fee'] == {'add': '4', 'max': '0', 'method': 'ceil',
                                                         'min': '0', 'mult': '0'}
        user1.payin(payway='usdt', amount=10, in_curr='USDT', out_curr='USDT')
        assert user1.response_payin['account_amount'] == '6'
        assert user1.response_payin['in_fee_amount'] == '4'
        admin.set_fee(payway_id=admin.payway['usdt'], currency_id=admin.currency['USDT'], owner_id=user1.id,
                      is_active=False)

    @pytest.mark.skip(reason='Disabled CASH payway')
    def test_payin_17(self, _personal_user_data):
        """ Payin 10 USD from CASH_MOSCOW with common percent fee 10%, common absolute fee 3 USD, personal percent fee 5%
            and personal absolute fee 4 USD. """
        admin.set_fee(mult=100000000, add=3000000000, payway_id=admin.payway['cash_moscow'], currency_id=admin.currency['USD'])
        admin.set_fee(mult=50000000, add=4000000000, payway_id=admin.payway['cash_moscow'], currency_id=admin.currency['USD'],
                      owner_id=user1.id, is_active=True)
        user1.payin_calc(payway='cash_moscow', amount=10, in_curr='USD', out_curr='USD')
        assert user1.response_payin_calc['account_amount'] == '5.5'
        assert user1.response_payin_calc['in_fee_amount'] == '4.5'
        assert user1.response_payin_calc['out_fee_amount'] == '4.5'
        user1.payin(payway='cash_moscow', amount=10, in_curr='USD', out_curr='USD', payee='@bobik11')
        assert user1.response_payin['account_amount'] == '5.5'
        assert user1.response_payin['in_amount'] == '10'
        assert user1.response_payin['out_amount'] == '10'
        assert user1.response_payin['in_fee_amount'] == '4.5'
        admin.set_fee(payway_id=admin.payway['cash_moscow'], currency_id=admin.currency['USD'], owner_id=user1.id, is_active=False)


    def test_payin_18(self, _personal_user_data):
        """ Payin 0.9 BTC with common percent fee 10%, common absolute fee 0.2 BTC, personal percent fee 5%
            and personal absolute fee 0.1 BTC. """
        admin.set_fee(mult=100000000, add=200000000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'])
        admin.set_fee(mult=50000000, add=100000000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'],
                      _max=1000000000, _min=1000000, owner_id=user1.id, is_active=True)
        user1.payin_params(payway='btc', in_curr='BTC', out_curr='BTC')
        assert user1.response_payin_params['in_fee'] == {'add': '0.1', 'max': '1', 'method': 'ceil', 'min': '0.001', 'mult': '0.05'}
        assert user1.response_payin_params['max'] == '3000'
        user1.payin(payway='btc', amount=0.9, in_curr='BTC', out_curr='BTC')
        assert user1.response_payin['account_amount'] == '0.755'
        assert user1.response_payin['out_fee_amount'] == '0.145'
        admin.set_fee(payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'], owner_id=user1.id,
                      is_active=False)


class TestPayinWithExchange:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session


    def test_payin_19(self):
        """ Payin 200 RUB from PAYEER with exchange to UAH common percent fee 2% and common absolute fee 1 RUB with
            common percent fee 6% for exchange with operations bonus 1 USD = 1%. """
        admin.set_fee(mult=20000000, add=1000000000, payway_id=admin.payway['payeer'], currency_id=admin.currency['RUB'])
        admin.set_rate_exchange(rate=2666600000, fee=60000000, in_currency='RUB', out_currency='UAH')
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=True)
        user1.payin(payway='payeer', amount=200, in_curr='RUB', out_curr='UAH')
        assert user1.response_payin['account_amount'] == '69.64'
        assert user1.response_payin['in_amount'] == '200'
        assert user1.response_payin['out_amount'] == '71.43'
        assert user1.response_payin['in_fee_amount'] == '5'
        assert user1.response_payin['out_fee_amount'] == '1.79'
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=False)

    @pytest.mark.skip(reason='Disable exchange')
    def test_payin_20(self):
        """ Payin 0.003 BTC with exchange to UAH with common percent fee 5% and common absolute fee 0.0002 BTC
            with common percent fee 6% for exchange with collected bonus 5 points = 1%. """
        admin.set_fee(mult=50000000, add=200000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'])
        admin.set_user_data(user_email=user1.email, bonus_points=5000000000, is_customfee=False, user=user1)
        admin.set_rate_exchange(rate=3580654100000, fee=60000000, in_currency='BTC', out_currency='USD')
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=True)
        user1.payin_params(payway='btc', in_curr='BTC', out_curr='USD')
        assert user1.response_payin_params['in_fee'] == {'add': '0.0002', 'mult': '0.05', 'method': 'ceil', 'min': '0',
                                                         'max': '0'}
        user1.payin(payway='btc', amount=0.003, in_curr='BTC', out_curr='USD')
        assert user1.response_payin['account_amount'] == '9'
        assert user1.response_payin['in_amount'] == '0.003'
        assert user1.response_payin['out_amount'] == '10.2'
        assert user1.response_payin['in_fee_amount'] == '0.00035'
        assert user1.response_payin['out_fee_amount'] == '1.2'
        assert user1.response_payin['out_curr'] == 'USD'
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=10000000, is_active=False)
        admin.set_user_data(user_email=user1.email, is_customfee=False, user=user1)


    def test_payin_21(self, _personal_user_data):
        """ Payin 200 RUB from QIWI with exchange to UAH common percent fee 3%, common absolute fee 2 RUB,
        personal percent fee 2%, personal absolute fee 1 RUB and common persent for exchange 5%. """
        admin.set_fee(mult=30000000, add=2000000000, payway_id=admin.payway['qiwi'], currency_id=admin.currency['RUB'])
        admin.set_fee(mult=20000000, add=1000000000, payway_id=admin.payway['qiwi'], currency_id=admin.currency['RUB'],
                      owner_id=user1.id, is_active=True)
        admin.set_rate_exchange(rate=2666600000, fee=50000000, in_currency='RUB', out_currency='UAH')
        user1.payin(payway='qiwi', amount=200, in_curr='RUB', out_curr='UAH', payer='+380661234567', email='user1.email')
        assert user1.response_payin['account_amount'] == '69.64'
        assert user1.response_payin['in_amount'] == '200'
        assert user1.response_payin['out_amount'] == '71.43'
        assert user1.response_payin['in_fee_amount'] == '5'
        assert user1.response_payin['out_fee_amount'] == '1.79'
        admin.set_fee(payway_id=admin.payway['qiwi'], currency_id=admin.currency['RUB'], owner_id=user1.id, is_active=False)

    @pytest.mark.skip(reason='Disable exchange')
    def test_payin_22(self, _personal_user_data):
        """ Payin 0.005 BTC with exchange to USD, with common percent fee 10%, common absolute fee 0.003 BTC,
            personal percent fee 5%, personal absolute fee 0.0002 BTC and common persent for exchange 5%. """
        admin.set_fee(mult=100000000, add=3000000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'])
        admin.set_fee(mult=50000000, add=200000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'],
                      owner_id=user1.id, is_active=True)
        admin.set_personal_exchange_fee(in_currency='BTC', out_currency='USD', is_active=False, email=user1.email, fee=0)
        admin.set_rate_exchange(rate=3580654100000, fee=50000000, in_currency='BTC', out_currency='USD')
        user1.payin(payway='btc', amount=0.005, in_curr='BTC', out_curr='USD')
        assert user1.response_payin['account_amount'] == '15.46'
        assert user1.response_payin['in_amount'] == '0.005'
        assert user1.response_payin['out_amount'] == '17'
        assert user1.response_payin['in_fee_amount'] == '0.00045'
        assert user1.response_payin['out_fee_amount'] == '1.54'
        assert user1.response_payin['out_curr'] == 'USD'
        user1.payin_get(user1.payin_lid)
        assert user1.response_payin_get['out_fee_amount'] == '1.54'
        admin.set_fee(payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'], owner_id=user1.id, is_active=False)

    def test_payin_23(self, _personal_user_data):
        """ Payin 200 RUB from ALFA_BANK with exchange to UAH, with common percent fee 3%, common absolute fee 2 RUB,
            personal percent fee 2%, personal absolute fee 1 RUB and common persent for exchange 5%
            with operations bonus 1%. """
        admin.set_fee(mult=30000000, add=2000000000, payway_id=admin.payway['alfa_bank'], currency_id=admin.currency['RUB'])
        admin.set_fee(mult=20000000, add=1000000000, payway_id=admin.payway['alfa_bank'], currency_id=admin.currency['RUB'],
                      owner_id=user1.id, is_active=True)
        admin.set_rate_exchange(rate=2666600000, fee=50000000, in_currency='RUB', out_currency='UAH')
        admin.set_personal_exchange_fee(in_currency='RUB', out_currency='UAH', is_active=False, fee=0, email=user1.email)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=True)
        user1.payin_calc(payway='alfa_bank', amount=200, in_curr='RUB', out_curr='UAH')
        assert user1.response_payin_calc['account_amount'] == '69.64'
        assert user1.response_payin_calc['in_fee_amount'] == '5'
        assert user1.response_payin_calc['out_fee_amount'] == '1.79'
        user1.payin(payway='alfa_bank', amount=200, in_curr='RUB', out_curr='UAH')
        assert user1.response_payin['account_amount'] == '69.64'
        assert user1.response_payin['in_amount'] == '200'
        assert user1.response_payin['out_amount'] == '71.43'
        assert user1.response_payin['in_fee_amount'] == '5'
        assert user1.response_payin['out_fee_amount'] == '1.79'
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=False)
        admin.set_fee(payway_id=admin.payway['alfa_bank'], currency_id=admin.currency['RUB'], owner_id=user1.id, is_active=False)

    @pytest.mark.skip(reason='Disabled exchange. ')
    def test_payin_24(self, _personal_user_data):
        """ Payin 0.005 BTC with exchange to UAH common percent fee 10%, common absolute fee 0.003 BTC,
            personal percent fee 5%, personal absolute fee 0.0002 BTC and common persent for exchange 5%. """
        admin.set_fee(mult=100000000, add=3000000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'])
        admin.set_fee(mult=50000000, add=200000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'],
                      owner_id=user1.id, is_active=True)
        admin.set_rate_exchange(rate=3580654100000, fee=50000000, in_currency='BTC', out_currency='USD')
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=True)
        user1.payin(payway='btc', amount=0.005, in_curr='BTC', out_curr='USD')
        assert user1.response_payin['account_amount'] == '15.63'
        assert user1.response_payin['in_amount'] == '0.005'
        assert user1.response_payin['out_amount'] == '17.18'
        assert user1.response_payin['out_fee_amount'] == '1.55'
        user1.payin_list(in_curr='BTC', out_curr='USD')
        assert user1.response_payin_list['data'][0]['account_amount'] == '15.63'
        assert user1.response_payin_list['data'][0]['in_amount'] == '0.005'
        assert user1.response_payin_list['data'][0]['out_amount'] == '17.18'
        admin.set_fee(payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'], owner_id=user1.id, is_active=False)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=False)


    def test_payin_25(self):
        """ Payin 200 RUB from vtb24 with exchange to UAH common percent fee 3%, common absolute fee 2 RUB, personal percent fee 2%,
            personal absolute fee 1 RUB and common persent for exchange 7% with personal persent for exchange 5%
            with operations bonus 1 USD = 1%. """
        admin.set_user_data(user_email=user1.email, is_customfee=True, user=user1)
        admin.set_fee(mult=30000000, add=2000000000, payway_id=admin.payway['vtb24'], currency_id=admin.currency['RUB'])
        admin.set_fee(mult=20000000, add=1000000000, payway_id=admin.payway['vtb24'], currency_id=admin.currency['RUB'],
                      owner_id=user1.id, is_active=True)
        admin.set_rate_exchange(rate=2666600000, fee=70000000, in_currency='RUB', out_currency='UAH')
        admin.set_personal_exchange_fee(is_active=True, in_currency='RUB', out_currency='UAH', email=user1.email,
                                        fee=50000000)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=True)
        user1.payin_calc(payway='vtb24', amount=200, in_curr='RUB', out_curr='UAH')
        assert user1.response_payin_calc['account_amount'] == '69.64'
        assert user1.response_payin_calc['in_amount'] == '200'
        assert user1.response_payin_calc['out_amount'] == '71.43'
        user1.payin_params(payway='vtb24', in_curr='RUB', out_curr='UAH')
        assert user1.response_payin_params['in_fee'] == {'add': '1', 'max': '0', 'method': 'ceil', 'min': '0', 'mult': '0.02'}
        assert user1.response_payin_params['out_fee'] == {'add': '0.36', 'mult': '0.02'}
        assert user1.response_payin_params['max'] == '3000'
        assert user1.response_payin_params['min'] == '1.04'
        assert user1.response_payin_params['rate'] == ['2.79993', '1']
        user1.payin(payway='vtb24', amount=200, in_curr='RUB', out_curr='UAH')
        assert user1.response_payin['account_amount'] == '69.64'
        assert user1.response_payin['in_amount'] == '200'
        assert user1.response_payin['out_amount'] == '71.43'
        assert user1.response_payin['in_fee_amount'] == '5'
        assert user1.response_payin['out_fee_amount'] == '1.79'
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=False)
        admin.set_personal_exchange_fee(is_active=False, in_currency='RUB', out_currency='UAH', email=user1.email,
                                        fee=50000000)
        admin.set_fee(payway_id=admin.payway['vtb24'], currency_id=admin.currency['RUB'], owner_id=user1.id, is_active=False)
        admin.set_user_data(user_email=user1.email, is_customfee=False, user=user1)

    @pytest.mark.skip(reason='Disabled exchange. ')
    def test_payin_26(self):
        """ Payin 0.005 BTC with exchange to USD with common percent fee 10%, common absolute fee 0.003 USD,
        personal percent fee 5%, personal absolute fee 0.0002 BTC and common persent for exchange 7%
        with personal percent for exchange 5% with operations bonus 1 USD = 1%. """
        admin.set_user_data(user_email=user1.email, is_customfee=True, user=user1)
        admin.set_fee(mult=100000000, add=3000000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'])
        admin.set_fee(mult=50000000, add=200000, payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'], owner_id=user1.id,
                      is_active=True)
        admin.set_rate_exchange(rate=3580654100000, fee=70000000, in_currency='BTC', out_currency='USD')
        admin.set_personal_exchange_fee(is_active=True, in_currency='BTC', out_currency='USD', email=user1.email,
                                        fee=50000000)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=True)
        user1.payin(payway='btc', amount=0.005, in_curr='BTC', out_curr='USD')
        assert user1.response_payin['account_amount'] == '15.46'
        assert user1.response_payin['in_amount'] == '0.005'
        assert user1.response_payin['out_amount'] == '17'
        assert user1.response_payin['in_fee_amount'] == '0.00045'
        assert user1.response_payin['out_fee_amount'] == '1.54'
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=False)
        admin.set_personal_exchange_fee(is_active=False, in_currency='BTC', out_currency='USD', email=user1.email)
        admin.set_fee(payway_id=admin.payway['btc'], currency_id=admin.currency['BTC'], owner_id=user1.id, is_active=False)
        admin.set_user_data(user_email=user1.email, is_customfee=False, user=user1)


class TestPayinLimits:

    def test_0(self, create_session):
        """Warm"""
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_payin_27(self):
        """ Payin 10 UAH with min fee 2.01 UAH and common percent fee 20%. """
        admin.set_fee(mult=200000000, _min=2010000000, payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'])
        user1.payin(payway='visamc', amount=10, in_curr='UAH', out_curr='UAH')
        assert user1.response_payin['account_amount'] == '7.99'

    def test_payin_28(self, _personal_user_data):
        """ Payin 10 UAH with max fee 1.99 UAH and personal percent fee 20%. """
        admin.set_fee(add=2000000000, _max=1990000000, payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'], owner_id=user1.id,
                      is_active=True)
        user1.payin(payway='visamc', amount=10, in_curr='UAH', out_curr='UAH')
        assert user1.response_payin['account_amount'] == '8.01'
        admin.set_fee(payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'], owner_id=user1.id, is_active=False)

    def test_payin_29(self):
        """ Payin 10 UAH with around fee CEIL with common fee 1.15%. """
        admin.set_fee(mult=11500000, payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'])
        user1.payin(payway='visamc', amount=10, in_curr='UAH', out_curr='UAH')
        assert user1.response_payin['account_amount'] == '9.88'

    def test_payin_30(self):
        """ Payin 10 UAH with around fee FLOOR with common fee 1.15%. """
        admin.set_fee(mult=11500000, around='floor', payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'])
        user1.payin(payway='visamc', amount=10, in_curr='UAH', out_curr='UAH')
        assert user1.response_payin['account_amount'] == '9.89'
        assert user1.response_payin['out_fee_amount'] == '0.11'

    def test_payin_31(self):
        """ Payin 10 UAH with around fee ROUND with common fee 1.15%. """
        admin.set_fee(mult=11500000, around='round', payway_id=admin.payway['visamc'], currency_id=admin.currency['UAH'])
        user1.payin(payway='visamc', amount=10, in_curr='UAH', out_curr='UAH')
        assert user1.response_payin['account_amount'] == '9.88'
        assert user1.response_payin['in_fee_amount'] == '0.12'


class TestWrongPayin:

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_payin_32(self):
        """ Payin amount less then tech_min value in currency table. """
        admin.set_pwcurrency_data(currency='UAH', is_active=True, tech_min=1010000000,
                                  tech_max=10000000000000)
        user1.payin(payway='visamc', amount=1, in_curr='UAH', out_curr='UAH')
        assert user1.response_payin['exception'] == 'AmountTooSmall'
        admin.set_pwcurrency_data(currency='UAH', is_active=True, tech_min=10000000,
                                  tech_max=2000000000000)

    def test_payin_33(self):
        """ Payin amount more then tech_max value in currency table. """
        admin.set_pwcurrency_data(currency='UAH', is_active=True, tech_min=1000000000,
                                  tech_max=10000000000)
        user1.payin(payway='visamc', amount=10000.01, in_curr='UAH', out_curr='UAH')
        assert user1.response_payin['exception'] == 'AmountTooBig'
        admin.set_pwcurrency_data(currency='UAH', is_active=True, tech_min=10000000,
                                  tech_max=2000000000000)

    def test_payin_34(self):
        """ Payin not real currency. """
        user1.payin(payway='visamc', amount=100, in_curr='UHA', out_curr='UHA')
        assert user1.response_payin['exception'] == 'InvalidCurrency'

    @pytest.mark.skip  # not deactivated currency
    def test_payin_35(self):
        """ Payin deactivated currency. """
        user1.payin(payway='bch', amount=0.5, in_curr='BCH', out_curr='BCH')
        assert user1.response_payin['exception'] == 'InvalidCurrency'

    def test_payin_36(self):
        """ Payin with not real payway. """
        user1.payin(payway='visam', amount=100, in_curr='UAH', out_curr='UAH')
        assert user1.response_payin['exception'] == 'InvalidPayway'

    def test_payin_37(self):
        """ Payin with deactivate payway. """
        admin.set_pwcurrency_data(currency='UAH', is_active=False, tech_min=1000000000,
                                  tech_max=10000000000000)
        user1.payin(payway='visamc', amount=100, in_curr='UAH', out_curr='UAH')
        assert user1.response_payin['exception'] == 'InactiveCurrency'
        admin.set_pwcurrency_data(currency='UAH', is_active=True, tech_min=10000000,
                                  tech_max=2000000000000)

    @pytest.mark.skip  # not deactivated currency
    def test_payin_38(self):
        """ Payin with exchange to inactive currency. """
        user1.payin(payway='visamc', amount=1000, in_curr='UAH', out_curr='BCH')
        assert user1.response_payin['exception'] == 'InvalidCurrency'

    @pytest.mark.skip  # not deactivated currency
    def test_payin_39(self):
        """ Payin with exchange from inactive currency. """
        user1.payin(payway='visamc', amount=1000, in_curr='BCH', out_curr='UAH')
        assert user1.response_payin['exception'] == 'InvalidCurrency'

    def test_payin_40(self):
        """ Payin with exchange to not real currency. """
        user1.payin(payway='visamc', amount=1000, in_curr='UAH', out_curr='UDS')
        assert user1.response_payin['exception'] == 'InvalidCurrency'

    def test_payin_41(self):
        """ Payin with exchange from not real currency. """
        user1.payin(payway='visamc', amount=1000, in_curr='UDS', out_curr='UAH')
        assert user1.response_payin['exception'] == 'InvalidCurrency'

    def test_payin_42(self):
        """ Payin with exchange by not active pair. """
        user1.payin(payway='payeer', amount=1, in_curr='USD', out_curr='RUB')
        assert user1.response_payin['exception'] == 'UnavailExchange'

    def test_payin_43(self):
        """ Payin with payway, that hasn't currency . """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user1.email)
        user1.payin(payway='qiwi', amount=1, in_curr='UAH', out_curr='UAH', payer='+380661234567', email=user1.email)
        assert user1.response_payin['exception'] == 'InvalidCurrency'

    def test_payin_44(self):
        """ Payin without external id. """
        response = requests.post(url=user1.wapi_url,
                                 json={'method': 'create', 'model': 'payin',
                                       'data': {'payway': 'payeer', 'amount': 1,
                                                'in_curr': 'USD', 'out_curr': 'USD'}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'InvalidParam'

    def test_payin_45(self):
        """ Payin without in_curr parameter. """
        response = requests.post(url=user1.wapi_url,
                                 json={'method': 'create', 'model': 'payin',
                                       'data': {'externalid': user1.ex_id(), 'payway': 'payeer', 'amount': 1,
                                                'out_curr': 'USD'}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'InvalidCurrency'

    def test_payin_46(self):
        """ Payin without out_curr parameter. """
        response = requests.post(url=user1.wapi_url,
                                 json={'method': 'create', 'model': 'payin',
                                       'data': {'externalid': user1.ex_id(), 'payway': 'payeer', 'amount': 1,
                                                'in_curr': 'USD'}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['out_amount'] == '1'

    def test_payin_47(self):
        """ Payin without amount parameter. """
        response = requests.post(url=user1.wapi_url,
                                 json={'method': 'create', 'model': 'payin',
                                       'data': {'externalid': user1.ex_id(), 'payway': 'payeer',
                                                'in_curr': 'USD', 'out_curr': 'USD'}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'InvalidParam'

    def test_payin_48(self):
        """ Payin without user token. """
        response = requests.post(url=user1.wapi_url,
                                 json={'method': 'create', 'model': 'payin',
                                       'data': {'externalid': user1.ex_id(), 'payway': 'payeer', 'amount': 1,
                                                'in_curr': 'USD', 'out_curr': 'USD'}},
                                 verify=False)
        assert json.loads(response.text)['exception'] == 'Unauth'

    def test_payin_49(self):
        """ Payin with wrong user token. """
        response = requests.post(url=user1.wapi_url,
                                 json={'method': 'create', 'model': 'payin',
                                       'data': {'externalid': user1.ex_id(), 'payway': 'payeer', 'amount': 1,
                                                'in_curr': 'USD', 'out_curr': 'USD'}},
                                 headers={'x-token': '1'}, verify=False)
        assert json.loads(response.text)['exception'] == 'Unauth'

    def test_payin_50(self):
        """ Payin with excess parameter in data field. """
        requests.post(url=user1.wapi_url,
                      json={'method': 'create', 'model': 'payin',
                            'data': {'externalid': user1.ex_id(), 'payway': 'payeer', 'amount': 1,
                                                'in_curr': 'USD', 'out_curr': 'USD', 'par': 123}},
                      headers=user1.headers, verify=False)

    def test_payin_51(self):
        """ Payin with excess parameter in common filed. """
        requests.post(url=user1.wapi_url,
                      json={'method': 'create', 'model': 'payin',
                            'data': {'externalid': user1.ex_id(), 'payway': 'payeer', 'amount': 1,
                                                'in_curr': 'USD', 'out_curr': 'USD'}},
                      headers=user1.headers, verify=False)
