import pytest
import requests
import json
import time
from tests import tools
from user.user import User


class TestTransfer:

    def test_0(self, create_session):
        """ Warm """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_transfer_1(self):
        """ Transfer 0.01 UAH to other user without exchange. """
        admin.set_wallet_amount(balance=100000000000, currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='UAH', email=user2.email)
        admin.set_fee(currency_id=admin.currency['UAH'], tp=30)
        user1.transfer_params(in_curr='UAH', out_curr='UAH')
        assert user1.resp_transfer_params['min'] == '0.01'
        assert user1.resp_transfer_params['max'] == '3000'
        assert user1.resp_transfer_params['min_balance_limit'] == '0.01'
        user1.transfer(amount=0.01, in_curr='UAH', out_curr='UAH', email_user=user2.email)
        assert user1.resp_transfer['account_amount'] == '0.01'
        assert user1.resp_transfer['in_amount'] == '0.01'
        assert user1.resp_transfer['out_amount'] == '0.01'
        assert user1.resp_transfer['tp'] == 'transfer'
        user1.get_balances(curr='UAH')
        assert user1.resp_balances['UAH'] == '99.99'
        user2.get_balances('UAH')
        assert user2.resp_balances['UAH'] == '0.01'
        user1.transfer_last_used(count=1)
        assert user1.resp_last_used == ['AC179']
        user1.transfer(amount=0.01, in_curr='UAH', out_curr='UAH', email_user=user3.email)
        user1.transfer_last_used(count=2)
        assert user1.resp_last_used == ['AC4408', 'AC179']

    def test_transfer_2(self, _disable_2type):
        """ Transfer 3000 USD to other user without fee with two_step auth. """
        admin.set_wallet_amount(balance=2999990000000, currency='USD', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user2.email)
        admin.set_fee(currency_id=admin.currency['USD'], tp=30)
        user1.set_2type(tp='0')
        user1.transfer_calc(amount=3000, in_curr='USD', out_curr='USD')
        assert user1.resp_transfer_calc['account_amount'] == '3000'
        assert user1.resp_transfer_calc['out_fee_amount'] == '0'
        user1.transfer_calc(amount=3000.01, in_curr='USD', out_curr='USD')
        assert user1.resp_transfer_calc['exception'] == 'AmountTooBig'
        admin.set_wallet_amount(balance=3000000000000, currency='USD', email=user1.email)
        user1.transfer(amount=3000, in_curr='USD', out_curr='USD', email_user=user2.email)
        assert user1.resp_transfer['exception'] == 'Auth2Required'
        user1.confirm_registration(key=user1.transfer_2type, code=admin.get_onetime_code(user1.email), user=user1)
        assert user1.resp_confirm['account_amount'] == '3000'
        user1.get_balances(curr='USD')
        assert user1.resp_balances['USD'] == '0'
        user2.get_balances('USD')
        assert user2.resp_balances['USD'] == '3000'
        user1.transfer_list(in_curr='USD', out_curr='USD', first=None, count=None)
        assert user1.resp_transfer_list['data'][0]['lid'] == user1.resp_confirm['lid']

    def test_transfer_3(self):
        """ Transfer 0.00999 BTC to another user without exchange. """
        admin.set_wallet_amount(balance=100000000, currency='BTC', email=user1.email)
        admin.set_fee(currency_id=admin.currency['BTC'], tp=30)
        user1.transfer_calc(amount='0.100001', in_curr='BTC', out_curr='BTC')
        assert user1.resp_transfer_calc['account_amount'] == '0.100001'
        assert user1.resp_transfer_calc['out_fee_amount'] == '0'
        user1.transfer_params(in_curr='BTC', out_curr='BTC')
        assert user1.resp_transfer_params['min'] == '0.000001'
        assert user1.resp_transfer_params['max'] == '3'
        assert user1.resp_transfer_params['min_balance_limit'] == '0.000001'
        user1.transfer(amount=0.00999, in_curr='BTC', out_curr='BTC', email_user=user2.email)
        assert user1.resp_transfer['in_amount'] == '0.00999'
        assert user1.resp_transfer['out_amount'] == '0.00999'

    def test_transfer_4(self):
        """ Transfer 5 USD with common percent fee 1% and with common absolute fee for transfer 0.5 USD. """
        admin.set_wallet_amount(balance=6000000000, currency='USD', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user2.email)
        admin.set_fee(mult=20000000, add=1000000000, currency_id=admin.currency['USD'], tp=35)
        admin.set_fee(mult=10000000, add=500000000, currency_id=admin.currency['USD'], tp=30)
        user1.transfer_params(in_curr='USD', out_curr='USD')
        assert user1.resp_transfer_params['out_fee'] == {'add': '0.5', 'max': '0', 'method': 'ceil', 'min': '0',
                                                         'mult': '0.01'}
        assert user1.resp_transfer_params['min_balance_limit'] == '0.52'
        assert user1.resp_transfer_params['min'] == '0.01'
        assert user1.resp_transfer_params['max'] == '3000'
        user1.transfer_calc(amount=5.45, in_curr='USD', out_curr='USD')
        assert user1.resp_transfer_calc['account_amount'] == '6.01'
        assert user1.resp_transfer_calc['out_fee_amount'] == '0.56'
        user1.transfer(amount=5, in_curr='USD', out_curr='USD', email_user=user2.email)
        assert user1.resp_transfer['account_amount'] == '5.55'
        assert user1.resp_transfer['in_amount'] == '5'
        assert user1.resp_transfer['out_amount'] == '5'
        assert user1.resp_transfer['out_fee_amount'] == '0.55'
        user1.get_balances(curr='USD')
        assert user1.resp_balances['USD'] == '0.45'
        user2.get_balances('USD')
        assert user2.resp_balances['USD'] == '5'

    def test_transfer_5(self, _personal_user_data):
        """ Transfer 0.003 BTC with personal absolute fee for transfer 0.0001 BTC. """
        admin.set_wallet_amount(balance=10000000, currency='BTC', email=user1.email)
        admin.set_fee(add=1000000, currency_id=admin.currency['BTC'], tp=30)
        admin.set_fee(add=100000, currency_id=admin.currency['BTC'], tp=30, owner_id=user1.id, is_active=True)
        user1.transfer_params(in_curr='BTC', out_curr='BTC')
        assert user1.resp_transfer_params['out_fee'] == {'add': '0.0001', 'max': '0', 'method': 'ceil', 'min': '0',
                                                         'mult': '0'}
        assert user1.resp_transfer_params['min_balance_limit'] == '0.000101'
        user1.transfer(amount=0.003, in_curr='BTC', out_curr='BTC', email_user=user2.email)
        assert user1.resp_transfer['account_amount'] == '0.0031'
        user1.transfer_get(lid=user1.transfer_lid)
        assert user1.resp_transfer_get['account_amount'] == '0.0031'
        assert user1.resp_transfer_get['status'] == 'done'
        user2.transfer_get(lid=user1.transfer_lid)
        assert user2.resp_transfer_get['exception'] == 'NotFound'
        user3.transfer_get(lid=user1.transfer_lid)
        assert user3.resp_transfer_get['exception'] == 'NotFound'
        admin.set_fee(currency_id=admin.currency['BTC'], tp=30, owner_id=user1.id, is_active=False)

    def test_transfer_6(self):
        """ Transfer 1 USD with exchange from UAH, without exchange fee, with percent fee for transfer 10%,
            with absolute fee for transfer 0.1 USD. """
        admin.set_wallet_amount(balance=33830000000, currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user2.email)
        admin.set_fee(mult=100000000, add=100000000, currency_id=admin.currency['USD'], tp=30)
        admin.set_rate_exchange(rate=28199900000, fee=0, in_currency='UAH', out_currency='USD')
        user1.transfer_params(in_curr='UAH', out_curr='USD')
        assert user1.resp_transfer_params['rate'] == ['28.1999', '1']
        assert user1.resp_transfer_params['from_in_curr_balance'] == '1.19'
        assert user1.resp_transfer_params['min_balance_limit'] == '0.12'
        user1.transfer_calc(amount=1, in_curr='UAH', out_curr='USD')
        assert user1.resp_transfer_calc['account_amount'] == '33.84'
        assert user1.resp_transfer_calc['in_fee_amount'] == '5.64'
        admin.set_wallet_amount(balance=40000000000, currency='UAH', email=user1.email)
        user1.transfer(amount=1, in_curr='UAH', out_curr='USD', email_user='AC179')
        assert user1.resp_transfer['account_amount'] == '33.84'
        assert user1.resp_transfer['in_amount'] == '28.2'
        assert user1.resp_transfer['out_amount'] == '1'
        assert user1.resp_transfer['in_curr'] == 'UAH'
        assert user1.resp_transfer['out_curr'] == 'USD'
        '''
        user1.get_balances(curr='UAH')
        assert user1.resp_balances['UAH'] == '6.16'
        user2.get_balances('USD')
        assert user2.resp_balances['USD'] == '1'
        '''

    @pytest.mark.skip(reason='Disable exchange')
    def test_transfer_7(self):
        """ Transfer 10 USD with exchange from BTC without exchange fee, with percent fee for transfer 50%,
            with absolute fee for transfer 5 USD. """
        admin.set_wallet_amount(balance=10000000, currency='BTC', email=user1.email)
        admin.set_fee(mult=500000000, add=5000000000, currency_id=admin.currency['USD'], tp=30)
        admin.set_rate_exchange(rate=3580654100000, fee=0, in_currency='BTC', out_currency='USD')
        user1.transfer_params(in_curr='BTC', out_curr='USD')
        assert user1.resp_transfer_params['min'] == '0.01'
        assert user1.resp_transfer_params['rate'] == ['1', '3580.6541']
        assert user1.resp_transfer_params['from_in_curr_balance'] == '35.8'
        assert user1.resp_transfer_params['min_balance_limit'] == '5.02'
        user1.transfer(amount=10, in_curr='BTC', out_curr='USD', email_user=user2.email)
        assert user1.resp_transfer['account_amount'] == '0.00558558'
        assert user1.resp_transfer['in_amount'] == '0.00279279'
        assert user1.resp_transfer['out_amount'] == '10'
        assert user1.resp_transfer['in_fee_amount'] == '0.00279279'
        assert user1.resp_transfer['out_fee_amount'] == '10'


    def test_transfer_8(self):
        """ Transfer 1 USD with convertation from UAH with common fee 2.5% and operation bonus 1.5%. """
        admin.set_wallet_amount(balance=28480000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=25000000, in_currency='UAH', out_currency='USD')
        admin.set_fee(currency_id=admin.currency['USD'], tp=30)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=15000000, is_active=True)
        user1.transfer_calc(amount=1, in_curr='UAH', out_curr='USD')
        assert user1.resp_transfer_calc['account_amount'] == '28.49'
        assert user1.resp_transfer_calc['out_amount'] == '1'
        admin.set_wallet_amount(balance=100000000000, currency='UAH', email=user1.email)
        user1.transfer(amount=1, in_curr='UAH', out_curr='USD', email_user=user2.email)
        assert user1.resp_transfer['in_amount'] == '28.49'
        assert user1.resp_transfer['out_amount'] == '1'
        assert user1.resp_transfer['account_amount'] == '28.49'
        user1.transfer_get(lid=user1.transfer_lid)
        assert user1.resp_transfer_get['account_amount'] == '28.49'
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=15000000, is_active=False)

    @pytest.mark.skip(reason='Deactivated exchange for this pair. ')
    def test_transfer_9(self):
        """ Transfer 0.43 BTC with convertation from USD with common fee 0.55% and
            personal collected bonus 5 points = 0.5%. """
        admin.set_wallet_amount(balance=2000000000000, currency='USD', email=user1.email)
        admin.set_user_data(user_email=user1.email, bonus_points=5000000000, is_customfee=False, user=user1)
        admin.set_fee(currency_id=admin.currency['UAH'], tp=30)
        admin.set_rate_exchange(rate=3580654100000, fee=5500000, in_currency='USD', out_currency='BTC')
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=5000000, is_active=True)
        user1.transfer(amount=0.43, in_curr='USD', out_curr='BTC', email_user=user2.email)
        assert user1.resp_transfer['in_amount'] == '1540.46'
        assert user1.resp_transfer['out_amount'] == '0.43'
        assert user1.resp_transfer['out_curr'] == 'BTC'
        user1.transfer_get(lid=user1.transfer_lid)
        assert user1.resp_transfer_get['in_curr'] == 'USD'
        assert user1.resp_transfer_get['out_curr'] == 'BTC'
        admin.set_bonus_level(name='5points', points=5000000000, amount=None, surplus=5000000, is_active=False)


    def test_transfer_10(self, _personal_user_data):
        """ Transfer 3.5 USD with convertation from UAH with common fee 5% with personal fee 3.5%. """
        admin.set_wallet_amount(balance=200000000000, currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user2.email)
        admin.set_fee(currency_id=admin.currency['USD'], tp=30)
        admin.set_rate_exchange(rate=28199900000, fee=50000000, in_currency='UAH', out_currency='USD')
        admin.set_personal_exchange_fee(is_active=True, in_currency='UAH', out_currency='USD', email=user1.email,
                                        fee=35000000)
        user1.transfer(amount=3.5, in_curr='UAH', out_curr='USD', email_user=user2.email)
        assert user1.resp_transfer['in_amount'] == '102.16'
        assert user1.resp_transfer['out_amount'] == '3.5'
        user1.transfer_list(in_curr='UAH', out_curr='USD', first=0, count=100)
        assert user1.resp_transfer_list['data'][0]['account_amount'] == '102.16'
        assert user1.resp_transfer_list['data'][0]['out_amount'] == '3.5'
        admin.set_personal_exchange_fee(is_active=False, in_currency='UAH', out_currency='USD', email=user1.email)

    @pytest.mark.skip(reason='Deactivated exchange for this pair. ')
    def test_transfer_11(self, _personal_user_data):
        """ Transfer 0.43 BTC with convertation from USD with common exchange fee 1%
            with personal exchange fee 0.05%. """
        admin.set_wallet_amount(balance=3000000000000, currency='USD', email=user1.email)
        admin.set_fee(currency_id=admin.currency['USD'], tp=30)
        admin.set_rate_exchange(rate=3580654100000, fee=10000000, in_currency='USD', out_currency='BTC')
        admin.set_personal_exchange_fee(is_active=True, in_currency='USD', out_currency='BTC', email=user2.email,
                                        fee=500000)
        user1.transfer(amount=0.43, in_curr='USD', out_curr='BTC', email_user=user2.email)
        assert user1.response_transfer['in_amount'] == '1540.46'
        assert user1.response_transfer['out_amount'] == '0.43'
        admin.set_personal_exchange_fee(is_active=False, in_currency='USD', out_currency='BTC', email=user1.email)


    def test_transfer_12(self, _personal_user_data):
        """ Transfer 10.05 USD with convertation from UAH with common exchange fee 5% with personal exchange fee 3.5%
            with operations bonus 1%, with common fee for transfer 10% and 1 USD, with personal fee for transfer
            5% and 0.5 USD. """
        admin.set_wallet_amount(balance=322800000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=50000000, in_currency='UAH', out_currency='USD')
        admin.set_fee(mult=100000000, add=1000000000, currency_id=admin.currency['USD'], tp=30)
        admin.set_fee(mult=50000000, add=500000000, currency_id=admin.currency['USD'], tp=30, owner_id=user1.id,
                      is_active=True)
        admin.set_personal_exchange_fee(is_active=True, in_currency='UAH', out_currency='USD', email=user1.email,
                                        fee=35000000)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=True)
        user1.transfer_params(in_curr='UAH', out_curr='USD')
        assert user1.resp_transfer_params['out_fee'] == {'add': '0.5', 'max': '0', 'min': '0', 'method': 'ceil',
                                                         'mult': '0.05'}
        assert user1.resp_transfer_params['min_balance_limit'] == '0.52'
        user1.transfer_calc(amount=10.05, in_curr='UAH', out_curr='USD')
        assert user1.resp_transfer_calc['account_amount'] == '322.81'
        assert user1.resp_transfer_calc['in_amount'] == '293.33'
        assert user1.resp_transfer_calc['out_amount'] == '10.05'
        admin.set_wallet_amount(balance=400000000000, currency='UAH', email=user1.email)
        user1.transfer(amount=10.05, in_curr='UAH', out_curr='USD', email_user='AC179')
        assert user1.resp_transfer['account_amount'] == '322.81'
        assert user1.resp_transfer['in_amount'] == '293.33'
        assert user1.resp_transfer['out_amount'] == '10.05'
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=False)
        admin.set_fee(currency_id=admin.currency['USD'], tp=30, owner_id=user1.id, is_active=False)
        user1.transfer_list(in_curr='UAH', out_curr='USD', first=0, count=100)
        assert user1.resp_transfer_list['data'][0]['lid'] == user1.transfer_lid

    @pytest.mark.skip(reason='Deactivated exchange for this pair. ')
    def test_transfer_13(self, _personal_user_data):
        """ Transfer 0.43 BTC with convertation from USD with common fee 1% with personal exchange fee 0.05%
            with operations bonus 1 USD = 1%, with common fee for transfer 10% and 0.01 BTC,
            with personal fee for transfer 5% and 0.005 BTC. """
        admin.set_wallet_amount(balance=2000000000000, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=3580654100000, fee=10000000, in_currency='USD', out_currency='BTC')
        admin.set_fee(mult=100000000, add=10000000, currency_id='BTC', tp=30)
        admin.set_fee(mult=50000000, add=5000000, currency_id='BTC', tp=30, owner_id=user1.id)
        admin.set_personal_exchange_fee(is_active=True, in_currency='USD', out_currency='BTC', email=user1.email,
                                        fee=500000)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=True)
        user1.transfer_calc(amount=0.43, in_curr='USD', out_curr='BTC')
        assert user1.resp_transfer_calc['account_amount'] == '1635.41'
        assert user1.resp_transfer_calc['in_amount'] == '1540.46'
        assert user1.resp_transfer_calc['out_amount'] == '0.43'
        assert user1.resp_transfer_calc['in_fee_amount'] == '94.95'
        assert user1.resp_transfer_calc['out_fee_amount'] == '0.0265'
        user1.transfer(amount=0.43, in_curr='USD', out_curr='BTC', email_user=user2.email)
        assert user1.resp_transfer['account_amount'] == '1635.41'
        assert user1.resp_transfer['in_amount'] == '1540.46'
        assert user1.resp_transfer['out_amount'] == '0.43'
        admin.set_personal_exchange_fee(is_active=False, in_currency='USD', out_currency='BTC', email=user1.email)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=False)
        admin.set_fee(currency_id='BTC', tp=30, owner_id=user1.id, is_active=False)

    def test_transfer_14(self):
        """ List transfers for owner without exchange. """
        user1.transfer_list(in_curr='UAH', out_curr='UAH', first=None, count=100)
        exchange_ls = [(ls['in_curr'], ls['out_curr']) for ls in user1.resp_transfer_list['data']]
        tp_ls = [ls['tp'] for ls in user1.resp_transfer_list['data']]
        assert not ('UAH', 'UAH') not in exchange_ls and ('UAH', 'UAH') in exchange_ls
        assert not 'transfer' not in tp_ls and 'transfer' in tp_ls

    def test_transfer_15(self):
        """ List transfers for owner with in_curr. """
        user1.transfer_list(in_curr='UAH', out_curr=None, first=None, count=100)
        in_curr_ls = [ls['in_curr'] for ls in user1.resp_transfer_list['data']]
        assert not 'UAH' not in in_curr_ls

    def test_transfer_16(self):
        """ List transfers for owner with out_curr. """
        user1.transfer_list(in_curr=None, out_curr='USD', first=None, count=100)
        out_curr_ls = [ls['out_curr'] for ls in user1.resp_transfer_list['data']]
        assert not 'USD' not in out_curr_ls

    def test_transfer_17(self):
        """ List transfers for owner with in_curr and out_curr with 5 elements in lists. """
        user1.transfer_list(in_curr='UAH', out_curr='USD', first=None, count=5)
        exchange_ls = [(ls['in_curr'], ls['out_curr']) for ls in user1.resp_transfer_list['data']]
        assert not ('UAH', 'USD') not in exchange_ls
        assert len(user1.resp_transfer_list['data']) == 5

    def test_transfer_18(self):
        """ Transfer to not real user """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user1.email)
        user1.transfer(amount=1, in_curr='UAH', out_curr='UAH', email_user='anycashuser100@mailinator.com')
        assert user1.resp_transfer['exception'] == 'InvalidContact'
        user1.transfer(amount=1, in_curr='UAH', out_curr='UAH', email_user='AC0000')
        assert user1.resp_transfer['exception'] == 'InvalidContact'

    def test_transfer_19(self):
        """ Transfer without amount """
        user1.transfer(in_curr='UAH', out_curr='UAH', email_user=user2.email)
        assert user1.resp_transfer['exception'] == 'InvalidParam'

    def test_transfer_20(self):
        """ Transfer with not real currency. """
        user1.transfer(amount=1, in_curr='UDS', out_curr='UDS', email_user=user2.email)
        assert user1.resp_transfer['exception'] == 'InvalidCurrency'

    def test_transfer_21(self):
        """ Transfer amount less then tech_min in currency table """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user1.email)
        admin.set_currency_data(is_crypto=False, admin_min=20000000, admin_max=2000000000000)
        user1.transfer(amount=0.01, in_curr='UAH', out_curr='UAH', email_user=user2.email)
        assert user1.resp_transfer['exception'] == 'AmountTooSmall'
        admin.set_currency_data(is_crypto=False, admin_min=10000000, admin_max=3000000000000)

    def test_transfer_22(self):
        """ Transfer amount more then tech_max in currency table. """
        admin.set_wallet_amount(balance=2000000000, currency='UAH', email=user1.email)
        admin.set_currency_data(is_crypto=False, admin_min=20000000, admin_max=1000000000)
        user1.transfer(amount=1.01, in_curr='UAH', out_curr='UAH', email_user=user2.email)
        assert user1.resp_transfer['exception'] == 'AmountTooBig'
        admin.set_currency_data(is_crypto=False, admin_min=10000000, admin_max=3000000000000)

    @pytest.mark.skip(reason='Not deactivated currency')
    def test_transfer_23(self):
        """ Transfer by deactivated currency. """
        admin.set_wallet_amount(balance=1000000000, currency='BCHABC', email=user1.email)
        user1.transfer(amount=1, in_curr='BCHABC', out_curr='BCHABC', email_user=user2.email)
        assert user1.resp_transfer['exception'] == 'InvalidCurrency'

    def test_transfer_24(self):
        """ Transfer in to not real currency. """
        admin.set_wallet_amount(balance=100000000000, currency='UAH', email=user1.email)
        user1.transfer(amount=1, in_curr='UAH', out_curr='UDS', email_user=user2.email)
        assert user1.resp_transfer['exception'] == 'InvalidCurrency'

    def test_transfer_25(self):
        """ Transfer from not real currency. """
        admin.set_wallet_amount(balance=1000000000, currency='USD', email=user1.email)
        user1.transfer(amount=1, in_curr='UDS', out_curr='BTC', email_user=user2.email)
        assert user1.resp_transfer['exception'] == 'InvalidCurrency'

    @pytest.mark.skip(reason='Not deactivated currency')
    def test_transfer_26(self):
        """ Transfer from deactivated currency. """
        admin.set_wallet_amount(balance=1000000000, currency='BCHABC', email=user1.email)
        user1.transfer(amount=1, in_curr='BCHABC', out_curr='BTC', email_user=user2.email)
        assert user1.resp_transfer['exception'] == 'UnavailExchange'

    @pytest.mark.skip(reason='Not deactivated currency')
    def test_transfer_27(self):
        """ Transfer to deactivate currency. """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user1.email)
        user1.transfer(amount=1, in_curr='UAH', out_curr='BCHABC', email_user=user2.email)
        assert user1.resp_transfer['exception'] == 'UnavailExchange'

    def test_transfer_28(self):
        """ Transfer by deactivate currency pair. """
        admin.set_wallet_amount(balance=100000000000, currency='USD', email=user1.email)
        user1.transfer(amount=5, in_curr='USD', out_curr='RUB', email_user=user2.email)
        assert user1.resp_transfer['exception'] == 'UnavailExchange'

    def test_transfer_29(self):
        """ Transfer with convertation, amount more than user has on balance in in_amount currency. """
        admin.set_wallet_amount(balance=28480000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=10000000, in_currency='UAH', out_currency='USD')
        user1.transfer(amount=1, in_curr='UAH', out_curr='USD', email_user=user2.email)
        assert user1.resp_transfer['exception'] == 'InsufficientFunds'

    def test_transfer_30(self):
        """ Transfer with exchange, in_amount less then tech_min exchange table. """
        admin.set_wallet_amount(balance=2000000000, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=0, in_currency='USD', out_currency='UAH')
        admin.set_tech_limits_exchange(tech_min=1010000000, tech_max=2000000000000, in_currency='USD', out_currency='UAH')
        user1.transfer(amount=28.19, in_curr='USD', out_curr='UAH', email_user=user2.email)
        assert user1.resp_transfer['exception'] == 'AmountTooSmall'
        admin.set_tech_limits_exchange(tech_min=10000000, tech_max=3000000000000, in_currency='USD', out_currency='UAH')

    def test_transfer_31(self):
        """ Transfer with convertation: in_amount more then tech_max exchange table. """
        admin.set_wallet_amount(balance=10000000000, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=0, in_currency='USD', out_currency='UAH')
        admin.set_tech_limits_exchange(tech_min=990000000, tech_max=5000000000, in_currency='USD', out_currency='UAH')
        user1.transfer(amount=141, in_curr='USD', out_curr='UAH', email_user=user2.email)
        assert user1.resp_transfer['exception'] == 'AmountTooBig'
        admin.set_tech_limits_exchange(tech_min=10000000, tech_max=3000000000000, in_currency='USD', out_currency='UAH')

    def test_transfer_32(self):
        """ Transfer without in_curr parameter. """
        admin.set_wallet_amount(balance=300000000000, currency='UAH', email=user1.email)
        user1.transfer(amount=10, in_curr=None, out_curr='UAH', email_user=user2.email)
        assert user1.resp_transfer['in_curr'] == 'UAH'
        assert user1.resp_transfer['out_curr'] == 'UAH'

    def test_transfer_33(self):
        """ Transfer without out_curr parameter. """
        admin.set_wallet_amount(balance=30000000000, currency='UAH', email=user1.email)
        user1.transfer(amount=10, in_curr='UAH', out_curr=None, email_user=user2.email)
        assert user1.resp_transfer['exception'] == 'InvalidCurrency'

    def test_transfer_34(self):
        """ Transfer without external_id. """
        admin.set_wallet_amount(balance=30000000000, currency='UAH', email=user1.email)
        response = requests.post(url=user1.wapi_url,
                                 json={'method': 'create', 'model': 'transfer',
                                       'data': {'amount': 25, 'in_curr': 'UAH', 'out_curr': 'UAH', 'tgt': user1.email}},
                                 headers=user1.headers, verify=False)
        assert json.loads(response.text)['exception'] == 'InvalidParam'


class TestTransferProtect:

    def test_0(self, create_session):
        """ Warm """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session


    def test_transferprotect_1(self, _disable_2type):
        """ Transfer with protect 0.01 USD with 2 type autentication. """
        admin.set_wallet_amount(balance=1000000000, currency='USD', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user2.email)
        admin.set_fee(currency_id=admin.currency['USD'], tp=33)
        user1.set_2type(tp='0')
        user1.transferprotect_params(out_curr='USD')
        assert user1.resp_transferprotect_params['in_curr'] == 'USD'
        assert user1.resp_transferprotect_params['out_curr'] == 'USD'
        assert user1.resp_transferprotect_params['min'] == '0.01'
        assert user1.resp_transferprotect_params['max'] == '3000'
        assert user1.resp_transferprotect_params['out_fee'] == {'add': '0', 'max': '0', 'method': 'ceil', 'min': '0',
                                                                'mult': '0'}
        user1.transferprotect_calc(amount=1.01, out_curr='USD')
        assert user1.resp_transferprotect_calc['account_amount'] == '1.01'
        assert user1.resp_transferprotect_calc['in_fee_amount'] == '0'
        user1.transferprotect_create(amount=0.01, out_curr='USD', tgt=user2.email)
        assert user1.resp_transferprotect_create['exception'] == 'Auth2Required'
        user1.confirm_registration(key=user1.transferprotect_2type, code=admin.get_onetime_code(user1.email), user=user1)
        user1.get_balances(curr='USD')
        assert user1.resp_balances['USD'] == '0.99'
        user2.get_balances(curr='USD')
        assert user2.resp_balances['USD'] == '0'
        user1.transferprotect_confirm(lid=user1.resp_confirm['lid'])
        user1.confirm_registration(key=user1.transferprotect_2type, code=admin.get_onetime_code(user1.email),
                                   user=user1)
        assert user1.resp_confirm['status'] == 'done'
        user1.get_balances(curr='USD')
        assert user1.resp_balances['USD'] == '0.99'
        user2.get_balances(curr='USD')
        assert user2.resp_balances['USD'] == '0.01'


    def test_transferprotect_2(self):
        """ Transfer with protect 0.003 BTC with refusing by payee.  """
        admin.set_wallet_amount(balance=3000000, currency='BTC', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='BTC', email=user2.email)
        admin.set_fee(currency_id=admin.currency['BTC'], tp=33)
        user1.transferprotect_calc(amount=0.003, out_curr='BTC')
        assert user1.resp_transferprotect_calc['account_amount'] == '0.003'
        assert user1.resp_transferprotect_calc['in_amount'] == '0.003'
        user1.transferprotect_params(out_curr='BTC')
        assert user1.resp_transferprotect_params['min'] == '0.000001'
        assert user1.resp_transferprotect_params['max'] == '3'
        assert user1.resp_transferprotect_params['min_balance_limit'] == '0.000001'
        user1.transferprotect_create(amount=0.003, out_curr='BTC', tgt='AC179')
        assert user1.resp_transferprotect_create['account_amount'] == '0.003'
        assert user1.resp_transferprotect_create['in_curr'] == 'BTC'
        assert user1.resp_transferprotect_create['out_curr'] == 'BTC'
        user1.transferprotect_refuse(lid=user1.transferprotect_lid)
        assert user1.resp_transferprotect_refuse['exception'] == 'NotFound'
        user1.get_balances(curr='BTC')
        assert user1.resp_balances['BTC'] == '0'
        user3.transferprotect_refuse(lid=user1.transferprotect_lid)
        assert user3.resp_transferprotect_refuse['exception'] == 'NotFound'
        user2.transferprotect_refuse(lid=user1.transferprotect_lid)
        assert user2.resp_transferprotect_refuse['amount'] == '0.003'
        assert user2.resp_transferprotect_refuse['status'] == 'reject'
        assert user2.resp_transferprotect_refuse['curr'] == 'BTC'
        assert user2.resp_transferprotect_refuse['lid'] == user1.transferprotect_lid
        user1.get_balances(curr='BTC')
        assert user1.resp_balances['BTC'] == '0.003'
        user2.get_balances(curr='BTC')
        assert user2.resp_balances['BTC'] == '0'
        user1.transferprotect_confirm(lid=user1.transferprotect_lid)
        assert user1.resp_transferprotect_confirm['exception'] == 'Unavailable'
        user1.get_balances(curr='BTC')
        assert user1.resp_balances['BTC'] == '0.003'
        user2.get_balances(curr='BTC')
        assert user2.resp_balances['BTC'] == '0'
        user2.transferprotect_refuse(lid=user1.transferprotect_lid)
        assert user2.resp_transferprotect_refuse['exception'] == 'NotFound'
        user1.transferprotect_get(lid=user1.transferprotect_lid)
        assert user1.resp_transferprotect_get['status'] == 'reject'
        user2.transferprotect_get(lid=user1.transferprotect_lid)
        assert user2.resp_transferprotect_get['status'] == 'reject'
        user3.transferprotect_get(lid=user1.transferprotect_lid)
        assert user3.resp_transferprotect_get['exception'] == 'NotFound'


    def test_transferprotect_3(self):
        """ Transfer with protect 3.33 UAH with common persent fee 4.5% and common absolute fee 0.8 UAH. """
        admin.set_wallet_amount(balance=4270000000, currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='UAH', email=user2.email)
        admin.set_fee(currency_id=admin.currency['UAH'], tp=30, add=1000000000)
        admin.set_fee(currency_id=admin.currency['UAH'], tp=33, add=800000000, mult=45000000)
        user1.transferprotect_params(out_curr='UAH')
        assert user1.resp_transferprotect_params['min_balance_limit'] == '0.82'
        user1.transferprotect_calc(amount=3.33, out_curr='UAH')
        assert user1.resp_transferprotect_calc['account_amount'] == '4.28'
        admin.set_wallet_amount(balance=5000000000, currency='UAH', email=user1.email)
        user1.transferprotect_calc(amount=3.33, out_curr='UAH')
        assert user1.resp_transferprotect_calc['account_amount'] == '4.28'
        assert user1.resp_transferprotect_calc['out_amount'] == '3.33'
        assert user1.resp_transferprotect_calc['out_fee_amount'] == '0.95'
        user1.transferprotect_create(amount=3.33, out_curr='UAH', tgt='AC179')
        assert user1.resp_transferprotect_create['account_amount'] == '4.28'
        assert user1.resp_transferprotect_create['out_amount'] == '3.33'
        assert user1.resp_transferprotect_create['out_fee_amount'] == '0.95'
        user1.transferprotect_get(lid=user1.transferprotect_lid)
        assert user1.resp_transferprotect_get['status'] == 'wait'
        user2.transferprotect_get(lid=user1.transferprotect_lid)
        assert user2.resp_transferprotect_get['status'] == 'wait'
        user1.transferprotect_confirm(lid=user1.transferprotect_lid)
        assert user1.resp_transferprotect_confirm['in_curr'] == 'UAH'
        assert user1.resp_transferprotect_confirm['out_curr'] == 'UAH'
        user1.get_balances(curr='UAH')
        assert user1.resp_balances['UAH'] == '0.72'
        user2.get_balances(curr='UAH')
        assert user2.resp_balances['UAH'] == '3.33'
        user1.transferprotect_get(lid=user1.transferprotect_lid)
        assert user1.resp_transferprotect_get['status'] == 'done'
        user2.transferprotect_get(lid=user1.transferprotect_lid)
        assert user2.resp_transferprotect_get['status'] == 'done'


    def test_transferprotect_4(self):
        """ Transfer with protect 0.0015 BTC with common persent fee 1.2% and common absolute fee 0.0001 BTC. """
        admin.set_wallet_amount(balance=2000000, currency='BTC', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='BTC', email=user2.email)
        admin.set_fee(currency_id=admin.currency['BTC'], tp=33, add=100000, mult=12000000)
        user1.transferprotect_params(out_curr='BTC')
        assert user1.resp_transferprotect_params['min_balance_limit'] == '0.00010102'
        user1.transferprotect_calc(amount=0.0015, out_curr='BTC')
        assert user1.resp_transferprotect_calc['account_amount'] == '0.001618'
        user1.transferprotect_create(amount=0.0015, out_curr='BTC', tgt=user2.email)
        assert user1.resp_transferprotect_create['account_amount'] == '0.001618'
        assert user1.resp_transferprotect_create['out_fee_amount'] == '0.000118'
        user2.transferprotect_confirm(lid=user1.transferprotect_lid)
        assert user2.resp_transferprotect_confirm['exception'] == 'NotFound'
        user1.transferprotect_confirm(lid=user1.transferprotect_lid)
        assert user1.resp_transferprotect_confirm['account_amount'] == '0.001618'
        user1.get_balances(curr='BTC')
        assert user1.resp_balances['BTC'] == '0.000382'
        user2.get_balances(curr='BTC')
        assert user2.resp_balances['BTC'] == '0.0015'
        user2.transferprotect_refuse(lid=user1.transferprotect_lid)
        assert user2.resp_transferprotect_refuse['exception'] == 'NotFound'
        user1.get_balances(curr='BTC')
        assert user1.resp_balances['BTC'] == '0.000382'
        user2.get_balances(curr='BTC')
        assert user2.resp_balances['BTC'] == '0.0015'


    def test_transferprotect_5(self, _personal_user_data):
        """ Transfer with protect 10.15 RUB with common persent fee 7% and common absolute fee 2 RUB,
        and with personal persent fee 5% with personal absolute fee 1.3 RUB. """
        admin.set_wallet_amount(balance=11950000000, currency='RUB', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='RUB', email=user2.email)
        admin.set_fee(currency_id=admin.currency['RUB'], tp=33, add=2000000000, mult=70000000)
        admin.set_fee(currency_id=admin.currency['RUB'], tp=33, add=1300000000, mult=50000000, is_active=True,
                      owner_id=user1.id)
        user1.transferprotect_params(out_curr='RUB')
        assert user1.resp_transferprotect_params['min_balance_limit'] == '1.32'
        user1.transferprotect_calc(amount=10.15, out_curr='RUB')
        assert user1.resp_transferprotect_calc['account_amount'] == '11.96'
        admin.set_wallet_amount(balance=12000000000, currency='RUB', email=user1.email)
        user1.transferprotect_create(amount=10.15, out_curr='RUB', tgt=user2.email)
        assert user1.resp_transferprotect_create['account_amount'] == '11.96'
        assert user1.resp_transferprotect_create['out_fee_amount'] == '1.81'
        user1.transferprotect_confirm(lid=user1.transferprotect_lid)
        user1.get_balances(curr='RUB')
        assert user1.resp_balances['RUB'] == '0.04'
        user2.get_balances(curr='RUB')
        assert user2.resp_balances['RUB'] == '10.15'
        admin.set_fee(currency_id=admin.currency['RUB'], tp=33, is_active=False, owner_id=user1.id)


    def test_transferprotect_6(self, _personal_user_data):
        """ Create transfer with fee for operation and refused him.
            Checking than amount returned to payer account without fee. """
        admin.set_wallet_amount(balance=12000000000, currency='RUB', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='RUB', email=user2.email)
        admin.set_fee(currency_id=admin.currency['RUB'], tp=33, add=2000000000, mult=70000000)
        admin.set_fee(currency_id=admin.currency['RUB'], tp=33, add=1300000000, mult=50000000, is_active=True,
                      owner_id=user1.id)
        user1.transferprotect_create(amount=10.15, out_curr='RUB', tgt=user2.email)
        user2.transferprotect_refuse(lid=user1.transferprotect_lid)
        user1.get_balances(curr='RUB')
        assert user1.resp_balances['RUB'] == '10.19'
        admin.set_fee(currency_id=admin.currency['RUB'], tp=33, is_active=False, owner_id=user1.id)


    def test_transferprotect_7(self, _personal_user_data):
        """ Transfer with protect 0.002 BTC with common persent fee 7% and common absolute fee 0.0003 BTC,
            and with personal absolute fee 0.0004 BTC. """
        admin.set_wallet_amount(balance=2400000, currency='BTC', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='BTC', email=user2.email)
        admin.set_fee(currency_id=admin.currency['BTC'], tp=33, add=300000, mult=70000000)
        admin.set_fee(currency_id=admin.currency['BTC'], tp=33, add=400000, is_active=True, owner_id=user1.id)
        user1.transferprotect_params(out_curr='BTC')
        assert user1.resp_transferprotect_params['min_balance_limit'] == '0.000401'
        user1.transferprotect_calc(amount=0.002, out_curr='BTC')
        assert user1.resp_transferprotect_calc['account_amount'] == '0.0024'
        assert user1.resp_transferprotect_calc['out_fee_amount'] == '0.0004'
        user1.transferprotect_create(amount=0.002, out_curr='BTC', tgt=user2.email)
        assert user1.resp_transferprotect_create['account_amount'] == '0.0024'
        assert user1.resp_transferprotect_create['out_amount'] == '0.002'
        assert user1.resp_transferprotect_create['out_fee_amount'] == '0.0004'
        user1.get_balances(curr='BTC')
        assert user1.resp_balances['BTC'] == '0'
        user2.get_balances(curr='BTC')
        assert user2.resp_balances['BTC'] == '0'
        time.sleep(50)
        user1.transferprotect_get(lid=user1.transferprotect_lid)
        assert user1.resp_transferprotect_get['status'] != 'reject'
        user1.get_balances(curr='BTC')
        assert user1.resp_balances['BTC'] == '0'
        ''' Системный експайри отключен
        user1.transferprotect_get(lid=user1.transferprotect_lid)
        assert user1.resp_transferprotect_get['status'] == 'reject'
        user1.get_balances(curr='BTC')
        assert user1.resp_balances['BTC'] == '0.002'
        user2.get_balances(curr='BTC')
        assert user2.resp_balances['BTC'] == '0'
        admin.set_fee(currency_id=admin.currency['BTC'], tp=33, is_active=False, owner_id=user1.id)
        '''

    def test_transferprotect_8(self):
        """ Test list for owner. """
        admin.set_wallet_amount(balance=20000000000, currency='UAH', email=user1.email)
        admin.set_fee(currency_id=admin.currency['UAH'], tp=33, add=2000000000, mult=70000000)
        user1.transferprotect_create(amount=10.15, out_curr='UAH', tgt=user2.email)
        user1.transferprotect_list(out_curr='UAH', first=None, count=50)
        ls_currency = [ls['out_curr'] for ls in user1.resp_transferprotect_list['data']]
        ls_status = [ls['status'] for ls in user1.resp_transferprotect_list['data']]
        assert tools.equal_list(ls_currency, 'UAH')
        assert 'done' in ls_status
        assert 'wait' in ls_status
        assert 'reject' in ls_status
        user1.transferprotect_list(out_curr='UAH', first=None, count=2)
        assert len(user1.resp_transferprotect_list['data']) == 2

    def test_transferprotect_9(self):
        """ Test list for payee. """
        user2.transferprotect_list(out_curr='UAH', first=None, count=50)
        assert len(user2.resp_transferprotect_list['data']) == 0

    def test_transferprotect_10(self):
        """ Transfer with amount + fee that more then user has on his account. """
        admin.set_wallet_amount(balance=11950000000, currency='RUB', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='RUB', email=user2.email)
        admin.set_fee(currency_id=admin.currency['RUB'], tp=33, add=1300000000, mult=50000000)
        user1.transferprotect_create(amount=10.15, out_curr='RUB', tgt=user2.email)
        assert user1.resp_transferprotect_create['exception'] == 'InsufficientFunds'
        user1.get_balances(curr='RUB')
        assert user1.resp_balances['RUB'] == '11.95'

    def test_transferprotect_11(self):
        """ Transfer with not real currency. """
        user1.transferprotect_create(amount=1, out_curr='UHA', tgt=user2.email)
        assert user1.resp_transferprotect_create['exception'] == 'InvalidCurrency'

    def test_transferprotect_12(self):
        """ Transfer to not real user. """
        admin.set_wallet_amount(balance=1000000000, currency='RUB', email=user1.email)
        admin.set_fee(currency_id=admin.currency['RUB'], tp=33)
        user1.transferprotect_create(amount=1, out_curr='RUB', tgt='anycashuser200@mailinator.com')
        assert user1.resp_transferprotect_create['exception'] == 'InvalidContact'

    def test_transferprotect_13(self):
        """ Transfer with wrong format amount. """
        admin.set_wallet_amount(balance=1000000000, currency='RUB', email=user1.email)
        admin.set_fee(currency_id=admin.currency['RUB'], tp=33)
        user1.transferprotect_create(amount=0.999, out_curr='RUB', tgt=user2.email)
        assert user1.resp_transferprotect_create['exception'] == 'InvalidAmountFormat'

    def test_transferprotect_14(self):
        """ Transfer with amount less then tech_min and more than tech_max by currency table. """
        admin.set_wallet_amount(balance=11000000000, currency='RUB', email=user1.email)
        admin.set_fee(currency_id=admin.currency['RUB'], tp=33)
        admin.set_currency_data(is_crypto=False, admin_min=1000000000, admin_max=10000000000)
        user1.transferprotect_create(amount=0.99, out_curr='RUB', tgt=user2.email)
        assert user1.resp_transferprotect_create['exception'] == 'AmountTooSmall'
        user1.transferprotect_create(amount=10.01, out_curr='RUB', tgt=user2.email)
        assert user1.resp_transferprotect_create['exception'] == 'AmountTooBig'
        admin.set_currency_data(is_crypto=False, admin_min=10000000, admin_max=3000000000000)


class TestTransferByCheque:
    """ Testing cheque... """

    def test_0(self, create_session):
        """ Warm. """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    @pytest.mark.skip
    def test_cheque_1(self, _disable_2type):
        """ Transfer with common cheque 0.1 UAH with 2 step auth. """
        admin.set_wallet_amount(balance=100000000, currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='UAH', email=user3.email)
        admin.set_fee(currency_id=admin.currency['UAH'], tp=30, add=1000000000)
        admin.set_fee(currency_id=admin.currency['UAH'], tp=35)
        user1.set_2type(tp='0')
        user1.cheque_params(out_curr='UAH')
        assert user1.resp_cheque_params['max'] == '3000'
        assert user1.resp_cheque_params['min'] == '0.01'
        assert user1.resp_cheque_params['out_fee'] == {'add': '0', 'max': '0', 'method': 'ceil', 'min': '0', 'mult': '0'}
        user1.cheque_calc(amount=0.11, out_curr='UAH')
        assert user1.resp_cheque_calc['account_amount'] == '0.11'
        assert user1.resp_cheque_calc['out_fee_amount'] == '0'
        user1.cheque_create(amount=0.1, out_curr='UAH', tgt=None, trust_expiry=None)
        assert user1.resp_cheque_create['exception'] == 'Auth2Required'
        user1.get_balances('UAH')
        assert user1.resp_balances['UAH'] == '0.1'
        user1.confirm_registration(key=user1.cheque_2type, code=admin.get_onetime_code(user1.email), user=user1)
        assert user1.resp_confirm['account_amount'] == '0.1'
        assert user1.resp_confirm['status'] == 'new'
        assert user1.resp_confirm['tgt'] is None
        user1.get_balances('UAH')
        assert user1.resp_balances['UAH'] == '0'
        user3.cheque_cash(token=user1.resp_confirm['cheque'])
        assert user3.resp_cheque_cash['out_amount'] == '0.1'
        assert user3.resp_cheque_cash['cheque'] == user1.resp_confirm['cheque'].split('/')[0]
        assert user3.resp_cheque_cash['status'] == 'done'
        user1.get_balances('UAH')
        assert user1.resp_balances['UAH'] == '0'
        user3.get_balances('UAH')
        assert user3.resp_balances['UAH'] == '0.1'

    @pytest.mark.skip
    def test_cheque_2(self, _disable_2type):
        """ Transfer with personal cheque 1 USD. """
        admin.set_wallet_amount(balance=2000000000, currency='USD', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user2.email)
        admin.set_fee(currency_id=admin.currency['USD'], tp=35)
        user1.set_2type(tp='0')
        user1.cheque_calc(amount=2.01, out_curr='USD')
        assert user1.resp_cheque_calc['account_amount'] == '2.01'
        user1.cheque_create(amount=1, out_curr='USD', tgt=user2.email, trust_expiry=None)
        assert user1.resp_cheque_create['exception'] == 'Auth2Required'
        user1.confirm_registration(key=user1.cheque_2type, code=admin.get_onetime_code(user1.email), user=user1)
        assert user1.resp_confirm['tgt'] == 'AC179'
        assert user1.resp_confirm['status'] == 'new'
        user1.get_balances('USD')
        assert user1.resp_balances['USD'] == '1'
        user2.get_balances('USD')
        assert user2.resp_balances['USD'] == '0'
        user1.verify_cheque(cheque=user1.resp_confirm['cheque'])
        assert user1.resp_verify_cheque['amount'] == '1'
        assert user1.resp_verify_cheque['can_be_accepted'] == 'yes'
        assert user1.resp_verify_cheque['payway'] == 'anycash'
        user2.verify_cheque(cheque=user1.resp_confirm['cheque'].split('/')[0])
        assert user2.resp_verify_cheque['currency'] == 'USD'
        assert user2.resp_verify_cheque['tgt'] == 'AC179'
        assert user2.resp_verify_cheque['fee'] == {}
        user3.verify_cheque(cheque=user1.resp_confirm['cheque'])
        assert user3.resp_verify_cheque['exception'] == 'NotFound'
        user2.cheque_cash(token=user1.resp_confirm['cheque'])
        assert user2.resp_cheque_cash['out_amount'] == '1'
        assert user2.resp_cheque_cash['cheque'] == user1.resp_confirm['cheque'].split('/')[0]
        assert user2.resp_cheque_cash['status'] == 'done'
        user1.get_balances('USD')
        assert user1.resp_balances['USD'] == '1'
        user2.get_balances('USD')
        assert user2.resp_balances['USD'] == '1'

    @pytest.mark.skip
    def test_cheque_3(self):
        """ Transfer with personal cheque 0.0033 BTC and cashing him after trust_expiry: trust_expiry in request
            more then trust_expiry by defoult. """
        admin.set_wallet_amount(balance=3300000, currency='BTC', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='BTC', email=user2.email)
        admin.set_fee(currency_id=admin.currency['BTC'], tp=35)
        user1.cheque_create(amount=0.0033, out_curr='BTC', tgt=user2.email, trust_expiry='80s')
        time.sleep(63)
        user1.cheque_cash(token=user1.cheque)
        assert user1.resp_cheque_cash['exception'] == 'TrustExpiry'
        user1.get_balances('BTC')
        assert user1.resp_balances['BTC'] == '0'
        user2.redeem_cheque(cheque=user1.cheque)
        assert user2.resp_redeem_cheque['status'] == 'done'
        assert user2.resp_redeem_cheque['out_amount'] == '0.0033'
        user1.get_balances('BTC')
        assert user1.resp_balances['BTC'] == '0'
        user2.get_balances('BTC')
        assert user2.resp_balances['BTC'] == '0.0033'
        user2.redeem_cheque(cheque=user1.cheque)
        assert user2.resp_redeem_cheque['exception'] == 'NotFound'
        user1.cheque_cash(token=user1.cheque)
        assert user1.resp_cheque_cash['exception'] == 'NotFound'
        admin.set_wallet_amount(balance=3300000, currency='BTC', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='BTC', email=user3.email)
        user1.cheque_create(amount=0.0033, out_curr='BTC', tgt=user2.email, trust_expiry='15s')
        user3.cheque_cash(token=user1.cheque)
        assert user3.resp_cheque_cash['exception'] == 'NotFound'
        user3.get_balances('BTC')
        assert user3.resp_balances['BTC'] == '0'
        time.sleep(20)
        user1.cheque_cash(token=user1.cheque)
        assert user1.resp_cheque_cash['status'] == 'done'
        user1.get_balances('BTC')
        assert user1.resp_balances['BTC'] == '0.0033'
        user2.cheque_cash(token=user1.cheque)
        assert user2.resp_cheque_cash['exception'] == 'NotFound'

    @pytest.mark.skip
    def test_cheque_4(self):
        """ Transfer with personal cheque 5 RUB with common persent fee 1% and common absolute fee 0.5 RUB,
            trust_expiry less then defoult trust_expiry. """
        admin.set_wallet_amount(balance=6000000000, currency='RUB', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='RUB', email=user2.email)
        admin.set_fee(currency_id=admin.currency['RUB'], tp=35, mult=10000000, add=500000000)
        user1.cheque_params(out_curr='RUB')
        assert user1.resp_cheque_params['min_balance_limit'] == '0.52'
        assert user1.resp_cheque_params['out_fee'] == {'add': '0.5', 'max': '0', 'method': 'ceil', 'min': '0',
                                                       'mult': '0.01'}
        user1.cheque_create(amount=5, out_curr='RUB', tgt=user2.email, trust_expiry='20s')
        assert user1.resp_cheque_create['account_amount'] == '5.55'
        assert user1.resp_cheque_create['out_amount'] == '5'
        assert user1.resp_cheque_create['out_fee_amount'] == '0.55'
        user3.verify_cheque(cheque=user1.cheque)
        assert user3.resp_verify_cheque['exception'] == 'NotFound'
        user3.redeem_cheque(cheque=user1.cheque)
        assert user3.resp_redeem_cheque['exception'] == 'NotFound'
        time.sleep(22)
        user1.cheque_cash(token=user1.cheque)
        assert user1.resp_cheque_cash['out_amount'] == '5'
        user1.get_balances('RUB')
        assert user1.resp_balances['RUB'] == '5.45'

    @pytest.mark.skip(reason='Bug')
    def test_cheque_5(self):
        """ Transfer with common cheque 0.003 LTC with common absolute fee 0.0001 LTC, with default trust_expiry. """
        admin.set_wallet_amount(balance=3100000, currency='LTC', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='LTC', email=user2.email)
        admin.set_fee(currency_id=admin.currency['LTC'], tp=35, add=100000)
        user1.cheque_calc(out_curr='LTC', amount=0.0031)
        assert user1.resp_cheque_calc['account_amount'] == '0.0032'
        assert user1.resp_cheque_calc['out_fee_amount'] == '0.0001'
        user1.cheque_create(amount=0.003, out_curr='LTC', tgt=None, trust_expiry=None)
        user1.cheque_get(part_token=user1.part_cheque)
        assert user1.resp_cheque_get['out_amount'] == '0.003'
        assert user1.resp_cheque_get['is_cashed'] is False
        user3.cheque_get(part_token=user1.part_cheque)
        assert user3.resp_cheque_get['out_amount'] == '0.003'
        assert user3.resp_cheque_get['is_cashed'] is False
        user1.cheque_cash(token=user1.cheque)
        assert user1.resp_cheque_cash['exception'] == 'TrustExpiry'
        time.sleep(63)
        user1.redeem_cheque(cheque=user1.cheque)
        assert user1.resp_redeem_cheque['out_amount'] == '0.003'
        user1.get_balances('LTC')
        assert user1.resp_balances['LTC'] == '0.003'
        user2.cheque_cash(token=user1.cheque)
        assert user2.resp_cheque_cash['exception'] == 'NotFound'
        user1.cheque_get(part_token=user1.part_cheque)
        assert user1.resp_cheque_get['is_cashed'] is True

    def test_cheque_6(self, _personal_user_data):
        """ Transfer with common cheque 5 USD with common percent fee 5% and absolute fee 1 USD,
            with personal percent fee 1% and absolute fee 0.5 USD. """
        admin.set_wallet_amount(balance=6000000000, currency='USD', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user2.email)
        admin.set_fee(currency_id=admin.currency['USD'], tp=35, add=1000000000, mult=50000000)
        admin.set_fee(currency_id=admin.currency['USD'], tp=35, add=500000000, mult=10000000, owner_id=user1.id,
                      is_active=True)
        user1.cheque_params(out_curr='USD')
        assert user1.resp_cheque_params['min_balance_limit'] == '0.52'
        assert user1.resp_cheque_params['out_fee'] == {'add': '0.5', 'max': '0', 'method': 'ceil', 'min': '0',
                                                       'mult': '0.01'}
        user1.cheque_create(amount=5, out_curr='USD', tgt=None, trust_expiry=None)
        assert user1.resp_cheque_create['account_amount'] == '5.55'
        assert user1.resp_cheque_create['out_curr'] == 'USD'
        assert user1.resp_cheque_create['out_fee_amount'] == '0.55'
        user2.redeem_cheque(cheque=user1.cheque)
        assert user2.resp_redeem_cheque['out_amount'] == '5'
        user2.cheque_cash(token=user1.cheque)
        assert user2.resp_cheque_cash['exception'] == 'NotFound'
        user1.get_balances('USD')
        assert user1.resp_balances['USD'] == '0.45'
        user2.get_balances('USD')
        assert user2.resp_balances['USD'] == '5'
        user1.cheque_get(part_token=user1.part_cheque)
        assert user1.resp_cheque_get['is_cashed'] is True
        user3.cheque_get(part_token=user1.part_cheque)
        assert user3.resp_cheque_get['is_cashed'] is True
        user2.cheque_get(part_token=user1.part_cheque)
        assert user2.resp_cheque_get['is_cashed'] is True

    def test_cheque_7(self, _personal_user_data):
        """ Transfer with personal cheque 0.003 ETH with common absolute fee 0.001 ETH,
            with personal absolute fee 0.0001 ETH. """
        admin.set_wallet_amount(balance=3140000, currency='ETH', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='ETH', email=user2.email)
        admin.set_fee(currency_id=admin.currency['ETH'], tp=35, add=1000000)
        admin.set_fee(currency_id=admin.currency['ETH'], tp=35, mult=50000000, owner_id=user1.id, is_active=True)
        user1.cheque_calc(amount=0.003, out_curr='ETH')
        assert user1.resp_cheque_calc['account_amount'] == '0.00315'
        assert user1.resp_cheque_calc['out_fee_amount'] == '0.00015'
        admin.set_wallet_amount(balance=1000000000, currency='ETH', email=user1.email)
        user1.cheque_create(amount=0.003, out_curr='ETH', tgt='AC179', trust_expiry='10s')
        assert user1.resp_cheque_create['account_amount'] == '0.00315'
        user1.cheque_get(part_token=user1.part_cheque)
        assert user1.resp_cheque_get['out_amount'] == '0.003'
        user2.cheque_get(part_token=user1.part_cheque)
        assert 'trust_expiry' in user2.resp_cheque_get
        user3.cheque_get(part_token=user1.part_cheque)
        assert user3.resp_cheque_get['exception'] == 'NotFound'
        user2.cheque_cash(token=user1.cheque)
        time.sleep(12)
        user1.cheque_cash(token=user1.cheque)
        assert user1.resp_cheque_cash['exception'] == 'NotFound'
        user1.cheque_get(part_token=user1.part_cheque)
        assert user1.resp_cheque_get['is_cashed'] is True
        user2.cheque_get(part_token=user1.part_cheque)
        assert user2.resp_cheque_get['is_cashed'] is True

    def test_cheque_8(self):
        """ List cheques for owner. """
        user1.cheque_list(out_curr='USD', first=None, count=100)
        currency_ls = [ls['out_curr'] for ls in user1.resp_cheque_list['data']]
        assert tools.equal_list(_list=currency_ls, elem='USD')
        assert admin.get_order(lid=user1.resp_cheque_list['data'][-1]['lid'])['owner_id'] == user1.id

    def test_cheque_9(self):
        """ List cheques with 2 elements. """
        user1.cheque_list(out_curr='USD', first=0, count=2)
        assert len(user1.resp_cheque_list['data']) == 2

    def test_cheque_10(self):
        """ List all complete cheque for receiver with USD currency. """
        user2.cashed_list(out_curr='USD')
        ls_status = [ls['status'] for ls in user2.resp_cashed_list['data']]
        ls_tgt = [ls['tgt'] for ls in user2.resp_cashed_list['data']]
        assert 'done' in ls_status
        assert 'AC179' in ls_tgt

    def test_cheque_11(self):
        """ List complete of tree elements. """
        user2.cashed_list(out_curr='USD', count=3)
        assert len(user2.resp_cashed_list['data']) == 3

    def test_cheque_13(self):
        """ List complete with ftime filter. """
        user2.cashed_list(out_curr='USD', ord_by='ftime', ord_dir=False)
        begin_ctime = user2.resp_cashed_list['data'][-2]['ctime']
        m_ctime = user2.resp_cashed_list['data'][-3]['ctime']
        end_ctime = user2.resp_cashed_list['data'][-4]['ctime']
        user2.cashed_list(out_curr='USD', begin=begin_ctime, end=end_ctime)
        assert user2.resp_cashed_list['data'][-1]['ctime'] == begin_ctime
        assert user2.resp_cashed_list['data'][0]['ctime'] == m_ctime

    def test_cheque_14(self):
        """ Banned on creating cheque with not enough money on owner balance. """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user1.email)
        admin.set_fee(currency_id=admin.currency['UAH'], tp=35, add=10000000)
        user1.cheque_create(amount=1, out_curr='UAH', tgt=None, trust_expiry=None)
        assert user1.resp_cheque_create['exception'] == 'InsufficientFunds'
        user1.get_balances('UAH')
        assert user1.resp_balances['UAH'] == '1'

    def test_cheque_15(self):
        """ Banned on creating cheque with not real currency. """
        user1.cheque_create(amount=1, out_curr='UHA', tgt=None, trust_expiry=None)
        assert user1.resp_cheque_create['exception'] == 'InvalidCurrency'

    def test_cheque_16(self):
        """ Banned on creating cheque with wrong format amount. """
        user1.cheque_create(amount=0.011, out_curr='UAH', tgt=None, trust_expiry=None)
        assert user1.resp_cheque_create['exception'] == 'InvalidAmountFormat'

    def test_cheque_17(self):
        """ Banned on creating cheque with wrong payee. """
        user1.cheque_create(amount=0.01, out_curr='UAH', tgt='anycashuser5000@mailinator.com', trust_expiry=None)
        assert user1.resp_cheque_create['exception'] == 'InvalidContact'

    def test_cheque_18(self):
        """ Banned on creating cheque amount less than tech_min in currency table. """
        admin.set_currency_data(is_crypto=False, admin_min=1000000000, admin_max=5000000000)
        admin.set_wallet_amount(currency='UAH', balance=10000000000, email=user1.email)
        admin.set_fee(currency_id=admin.currency['UAH'], tp=35)
        user1.cheque_create(amount=0.99, out_curr='UAH', tgt=None, trust_expiry=None)
        assert user1.resp_cheque_create['exception'] == 'AmountTooSmall'
        user1.cheque_create(amount=5.01, out_curr='UAH', tgt=None, trust_expiry=None)
        assert user1.resp_cheque_create['exception'] == 'AmountTooBig'
        admin.set_currency_data(is_crypto=False, admin_min=10000000, admin_max=3000000000000)

    def test_cheque_19(self):
        """ Redeem cheque with none cheque. """
        user1.redeem_cheque(cheque=None)
        assert user1.resp_redeem_cheque['exception'] == 'InvalidParam'


@pytest.mark.skip(reason='Disabled functional')
class TestTransferByChequeWithExchange:

    def test_0(self, create_session):
        """ Warm """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    @pytest.mark.skip
    def test_transfer_20(self):
        """ Transfer 1 USD by cheque with convertation from UAH without exchange fee, with percent fee for transfer 10%,
            with absolute fee for transfer 0.1 USD. """
        admin.set_wallet_amount(balance=40000000000, currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user2.email)
        admin.set_fee_transfer(mult=200000000, add=200000000, currency_id='USD', tp=30)
        admin.set_fee_transfer(mult=100000000, add=100000000, currency_id='USD', tp=35)
        admin.set_rate_exchange(rate=28199900000, fee=0, in_currency='UAH', out_currency='USD')
        user1.transfer_params(in_curr='UAH', out_curr='USD')
        #  assert user1.response_transfer_params['min'] == '0.29'  need fix
        assert user1.response_transfer_params['rate'] == ['28.1999', '1']
        assert user1.response_transfer_params['from_in_curr_balance'] == '1.41'
        user1.transfer(amount=1, in_curr='UAH', out_curr='USD', email_user=None)
        assert user1.response_transfer['account_amount'] == '33.84'
        assert user1.response_transfer['in_amount'] == '28.2'
        assert user1.response_transfer['out_amount'] == '1'
        user2.cash_cheque(cheque=User.cheque)
        assert user2.response_cash_cheque['account_amount'] == '33.84'

    @pytest.mark.skip
    def test_transfer_21(self):
        """ Transfer 10 USD by cheque with convertation from BTC without exchange fee, with percent fee for transfer 50%,
            with absolute fee for transfer 5 USD. """
        admin.set_wallet_amount(balance=10000000, currency='BTC', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user2.email)
        admin.set_fee_transfer(mult=500000000, add=5000000000, currency_id='USD', tp=35)
        admin.set_rate_exchange(rate=3580654100000, fee=0, in_currency='BTC', out_currency='USD')
        user1.transfer_params(in_curr='BTC', out_curr='USD')
        #  assert user1.response_transfer_params['min'] == '0.29'  need fix
        assert user1.response_transfer_params['rate'] == ['1', '3580.6541']
        assert user1.response_transfer_params['from_in_curr_balance'] == '35.8'
        user1.transfer(amount=1, in_curr='BTC', out_curr='USD', email_user=None)
        assert user1.response_transfer['account_amount'] == '0.00558559'
        assert user1.response_transfer['in_amount'] == '0.00279279'
        assert user1.response_transfer['out_amount'] == '10'
        user2.cash_cheque(cheque=User.cheque)
        assert admin.get_wallet_data(email=user1.email, currency_id='BTC') == 0.00441441
        assert admin.get_wallet_data(email=user2.email, currency_id='USD') == 10.0

    @pytest.mark.skip
    def test_transfer_22(self):
        """ Transfer with check 1 USD to other user from UAH with common exchange fee 1%. """
        admin.set_wallet_amount(balance=30000000000, currency='UAH', email=user1.email)
        admin.set_fee_transfer(currency_id='USD', tp=35)
        admin.set_rate_exchange(rate=28199900000, fee=10000000, in_currency='UAH', out_currency='USD')
        user1.transfer(amount=1, in_curr='UAH', out_curr='USD', email_user=None)
        assert user1.response_transfer['in_amount'] == '28.49'
        assert user1.response_transfer['out_amount'] == '1'
        user2.cash_cheque(cheque=User.cheque)
        # assert user2.response_cash_cheque['out_amount'] == '1'

    @pytest.mark.skip
    def test_transfer_23(self):
        """ Transfer with check 0.43 BTC to other user from USD with common fee 0.05%. """
        admin.set_wallet_amount(balance=1600000000000, currency='UAH', email=user1.email)
        admin.set_fee_transfer(currency_id='BTC', tp=35)
        admin.set_rate_exchange(rate=3580654100000, fee=500000, in_currency='USD', out_currency='BTC')
        user1.transfer(amount=0.43, in_curr='USD', out_curr='BTC', email_user=None)
        assert user1.response_transfer['in_amount'] == '1540.46'
        assert user1.response_transfer['out_amount'] == '0.43'
        user2.cash_cheque(cheque=User.cheque)
        # assert user2.response_cash_cheque['in_amount'] == '1540.46'

    @pytest.mark.skip
    def test_transfer_24(self):
        """ Transfer with check 1 USD to other user from UAH with common fee 1% with operations bonus 0.5%. """
        admin.set_wallet_amount(balance=30000000000, currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user2.email)
        admin.set_fee_transfer(currency_id='USD', tp=35)
        admin.set_rate_exchange(rate=28199900000, fee=10000000, in_currency='UAH', out_currency='USD')
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=5000000, is_active=True)
        user1.transfer(amount=1, in_curr='UAH', out_curr='USD', email_user=None)
        assert user1.response_transfer['in_amount'] == '28.35'
        assert user1.response_transfer['out_amount'] == '1'
        user2.cash_cheque(cheque=User.cheque)
        assert user2.response_cash_cheque['in_amount'] == '28.35'
        assert user2.response_cash_cheque['account_amount'] == '28.35'
        assert admin.get_wallet_data(email=user1.email, currency_id='UAH') == 1.65
        assert admin.get_wallet_data(email=user2.email, currency_id='USD') == 1
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=5000000, is_active=False)

    @pytest.mark.skip
    def test_transfer_25(self, _personal_user_data):
        """ Transfer with check 100 UAH with convertation from USD with common exchange fee 1%
            with personal exchange fee 0.5%. """
        admin.set_wallet_amount(balance=5000000000, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=10000000, in_currency='USD', out_currency='UAH')
        admin.set_fee_transfer(currency_id='UAH', tp=35)
        admin.set_personal_exchange_fee(is_active=True, in_currency='USD', out_currency='UAH', email=user1.email,
                                        fee=5000000)
        user1.transfer(amount=100, in_curr='USD', out_curr='UAH', email_user=None)
        assert user1.response_transfer['account_amount'] == '3.57'
        assert user1.response_transfer['in_amount'] == '3.57'
        assert user1.response_transfer['out_amount'] == '100'
        user2.cash_cheque(cheque=User.cheque)
        assert user2.response_cash_cheque['in_amount'] == '3.57'
        assert user2.response_cash_cheque['out_amount'] == '100'
        admin.set_personal_exchange_fee(is_active=False, in_currency='USD', out_currency='UAH', email=user1.email)

    @pytest.mark.skip
    def test_transfer_26(self, _personal_user_data):
        """ Transfer with check 0.0055 BTC with convertation from USD with common fee 1% with personal fee 0.05%. """
        admin.set_wallet_amount(balance=20000000000, currency='USD', email=user1.email)
        admin.set_fee_transfer(currency_id='BTC', tp=35)
        admin.set_rate_exchange(rate=3580654100000, fee=10000000, in_currency='USD', out_currency='BTC')
        admin.set_personal_exchange_fee(is_active=True, in_currency='USD', out_currency='BTC', email=user1.email,
                                        fee=500000)
        user1.transfer(amount=0.0055, in_curr='USD', out_curr='BTC', email_user=None)
        assert user1.response_transfer['in_amount'] == '19.71'
        assert user1.response_transfer['out_amount'] == '0.0055'
        user2.cash_cheque(cheque=User.cheque)
        # assert user2.response_cash_cheque['in_curr'] == 'USD'
        # assert user2.response_cash_cheque['out_curr'] == 'BTC'
        admin.set_personal_exchange_fee(is_active=False, in_currency='USD', out_currency='BTC', email=user1.email,
                                        fee=500000)

    @pytest.mark.skip
    def test_transfer_27(self,  _personal_user_data):
        """ Transfer 10.05 USD with convertation from UAH with common exchange fee 5% with personal exchange fee 3.5%
            with operations bonus 1%, with common fee for transfer 10% and 1 USD, with personal fee for transfer
            5% and 0.5 USD. """
        admin.set_wallet_amount(balance=300000000000, currency='UAH', email=user1.email)
        admin.set_rate_exchange(rate=28199900000, fee=50000000, in_currency='UAH', out_currency='USD')
        admin.set_fee_transfer(mult=100000000, add=1000000000, currency_id='USD', tp=30)
        admin.set_fee_transfer(mult=50000000, add=500000000, currency_id='USD', tp=30, owner_id=user1.email)
        admin.set_personal_exchange_fee(is_active=True, in_currency='UAH', out_currency='USD', email=user1.email,
                                        fee=35000000)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=True)
        user1.transfer_params(in_curr='UAH', out_curr='USD')
        assert user1.response_transfer_params['out_fee'] == {'add': '0.5', 'max': '0', 'min': '0', 'method': 'ceil',
                                                             'mult': '0.05'}
        assert user1.response_transfer_params['from_in_curr_balance'] == '10.27'
        user1.transfer(amount=10.05, in_curr='UAH', out_curr='USD', email_user=user2.email)
        assert user1.response_transfer['in_amount'] == '322.82'
        assert user1.response_transfer['out_amount'] == '10.05'
        user2.cash_cheque(cheque=User.cheque)
        # assert user2.response_cash_cheque['account_amount'] ==
        admin.set_personal_exchange_fee(is_active=False, in_currency='UAH', out_currency='USD', email=user1.email)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=False)
        admin.set_fee_transfer(currency_id='USD', tp=30, owner_id=user1.email, is_active=False)

    @pytest.mark.skip
    def test_transfer_28(self, _personal_user_data):
        """ Transfer 0.43 BTC with convertation from USD with common fee 1% with personal exchange fee 0.05%
            with operations bonus 1 USD = 1%, with common fee for transfer 10% and 0.01 BTC,
            with personal fee for transfer 5% and 0.005 BTC. """
        admin.set_wallet_amount(balance=2000000000000, currency='USD', email=user1.email)
        admin.set_rate_exchange(rate=3580654100000, fee=10000000, in_currency='USD', out_currency='BTC')
        admin.set_fee_trasfer(mult=100000000, add=10000000, currency_id='BTC', tp=30)
        admin.set_fee_trasfer(mult=50000000, add=5000000, currency_id='BTC', tp=30, owner_id=user1.email)
        admin.set_personal_exchange_fee(is_active=True, in_currency='USD', out_currency='BTC', email=user1.email,
                                        fee=500000)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=True)
        user1.transfer_calc(amount=0.43, in_curr='USD', out_curr='BTC')
        assert user1.response_transfer_calc['account_amount'] == '1635.41'
        assert user1.response_transfer_calc['in_amount'] == '1540.46'
        assert user1.response_transfer_calc['out_amount'] == '0.43'
        assert user1.response_transfer_calc['in_fee_amount'] == '94.95'
        assert user1.response_transfer_calc['out_fee_amount'] == '0.0265'
        user1.transfer(amount=0.43, in_curr='USD', out_curr='BTC', email_user=user2.email)
        assert user1.response_transfer['account_amount'] == '1635.41'
        assert user1.response_transfer['in_amount'] == '1540.46'
        assert user1.response_transfer['out_amount'] == '0.43'
        user2.cash_cheque(cheque=User.cheque)
        assert user1.response_transfer['account_amount'] == '1635.41'
        assert user1.response_transfer['in_amount'] == '1540.46'
        assert user1.response_transfer['out_amount'] == '0.43'
        admin.set_personal_exchange_fee(is_active=False, in_currency='USD', out_currency='BTC', email=user1.email)
        admin.set_bonus_level(name='1usd', points=None, amount=1000000000, surplus=10000000, is_active=False)
        admin.set_fee_transfer(currency_id='BTC', tp=30, owner_id=user1.email, is_active=False)


@pytest.mark.skip(reason='Disabled functional')
class TestTransferWithProtectedCheque:

    def test_0(self, create_session):
        """ Warm """
        global admin, user1, user2, user3
        admin, user1, user2, user3 = create_session

    def test_1(self):
        """ Transfer currency by protected cheque without fee for operation: transfer 0.1 UAH. """
        admin.set_wallet_amount(balance=100000000, currency='UAH', email=user1.email)
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user2.email)
        admin.set_fee(currency_id=admin.currency['UAH'], tp=30, mult=10000000)
        admin.set_fee(currency_id=admin.currency['UAH'], tp=60)
        user1.chequeprotect_calc(amount=0.1, out_curr='UAH')
        assert user1.resp_chequeprotect_calc['account_amount'] == '0.1'
        assert user1.resp_chequeprotect_calc['in_amount'] == '0.1'
        assert user1.resp_chequeprotect_calc['out_amount'] == '0.1'
        user1.chequeprotect_params(out_curr='UAH')
        assert user1.resp_chequeprotect_params['max'] == '3000'
        assert user1.resp_chequeprotect_params['min'] == '0.01'
        assert user1.resp_chequeprotect_params['min_balance_limit'] == '0.01'
        user1.chequeprotect_create(amount=0.1, out_curr='UAH', tgt=user2.email)
        assert user1.resp_chequeprotect_create['account_amount'] == '0.1'
        assert user1.resp_chequeprotect_create['in_fee_amount'] == '0'
        assert user1.resp_chequeprotect_create['out_fee_amount'] == '0'
        assert user1.resp_chequeprotect_create['status'] == 'new'
        user1.chequeprotect_confirm(token=User.part_protected_cheque)
        assert user1.resp_chequeprotect_confirm['cheque'] == User.part_protected_cheque
        assert user1.resp_chequeprotect_confirm['status'] == 'confirmed'
        user2.chequeprotect_cash(token=User.protected_cheque)
        assert user2.resp_chequeprotect_cash['currency'] == 'UAH'
        user1.get_balances(curr='UAH')
        assert user1.resp_balances['UAH'] == '0'
        user2.get_balances(curr='UAH')
        assert user2.resp_balances['UAH'] == '1.1'

    def test_2(self):
        """ Transfer currency by protected cheque without fee for operation: transfer 1 USD. """
        admin.set_wallet_amount(balance=1000000000, currency='USD', email=user1.email)
        admin.set_wallet_amount(balance=2000000000, currency='USD', email=user2.email)
        admin.set_fee(currency_id=admin.currency['USD'], tp=60)
        user1.chequeprotect_calc(amount=1, out_curr='USD')
        assert user1.resp_chequeprotect_calc['equiv_amount'] == '1'
        assert user1.resp_chequeprotect_calc['in_fee_amount'] == '0'
        assert user1.resp_chequeprotect_calc['out_fee_amount'] == '0'
        user1.chequeprotect_create(amount=1, out_curr='USD', tgt=user2.email)
        assert user1.resp_chequeprotect_create['account_amount'] == '1'
        assert user1.resp_chequeprotect_create['out_curr'] == 'USD'
        assert user1.resp_chequeprotect_create['tp'] == 'chequeprotect'
        user1.get_balances(curr='USD')
        assert user1.resp_balances['USD'] == '0'
        user2.get_balances(curr='USD')
        assert user2.resp_balances['USD'] == '2'
        user1.chequeprotect_confirm(token=User.part_protected_cheque)
        assert user1.resp_chequeprotect_confirm['status'] == 'confirmed'
        user2.chequeprotect_cash(token=User.protected_cheque)
        assert user2.resp_chequeprotect_cash['status'] == 'done'

    def test_3(self):
        """ Transfer currency by protected cheque without fee for operation: transfer 1.00999 BTC. """
        admin.set_wallet_amount(balance=2000000000, currency='BTC', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='BTC', email=user2.email)
        admin.set_fee(currency_id=admin.currency['BTC'], tp=35, add=100000000)
        admin.set_fee(currency_id=admin.currency['BTC'], tp=60)
        user1.chequeprotect_create(amount='1.00999', out_curr='BTC', tgt=user2.email)
        assert user1.resp_chequeprotect_create['account_amount'] == '1.00999'
        user1.chequeprotect_get_cheque(token=User.part_protected_cheque)
        assert user1.resp_chequeprotect_get_cheque == {'amount': '1.00999', 'currency': 'BTC', 'is_cashed': False,
                                                       'status': 'new', 'cheque': User.part_protected_cheque}
        user2.chequeprotect_get_cheque(token=User.part_protected_cheque)
        assert user2.resp_chequeprotect_get_cheque == {'amount': '1.00999', 'currency': 'BTC', 'is_cashed': False,
                                                       'status': 'new', 'cheque': User.part_protected_cheque}
        user3.chequeprotect_get_cheque(token=User.part_protected_cheque)
        assert user3.resp_chequeprotect_get_cheque['exception'] == 'InvalidToken'
        user1.chequeprotect_confirm(token=User.part_protected_cheque)
        user1.get_balances(curr='BTC')
        assert user1.resp_balances['BTC'] == '0.99001'
        user2.get_balances(curr='BTC')
        assert user2.resp_balances['BTC'] == '0'
        user1.chequeprotect_get_cheque(token=User.part_protected_cheque)
        assert user1.resp_chequeprotect_get_cheque == {'amount': '1.00999', 'currency': 'BTC', 'is_cashed': False,
                                                       'status': 'confirmed', 'cheque': User.part_protected_cheque}
        user2.chequeprotect_get_cheque(token=User.part_protected_cheque)
        assert user2.resp_chequeprotect_get_cheque == {'amount': '1.00999', 'currency': 'BTC', 'is_cashed': False,
                                                       'status': 'confirmed', 'cheque': User.part_protected_cheque}
        user3.chequeprotect_get_cheque(token=User.part_protected_cheque)
        assert user3.resp_chequeprotect_get_cheque['exception'] == 'InvalidToken'
        user2.chequeprotect_cash(token=User.protected_cheque)
        user1.get_balances(curr='BTC')
        assert user1.response_balances['BTC'] == '0.99001'
        user2.get_balances(curr='BTC')
        assert user2.response_balances['BTC'] == '1.00999'
        user1.chequeprotect_get_cheque(token=User.part_protected_cheque)
        assert user1.resp_chequeprotect_get_cheque == {'currency': 'BTC', 'is_cashed': True, 'status': 'done',
                                                       'cheque': User.part_protected_cheque}
        user2.chequeprotect_get_cheque(token=User.part_protected_cheque)
        assert user2.resp_chequeprotect_get_cheque == {'currency': 'BTC', 'is_cashed': True, 'status': 'done',
                                                       'cheque': User.part_protected_cheque}
        user3.chequeprotect_get_cheque(token=User.part_protected_cheque)
        assert user3.resp_chequeprotect_get_cheque['exception'] == 'InvalidToken'

    def test_4(self):
        """ Transfer currency by protected cheque with fee for operation: transfer 5 USD,
            common percent fee 1%, common absolute fee 0.5 USD. Refused after creating cheque and cashing by owner,
            refused after confirmed, owner get amount on his balance."""
        admin.set_wallet_amount(balance=6000000000, currency='USD', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='USD', email=user2.email)
        admin.set_fee(currency_id=admin.currency['USD'], tp=60, mult=10000000, add=500000000, is_active=True)
        user1.chequeprotect_calc(amount=5, out_curr='USD')
        assert user1.resp_chequeprotect_calc['account_amount'] == '5.55'
        assert user1.resp_chequeprotect_calc['in_fee_amount'] == '0.55'
        assert user1.resp_chequeprotect_calc['out_fee_amount'] == '0.55'
        assert user1.resp_chequeprotect_calc['in_amount'] == '5'
        assert user1.resp_chequeprotect_calc['out_amount'] == '5'
        user1.chequeprotect_create(amount=5, out_curr='USD', tgt=user2.email)
        assert user1.resp_chequeprotect_create['account_amount'] == '5.55'
        assert user1.resp_chequeprotect_create['in_fee_amount'] == '0.55'
        assert user1.resp_chequeprotect_create['out_fee_amount'] == '0.55'
        user2.chequeprotect_refuse(token=User.part_protected_cheque)
        assert user2.resp_chequeprotect_refuse['status'] == 'reject'
        user1.get_balances(curr='USD')
        assert user1.resp_balances['USD'] == '5.45'
        user2.get_balances(curr='USD')
        assert user2.resp_balances['USD'] == '0'
        user1.chequeprotect_cash(token=User.protected_cheque)
        assert user1.resp_chequeprotect_cash['exception'] == 'InvalidToken'
        user1.get_balances(curr='USD')
        assert user1.resp_balances['USD'] == '5.45'
        user2.get_balances(curr='USD')
        assert user2.resp_balances['USD'] == '0'
        admin.set_wallet_amount(balance=6000000000, currency='USD', email=user1.email)
        user1.chequeprotect_create(amount=5, out_curr='USD', tgt=user2.email)
        user1.chequeprotect_confirm(token=User.part_protected_cheque)
        user2.chequeprotect_refuse(token=User.part_protected_cheque)
        user1.get_balances(curr='USD')
        assert user1.resp_balances['USD'] == '5.45'
        user2.get_balances(curr='USD')
        assert user2.resp_balances['USD'] == '0'
        user1.chequeprotect_cash(token=User.protected_cheque)
        assert user1.resp_chequeprotect_cash['exception'] == 'InvalidToken'
        user1.get_balances(curr='USD')
        assert user1.resp_balances['USD'] == '5.45'
        user2.get_balances(curr='USD')
        assert user2.resp_balances['USD'] == '0'

    def test_5(self):
        """ Transfer currency by protected cheque with fee for operation: transfer 0.003 BTC,
            common absolute fee 0.0001 USD. """
        admin.set_wallet_amount(balance=100000000, currency='BTC', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='BTC', email=user2.email)
        admin.set_fee(currency_id=admin.currency['BTC'], tp=60, add=100000, is_active=True)
        user1.chequeprotect_params(out_curr='BTC')
        assert user1.resp_chequeprotect_params['min'] == '0.00001'
        assert user1.resp_chequeprotect_params['max'] == '3'
        assert user1.resp_chequeprotect_params['out_fee'] == {'add': '0.0001', 'max': '0', 'method': 'ceil', 'min': '0',
                                                              'mult': '0'}
        assert user1.resp_chequeprotect_params['min_balance_limit'] == '0.000101'
        user1.chequeprotect_create(amount='0.003', out_curr='BTC', tgt='AC179')
        assert user1.resp_chequeprotect_create['account_amount'] == '0.0031'
        user2.chequeprotect_cash(token=User.protected_cheque)
        assert user2.resp_chequeprotect_cash['exception'] == 'InvalidToken'
        user1.get_balances(curr='BTC')
        assert user1.resp_balances['BTC'] == '0.0969'
        user2.get_balances(curr='BTC')
        assert user2.resp_balances['BTC'] == '0'
        user1.chequeprotect_confirm(token=User.part_protected_cheque)
        user2.chequeprotect_cash(token=User.protected_cheque)
        user1.get_balances(curr='BTC')
        assert user1.resp_balances['BTC'] == '0.0969'
        user2.get_balances(curr='BTC')
        assert user2.resp_balances['BTC'] == '0.003'
        user1.chequeprotect_list(out_curr='BTC')
        assert user1.resp_chequeprotect_list['data'][0]['cheque'] == User.protected_cheque
        assert user1.resp_chequeprotect_list['data'][0]['status'] == 'done'

    def test_6(self, _personal_user_data, _disable_personal_fee):
        """ Transfer currency by protected cheque with fee for operation: transfer 5 RUB,
            common percent fee 5% and common absolute fee 1 RUB, with personal percent fee 1%
            and personal absolute fee 0.5 RUB. """
        admin.set_wallet_amount(balance=5550000000, currency='RUB', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='RUB', email=user2.email)
        admin.set_fee(currency_id=admin.currency['RUB'], tp=60, mult=50000000, add=1000000000, is_active=True)
        admin.set_fee(currency_id=admin.currency['RUB'], tp=60, mult=10000000, add=500000000, is_active=True, owner_id=user1.id)
        user1.chequeprotect_calc(amount=5, out_curr='RUB')
        assert user1.resp_chequeprotect_calc['account_amount'] == '5.55'
        assert user1.resp_chequeprotect_calc['in_fee_amount'] == '0.55'
        assert user1.resp_chequeprotect_calc['out_fee_amount'] == '0.55'
        user1.chequeprotect_create(amount=5, out_curr='RUB', tgt=user2.email)
        assert user1.resp_chequeprotect_create['account_amount'] == '5.55'
        assert user1.resp_chequeprotect_create['in_amount'] == '5'
        assert user1.resp_chequeprotect_create['out_amount'] == '5'
        user1.chequeprotect_cash(token=User.protected_cheque)
        assert user1.resp_chequeprotect_cash['exception'] == 'InvalidToken'
        user1.get_balances(curr='RUB')
        assert user1.resp_balances['RUB'] == '0'
        user2.get_balances(curr='RUB')
        assert user2.resp_balances['RUB'] == '0'
        user1.chequeprotect_confirm(token=User.part_protected_cheque)
        user1.chequeprotect_confirm(token=User.part_protected_cheque)
        assert user1.resp_chequeprotect_confirm['exception'] == 'InvalidToken'
        user2.chequeprotect_cash(token=User.protected_cheque)
        assert user2.resp_chequeprotect_cash['currency'] == 'RUB'
        user1.get_balances(curr='RUB')
        assert user1.resp_balances['RUB'] == '0'
        user2.get_balances(curr='RUB')
        assert user2.resp_balances['RUB'] == '5'
        user2.chequeprotect_refuse(token=User.part_protected_cheque)
        assert user2.resp_chequeprotect_refuse['exception'] == 'InvalidToken'
        user2.chequeprotect_cash(token=User.protected_cheque)
        assert user2.resp_chequeprotect_cash['exception'] == 'InvalidToken'
        user1.get_balances(curr='RUB')
        assert user1.resp_balances['RUB'] == '0'
        user2.get_balances(curr='RUB')
        assert user2.resp_balances['RUB'] == '5'

    def test_7(self, _personal_user_data, _disable_personal_fee):
        """ Transfer currency by protected cheque with fee for operation: transfer 0.003 LTC,
            common absolute fee 0.001 LTC, with personal absolute fee 0.0001 LTC. """
        admin.set_wallet_amount(balance=3100000, currency='LTC', email=user1.email)
        admin.set_wallet_amount(balance=0, currency='LTC', email=user2.email)
        admin.set_fee(currency_id=admin.currency['LTC'], tp=60, add=1000000, is_active=True)
        admin.set_fee(currency_id=admin.currency['LTC'], tp=60, add=100000, is_active=True, owner_id=user1.id)
        user1.chequeprotect_params(out_curr='LTC')
        assert user1.resp_chequeprotect_params['min'] == '0.00001'
        assert user1.resp_chequeprotect_params['min_balance_limit'] == '0.00011'
        user1.chequeprotect_create(amount='0.003', out_curr='LTC', tgt=user2.email)
        assert user1.resp_chequeprotect_create['account_amount'] == '0.0031'
        assert user1.resp_chequeprotect_create['in_fee_amount'] == '0.0001'
        assert user1.resp_chequeprotect_create['out_fee_amount'] == '0.0001'
        user1.chequeprotect_confirm(token=User.part_protected_cheque)
        user2.chequeprotect_cash(token=User.protected_cheque)
        user1.get_balances(curr='LTC')
        assert user1.resp_balances['LTC'] == '0'
        user2.get_balances(curr='LTC')
        assert user2.resp_balances['LTC'] == '0.003'
        user2.chequeprotect_cash(token=User.protected_cheque)
        user1.get_balances(curr='LTC')
        assert user1.resp_balances['LTC'] == '0'
        user2.get_balances(curr='LTC')
        assert user2.resp_balances['LTC'] == '0.003'

    def test_8(self):
        """ Create protected cheque for not real user. """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user1.email)
        user1.chequeprotect_create(amount=0.01, out_curr='UAH', tgt='zzz@mailinator.com')
        assert user1.resp_chequeprotect_create['exception'] == 'InvalidContact'
        user1.chequeprotect_create(amount=0.01, out_curr='UAH', tgt='AC100000')
        assert user1.resp_chequeprotect_create['exception'] == 'InvalidContact'
        user1.chequeprotect_create(amount=0.01, out_curr='UAH', tgt='@AC100000')
        assert user1.resp_chequeprotect_create['exception'] == 'InvalidContact'
        user1.chequeprotect_create(amount=0.01, out_curr='UAH', tgt=None)
        assert user1.resp_chequeprotect_create['exception'] == 'InvalidContact'
        user1.get_balances(curr='UAH')
        assert user1.resp_balances['UAH'] == '1'

    def test_9(self):
        """ Create protected cheque with wrong amount format. """
        user1.chequeprotect_create(amount=0.001, out_curr='UAH', tgt=user2.email)
        assert user1.resp_chequeprotect_create['exception'] == 'InvalidAmountFormat'

    def test_10(self):
        """ Create protected cheque with not real currency. """
        user1.chequeprotect_create(amount=0.01, out_curr='UHA', tgt=user2.email)
        assert user1.resp_chequeprotect_create['exception'] == 'InvalidCurrency'

    def test_11(self):
        """ Create protected cheque without amount. """
        user1.chequeprotect_create(amount=None, out_curr='UAH', tgt=user2.email)
        assert user1.resp_chequeprotect_create['exception'] == 'TypeError'

    def test_12(self):
        """ Create protected cheque without out_curr. """
        user1.chequeprotect_create(amount=0.01, out_curr=None, tgt=user2.email)
        assert user1.resp_chequeprotect_create['exception'] == 'InvalidCurrency'

    def test_13(self):
        """ Create protected cheque with fee, without enough amount on balance. """
        admin.set_wallet_amount(balance=10000000, currency='UAH', email=user1.email)
        admin.set_fee(currency_id=admin.currency['UAH'], tp=60, add=100000, is_active=True)
        user1.chequeprotect_create(amount=0.01, out_curr='UAH', tgt=user2.email)
        assert user1.resp_chequeprotect_create['exception'] == 'InsufficientFunds'

    def test_14(self):
        """ Confirmed protected cheque by not owner, refuse protected cheque by not recipient. """
        admin.set_wallet_amount(balance=1000000000, currency='UAH', email=user1.email)
        admin.set_fee(currency_id=admin.currency['UAH'], tp=60, is_active=True)
        user1.chequeprotect_create(amount=1, out_curr='UAH', tgt=user3.email)
        user2.chequeprotect_confirm(token=User.part_protected_cheque)
        assert user2.resp_chequeprotect_confirm['exception'] == 'InvalidToken'
        user3.chequeprotect_confirm(token=User.part_protected_cheque)
        assert user3.resp_chequeprotect_confirm['exception'] == 'InvalidToken'
        user1.chequeprotect_refuse(token=User.part_protected_cheque)
        assert user1.resp_chequeprotect_refuse['exception'] == 'InvalidToken'
        user1.chequeprotect_confirm(token=User.part_protected_cheque)
        user1.chequeprotect_refuse(token=User.part_protected_cheque)
        assert user1.resp_chequeprotect_refuse['exception'] == 'InvalidToken'
        user3.chequeprotect_refuse(token=User.part_protected_cheque)
        user3.chequeprotect_cash(token=User.protected_cheque)
        assert user3.resp_chequeprotect_cash['exception'] == 'InvalidToken'
        user1.get_balances('UAH')
        assert user1.resp_balances['UAH'] == '1'

